//HASPHOOK JOB MSGLEVEL=(1,1),CLASS=A,MSGCLASS=A
//*********************************************************************
//*                                                                 ***
//*    Job:      HASPHOOK                                           ***
//*    Product:  HASP V4 for MVT.                                   ***
//*    Purpose:  Update IOS macros IECXCP and IECIOS in             ***
//*              SYS1.MODGEN to add HASP support to IOS.            ***
//*    Step:     5.7                                                ***
//*    Update:   2003/02/08                                         ***
//*                                                                 ***
//*********************************************************************
//*
//IOSUPDTE EXEC PGM=IEBUPDTE,PARM=MOD,REGION=96K
//SYSPRINT  DD  SYSOUT=A
//SYSUT1    DD  DISP=OLD,DSN=SYS1.MODGEN,UNIT=3330,VOL=SER=DLIB01
//SYSUT2    DD  DISP=OLD,DSN=SYS1.MODGEN,UNIT=3330,VOL=SER=DLIB01
//SYSIN     DD  DATA
./    CHANGE NAME=IECXCP,LIST=ALL
*    ATTN TABLE INDEX FOR HASP                                 @THAS801 10984019
UCBHASP  EQU   X'01' .                  HASP PSEUDO-DEVICE     @THAS801 10985019
         SPACE 1                                               @THAS801 10986019
INVHSP   EQU   X'10'               ABEND CODE FOR INVALID HASP @THAS801 25612101
         TM    UCBATI(UCBREG),UCBHASP   PSEUDO DEVICE          @THAS801 35220021
         BC    8,XCP061C1               NO                     @THAS801 35280021
         L     APBSRG,AHASPE            GET HASP ROUTINE       @THAS801 35340021
         BALR  LNKRG2,APBSRG            LINK TO HASP           @THAS801 35400021
*****     HASP RETURNS          *******************************@THAS801 35460021
         B     XCP021                   NORMAL - IGNORE REQUEST@THAS801 35520021
         B     XERX02F1                 ABEND WITH 100         @THAS801 35580021
***************************************************************@THAS801 35640021
XCP061C1 DS    0H                                              @THAS801 35700021
./  CHANGE NAME=IECIOS,LIST=ALL
XERX02F1 MVI   CODEDUMP,INVHSP .   INVALID HASP                @THAS801 09043201
         B     XERX02A .           HANDLE THE ERROR            @THAS801 09043401
AHASPE   DC    V(IECHASPE) .       HASP EXCP ROUTINE           @THAS801 74220001
./  ENDUP
/*
//
