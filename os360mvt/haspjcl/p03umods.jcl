//P03UMODS JOB 1,'P03UMODS HASP 4',MSGLEVEL=(1,1),CLASS=A,MSGCLASS=A
//*********************************************************************
//*                                                                 ***
//*    Job:      P03UMODS                                           ***
//*    Product:  HASP V4 for MVT.                                   ***
//*    Purpose:  Update customized HASP source in SYS1.HASPSRC      ***
//*              with user modifications.                           ***
//*    Update:   2003/02/09                                         ***
//*                                                                 ***
//*    Note:     Message "IEB805I CONTROL STATEMENT ERROR" and      ***
//*              return code 4 are normal if there are no usermods  ***
//*              in the SYSIN input stream.                         ***
//*                                                                 ***
//*********************************************************************
//*
//UMODS   EXEC PGM=IEBUPDTE,PARM=MOD,REGION=96K
//SYSUT1   DD  DISP=OLD,DSN=SYS1.HASPSRC
//SYSUT2   DD  DISP=OLD,DSN=SYS1.HASPSRC
//SYSPRINT DD  SYSOUT=A
//SYSIN    DD  DATA,DLM='??'
./ CHANGE NAME=HASPXEQ
         CLI   3(R1),C'*'          Was class specified as "*"? @THAS802 X6698040
         BE    XJCLSYST            Go set to MSGCLASS if so    @THAS802 X6698060
XJCLSYST IC    WA,XJCLOMC+1        Set SYSOUT class = MSGCLASS @THAS802 X6704000
./ ENDUP
??
//
/*EOF
