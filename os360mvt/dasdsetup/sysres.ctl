#
# Pack layout file for MFT system residence volume
#
sysres 3330 * dasdsetup/ieaipl00.rdr
sysctlg         cvol    trk 1 0 0       ps f 256 256 8
sys1.logrec     dip     trk 1 0 0       ps u 0 1944 0
sysvtoc         vtoc    trk 16
sys1.parmlib    xmit ../os360/Reslibs/PARMLIB.XMI
sys1.imagelib   xmit ../os360/Reslibs/IMAGELIB.XMI
sys1.nucleus    xmit ../os360/Reslibs/NUCLEUS.XMI cyl
sys1.linklib    xmit ../os360/Reslibs/LINKLIB.XMI cyl
sys1.svclib     xmit ../os360/Reslibs/SVCLIB.XMI cyl
sys1.proclib    xmit ../os360/Reslibs/PROCLIB.XMI cyl
sys1.sysjobqe   empty   cyl 40 0 0      da f 176 176 0
sys1.dump       empty   cyl 15 0 0      ps u 0 7294 0
sys1.coblib     xmit ../os360/Reslibs/COBLIB.XMI
sys1.maclib     xmit ../os360/Reslibs/MACLIB.XMI
sys1.pl1lib     xmit ../os360/Reslibs/PL1LIB.XMI
sys1.samplib    xmit ../os360/Reslibs/SAMPLIB.XMI
sys1.sortlib    xmit ../os360/Reslibs/SORTLIB.XMI
