#
# Pack layout file for OS/360 distribution library volume
#
dlib01 3330 *
sysvtoc        vtoc trk 18
sys1.modgen    xmit ../os360/dlibs/MODGEN.XMI cyl 40 5
sys1.modgen2   xmit ../os360/dlibs/MODGEN2.XMI cyl 20 5
sys1.genlib    xmit ../os360/dlibs/GENLIB.XMI cyl
sys1.coblib    xmit ../os360/Reslibs/COBLIB.XMI
sys1.parmlib   xmit ../os360/Reslibs/PARMLIB.XMI
sys1.pl1lib    xmit ../os360/Reslibs/PL1LIB.XMI
sys1.proclib   xmit ../os360/Reslibs/PROCLIB.XMI
sys1.sortlib   xmit ../os360/Reslibs/SORTLIB.XMI
sys1.al531     xmit ../os360/dlibs/AL531.XMI
sys1.as037     xmit ../os360/dlibs/AS037.XMI
sys1.cb545     xmit ../os360/dlibs/CB545.XMI
sys1.ci505     xmit ../os360/dlibs/CI505.XMI
sys1.ci535     xmit ../os360/dlibs/CI535.XMI
sys1.co503     xmit ../os360/dlibs/CO503.XMI
sys1.cq513     xmit ../os360/dlibs/CQ513.XMI
sys1.cq519     xmit ../os360/dlibs/CQ519.XMI
sys1.dm508     xmit ../os360/dlibs/DM508.XMI
sys1.dm509     xmit ../os360/dlibs/DM509.XMI
sys1.dn527     xmit ../os360/dlibs/DN527.XMI
sys1.dn533     xmit ../os360/dlibs/DN533.XMI
sys1.dn539     xmit ../os360/dlibs/DN539.XMI
sys1.dn554     xmit dasdsetup/dn554.xmi
sys1.ed521     xmit ../os360/dlibs/ED521.XMI
sys1.fo500     xmit ../os360/dlibs/FO500.XMI
sys1.fo520     xmit ../os360/dlibs/FO520.XMI
sys1.fo550     xmit ../os360/dlibs/FO550.XMI
sys1.io523     xmit ../os360/dlibs/IO523.XMI
sys1.io526     xmit ../os360/dlibs/IO526.XMI
sys1.ld547     xmit ../os360/dlibs/LD547.XMI
sys1.lm501     xmit ../os360/dlibs/LM501.XMI
sys1.lm512     xmit ../os360/dlibs/LM512.XMI
sys1.lm532     xmit ../os360/dlibs/LM532.XMI
sys1.lm546     xmit ../os360/dlibs/LM546.XMI
sys1.lm537     xmit ../os360/dlibs/LM537.XMI
sys1.lm542     xmit ../os360/dlibs/LM542.XMI
sys1.nl511     xmit ../os360/dlibs/NL511.XMI
sys1.pl552     xmit ../os360/dlibs/PL552.XMI
sys1.pvtmacs   xmit ../os360/dlibs/PVTMACS.XMI
sys1.rc536     xmit ../os360/dlibs/RC536.XMI
sys1.rc541     xmit ../os360/dlibs/RC541.XMI
sys1.rc543     xmit ../os360/dlibs/RC543.XMI
sys1.rc551     xmit ../os360/dlibs/RC551.XMI
sys1.rg038     xmit ../os360/dlibs/RG038.XMI
sys1.sm023     xmit ../os360/dlibs/SM023.XMI
sys1.ut506     xmit ../os360/dlibs/UT506.XMI
sys1.dcmdlib   xmit ../os360/dlibs/DCMDLIB.XMI
sys1.dhelp     xmit ../os360/dlibs/DHELP.XMI
sys1.duads     xmit dasdsetup/sys1auad.xmi
sys1.ci555     xmit ../os360/dlibs/CI555.XMI
sys1.cq548     xmit ../os360/dlibs/CQ548.XMI
sys1.maclib    xmit ../os360/Reslibs/MACLIB.XMI
sys1.genlibx   xmit ../os360/dlibs/GENLIBX.XMI
sys1.tsogen    xmit ../os360/dlibs/TSOGEN.XMI
sys1.tsomac    xmit ../os360/Reslibs/TSOMAC.XMI
sys1.tcammac   xmit ../os360/Reslibs/TCAMMAC.XMI
sys1.fortlib   empty   cyl 4  2  40   po  u  0   13030 0
