Here at last is the result of my long search for OS/360 materials.

Several directories are present, and a multitude of files, bin, txt, ldr and XMI
types; here's how they are used:

BIN files are card images that were downloaded in BINARY mode, to preserve the
contents. They should be uploaded to card-image datasets, for the same reasons.

TXT files are source, downloaded from MVS, with translation to ASCII and CR/LF

XMI files are created by the TSO TRANSMIT command, then downloaded in binary.
To reverse this process, upload the files, in binary, to a card-image dataset
with blksize=3200.  Then use the TSO RECEIVE command to recreate the original
dataset.

LDR files are the stand-alone loaders, from the stand-alone utilities.  They are
presented as separate files here because they were downloaded in BINARY for this
file set.  In order to even try to use these, you need to upload them, in
BINARY, to card-image datasets.  Then you can punch them, disassemble, or
whatever.

(The loaders directory contains, in binary form, the loader programs from the
 utilities in the UT507 library.  The program called clip.ldr is the best damn
stand-alone loader I've had the privilege to use.  It's seven cards and will
load just about anything!  It was always my 'general purpose loader' when I had
to work with a stand-alone program.  Whenever a load failed, I stuck this loader
in and it performed FLAWLESSLY.)

All MACLIBs, and PARMLIB and PROCLIB, are present in both forms, to allow
browsing directly without the need to upload.

In the SRCLIBS directory are the original tape files, from the optional
materials tapes.  These are files whose names end in F#, with # ranging from 1
to 8.  These can be uploaded as TXT files and run through such programs as
IEBUPDTE or PDSLOAD to re-create the original source libraries.  Unfortunately,
those tapes were flawed; a few of the modules are lacking the last few lines.  I
have not yet determined which modules have this problem; perhaps in a future
version, I'll get the actual code fixed.  I'll probably have to disassemble the
corresponting load modules, etc.

Reference cards are for the common DASD devices at that time, with 
a short note about the 2841 controller, which seems to have been the
most common controller used with most of these devices. (but NOT the 3330!)

There's an additional "bonus".  The original CLIP/360 program is included in the
UT507 directory.  Those who know its usage will welcome an old friend; those who
don't should study it with great care before any attempt to use it. (Good luck
making it work; it depends on a BC-mode PSW and the old SIO/HIO/TIO
instructions.)

To the curious: my purpose in assembling this was two-fold.  I wanted to
resurrect those original language processors and I wanted to learn more about
TCAM queue handling and how it might have related to MQ Series queue handling.
As time permits, the language processors will eventually be altered to make
OS/390-compatible processors, with Y2K support.  (Except COBOL, which I never
learned).  With regard to the TCAM goodies, I was a "babe in the woods", and
still am, about the queue handler.  I am just trying to learn more about this
entire process.  As an old user of ALGOL, that was my first language processor
to attack.  :-)

To all who contributed, my heartfelt appreciation.  Thank you all.
