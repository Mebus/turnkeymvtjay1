         MACRO                                                          00020000
         SGIEE301                                                       00040000
         COPY  SGGBLPAK                                                 00060000
.*A000848-000857,003409-003539,148890-149890,197890,             S21003 00060221
.*A211890-212890,260890,274890-275890,316890-322890,             S21003 00060421
.*A323890-324390,594890-604390                                   S21003 00060621
.*C323390                                                        S21003 00060821
.*D094390-141390,155390-183390,204390-211390,218390-253390       S21003 00061021
.*D267390,274390,281390-309390,554390-594390                     S21003 00061221
.*                                                                19029 00062019
.*000700,000800,012660,012730,014250-014290,014371-024385,054390  19029 00064019
.*074390,114390,154390-314390,494390                              19029 00066019
.*DIDOCS                                                          19029 00068019
.*                                                                 I274 00074019
.*011200,013200,014200,014361,014365,013950                 19085  I274 00076019
.*CONSOLIDATED SYSGEN                                       19085  I274 00078019
.*                                                                 I254 00078619
.*WRITE TO PROGRAMMER -WTP-                                 19058  I254 00079219
.*                                                                19872 00080019
.*014330                                                          19872 00082019
.*OWNERSHIP                                                       19872 00084019
.*02677019,02717019-02777019                                     A34917 00084420
         GBLA  &GRCON,&TRCON                                     S21003 00084721
         GBLC  &BRODA(64),&BRODD(64)                             S21003 00085021
         LCLC  &A(64),&B(64),&D(64)                              S21003 00085321
         LCLA  &TO                                               S21003 00085621
         LCLA  &N,&M,&O                                           19029 00086019
         AIF   (&SGCPRGB(8)).MS1                                        00100000
         AIF   (&SGCPRGB(4)).MFT                                   MFT2 00110015
.*                                                                      00120000
.*       THIS IS A MASTER SCHEDULER GROUP III SYSGEN MACRO WHICH        00140000
.*       GENERATES INCLUDE CARDS FOR THE FOLLOWING NUCELUS MODS;        00160000
.*       1.  MASTER SCHEDULER EXTERNAL INTERRUPT HANDLER ROUTINE        00180000
.*       (IF ALTERNATE CONSOLE SPEDIFIED)                               00200000
.*       2.  ONE OF TWO MASTER SCHEDULER CONSOLE ATTENTION              00220000
.*       INTERRUPT HANDLER ROUTINES(ONE FOR SYSTEMS 1 THRU 3;           00240000
.*       OR ONE FOR SYSTEMS 4 AND 5)                                    00260000
.*       3.  ONE OF TWO MASTER SCHEDULER RESIDENT CORE                  00280000
.*       DEFINITION MODULES (ONE FOR 1052 SUPPORT; OR ONE               00300000
.*       FOR COMPOSITE CONSOLE SUPPORT)                                 00320000
.*       4.  INITIATOR UNSOLICITED INTERRUPT ROUTINE                    00340000
.*       5. MASTER SCHEDULER RESIDENT CORE                       OPTN 4 00360000
.*       6. ATTENTION ROUTINE FOR ALLOCATE                       OPTN 4 00380000
.*       7. QUEUE MANAGER RESIDENT CORE                          OPTN 4 00400000
.*       8. VARIOUS CONTROL BLOCKS INCLUDING ATTENTION ECB       OPTN 4 00420000
.*       9. WRITE TO PROGRAMMER MODULE IEFWTP0A IN PCP             I254 00430019
.*                                                                      00440000
.*       THE FOLLOWING SYSGEN SYMBOLS WILL BE EQUATED TO THE            00460000
.*       CORRESPONDING VALUES:                                          00480000
.*       1.  &SGSCHDC(1)(1,3) = PRIN CONSOLE INPUT UCB                  00500000
.*                      (4,3) = PRIN CONSOLE OUTPUT UCB                 00520000
.*       2.  &SGSCHDC(2)(1,3) = ALT CONSOLE INPUT UCB                   00540000
.*                      (4,3) = ALT CONSOLE OUTPUT UCB                  00560000
.*       3.  &SGSCHDB(1) = SYSTEMS 1-5                                  00580000
.*           &SGCPRGB(3) = SYSTEM 4                                     00600000
.*           &SGCPRGB(4) = SYSTEM 5 OR 7                                00620000
.*       4.  &SGCPRGB(8) = OPTION 4                                     00640000
.*       5.  &SGSCHDB(34) = MCS OPTION                            19872 00645019
.*       6.  &GETB(3) = I/O DEVICE SYSGEN                         19872 00650019
.*       7.  &SGSCHDB(7) = SMF OPTION                             19872 00655019
.*                                                                      00660000
         AIF   ('&SGSCHDC(1)'(1,3) EQ '&SGSCHDC(1)'(4,3)).A1            00680000
         PUNCH ' INCLUDE CI505(IEERSR01)'  M.S. RES CORE(COMPOSITE      00700017
         PUNCH ' INCLUDE CI505(IEFWTP0A)'    WTP MODULE            I254 00705019
         AIF   (&GETB(3)).A2B               IODEVICE SYSGEN        I274 00710019
         AGO   .A2                                                      00720000
.A1      PUNCH ' INCLUDE CI505(IEERSC01)'  M.S. RES CORE(1052)          00740017
         PUNCH ' INCLUDE CI505(IEFWTP0A)'    WTP MODULE            I254 00745019
.A1A     AIF   (&GETB(3)).A2B               IODEVICE SYSGEN        I274 00750019
.A2      PUNCH ' INCLUDE CI505(IEFDPOST)'                               00760017
         PUNCH ' INCLUDE SYSPUNCH(IEACVTPC)'                            00770017
         AIF   (&SGCPRGB(4)).A2A                                        00780000
         AIF   ('&SGSCHDA(7)' GE '1').A20                          4605 00800000
         PUNCH ' INCLUDE CI505(IEFKRESA)'                               00820017
         AGO   .A2B                                                     00840000
.A20     PUNCH ' INCLUDE CI505(IEFKRESB)'                          4605 00860017
         AGO   .A2B                                                4605 00880000
.A2A     AIF   ('&SGSCHDA(7)' GE '1').A2A1                         4605 00900000
         PUNCH ' INCLUDE CI505(MCONRESA)'                          4605 00920017
         AGO   .A2A2                                               4605 00940000
.A2A1    PUNCH ' INCLUDE CI505(MCONRESB)'                          4605 00960017
.A2A2    PUNCH ' INCLUDE SYSPUNCH(IEFSD032)' MST BE CHGD FOR N PRT 4605 00980000
.A2B     ANOP                                                           01000000
         AIF   ('&SGSCHDC(1)'(1,3) EQ '&SGSCHDC(2)'(1,3)).A3            01020000
         AIF   (&SGCPRGB(3) OR &SGCPRGB(4)).A2C                         01040000
         PUNCH ' INCLUDE CI505(IEEBC1PE)'  EXTERNAL INTER RT(SYS1-3)    01060017
         AGO   .A3                                                      01080000
.A2C     PUNCH ' INCLUDE CI505(IEECVCRX)'  EXTERNAL INTER RT(SYS4-5)    01100017
.A3      AIF   (&GETB(3)).END               IODEVICE SYSGEN        I274 01110019
         AIF   (&SGSCHDB(1)).A4                                    I274 01120019
         AGO   .END                                                     01140000
.A4      AIF   (&SGCPRGB(3) OR &SGCPRGB(4)).A5                          01160000
         PUNCH ' INCLUDE CI505(IEECIR01)'  ATTN INTER RT(SYS1-3)        01180017
         AGO   .END                                                     01200000
.A5      PUNCH ' INCLUDE CI505(IEECVCRA,IEECVCTW)'  ATTN INT RT(SYS4-5) 01220017
         PUNCH ' INCLUDE CI505(IEECVPRG)'  REPLY PURGE ROUTINE(SYS4-5)  01240017
         PUNCH ' INCLUDE SYSPUNCH(IEECVUCM)'                            01260000
.END     ANOP                                                           01280000
         MEXIT                                                          01300000
.MS1     AIF   (&GETB(3) AND &SGSCHDB(34)).MCS1                    I274 01310019
         AIF   (&GETB(3)).IO1                                      I274 01320019
         PUNCH ' INCLUDE CI535(IEEBASEC,IEFVPOST)'                      01340017
         PUNCH ' INCLUDE CI505(IEECVCRA,IEFQRESD,IEFATECB)'             01360017
         AIF   (&SGSCHDB(34)).MCS1                                  MCS 01370018
         PUNCH ' INCLUDE CI535(IEECVCTB,IEECVED2)'                      01380017
         AGO   .RADICE8                                             MCS 01385018
.MCS1    PUNCH ' INCLUDE CI535(IEECMED2,IEECMQWR)'                  MCS 01390018
.RADICE8 AIF   (&GETB(3) AND &SGSCHDB(34)).IO1                     I274 01395019
         PUNCH ' INCLUDE CI505(IEECVCRX)'                               01400017
.IO1     PUNCH ' INCLUDE SYSPUNCH(IEECVUCM) '                      I274 01405019
         AIF   (&GETB(3) AND NOT &SGSCHDB(34)).OUT                 I274 01410019
         AIF   (&GETB(3) AND &SGSCHDB(34)).IO2                     I274 01415019
 PUNCH ' INCLUDE SYSPUNCH(IEACVTPC)'                              REFR  01423017
         PUNCH ' INCLUDE CI535(IEAQCB02)'                          REFR 01424018
.IO2     ANOP                                                      I274 01427019
         AGO   .TESTMCS                                             MCS 01431018
.MFT     ANOP                                                      MFT2 01431319
         AIF   (&GETB(3) AND &SGSCHDB(34)).MCS2                    I274 01432519
         AIF   (&GETB(3)).IEFSD                                  A30068 01432719
         PUNCH ' INCLUDE CI505(IEFSD567,IEFATECB)'                 MFT2 01433517
         PUNCH ' CHANGE IEECVPRB(IEEMSER)'                              01434015
         PUNCH ' INCLUDE CI505(IEESD568,IEFQRESD)'                 MFT2 01434517
.*      FIX FOR A47740 TO ONLY INCLUDE IEECVCRX WITH ALT CONSOLE A47740 01434921
         PUNCH ' INCLUDE CI505(IEECVCRA)'                               01435321
         AIF   ('&SGSCHDC(1)'(1,3) EQ '&SGSCHDC(2)'(1,3)).NOXTRN A47740 01435421
         PUNCH ' INCLUDE CI505(IEECVCRX)'                               01485421
.NOXTRN  AIF   (&SGSCHDB(34)).MCS2                            BE A47740 01495421
         PUNCH ' INCLUDE CI505(IEECVCTW)'                          MFT2 01535421
         AGO   .RADICE2                                             MCS 01585421
.MCS2    PUNCH ' INCLUDE SYSPUNCH(IEECMAWR)'                     A27750 01635421
.RADICE2 ANOP                                                       MCS 01685421
          AIF  (NOT &SGSCHDB(38) OR NOT &SGCPRGB(4)).RADICE3        MCS 01735421
         PUNCH ' INCLUDE CI505(IEELOGWR)'                           MCS 01785421
.RADICE3 AIF   (&GETB(3) AND &SGSCHDB(34)).IEFSD                 A39537 01835421
         PUNCH ' INCLUDE CI505(IEECIR50)'                          MFT2 01885421
         PUNCH ' INCLUDE CI505(IEAQCB01)'                          REFR 01935421
         PUNCH ' INCLUDE SYSPUNCH(IEACVTPC)'                       REFR 01985421
         PUNCH ' INCLUDE SYSPUNCH(IEFSD032)'                            02637019
.IEFSD   ANOP                                                     M4993 02657019
         PUNCH ' CHANGE IEECVTCB(IEACMTCB)'                       M4993 02697019
         PUNCH ' INCLUDE SYSPUNCH(IEECUCB)'                       M4993 02797019
.G2250   ANOP                                                      I274 02837019
.TESTMCS AIF   (NOT &SGSCHDB(34)).OUT                               MCS 03439018
.*  THIS CODE INCLUDES RESIDENT PORTION OF DCM INTO SYSTEM       S21003 05439021
&N       SETA  &SG5450A                                          S21003 07439021
.KCHECK  ANOP                                                     19029 14839019
         AIF    (&N EQ 0).TST2250                                S21003 14939021
         PUNCH ' INCLUDE SYSPUNCH(IEECVD&N) '                    S21003 15039021
&N       SETA &N-1                                               S21003 15139021
         AGO   .KCHECK                                            19029 19039019
.TST2250 ANOP                                                     19029 19739019
&N       SETA  &SGSCHDA(10)                                      S21003 20039021
.RCHECK  ANOP                                                     19029 21139019
         AIF   (&N EQ 0).TST2260                                 S21003 21239021
         PUNCH ' INCLUDE SYSPUNCH(IEECVE&N)  '                   S21003 21339021
&N       SETA  &N-1                                              S21003 21439021
         AGO   .RCHECK                                            19029 21539021
.TST2260 ANOP                                                     19029 26039019
&N       SETA   &SGSCHDA(11)                                     S21003 26339021
.CCHECK  ANOP                                                     19029 27439019
         AIF  (&N EQ 0).TSTAN48                               MD  M0434 27539000
         PUNCH ' INCLUDE SYSPUNCH(IEECVF&N) '                    S21003 27639021
&N       SETA  &N-1                                              S21003 27739021
         AGO   .CCHECK                                            19029 31639019
.TSTAN48 ANOP                                                 MD  M0434 31649000
&N       SETA  &SGSCHDA(17)            COUNT OF 3277-1        MD  M0434 31659000
.AN4CHK  ANOP                                                 MD  M0434 31669000
         AIF   (&N EQ 0).TSTAN19                              MD  M0434 31679000
         PUNCH ' INCLUDE SYSPUNCH(IEECVG&N) ' 3277-1 DCM      MD  M0434 31681000
&N       SETA  &N-1                    DECRE COUNT            MD  M0434 31683000
         AGO   .AN4CHK                 GET NEXT               MD  M0434 31685000
.TSTAN19 ANOP                                                 MD  M0434 31687000
&N       SETA  &SGSCHDA(18)            COUNT OF 3277-2        MD  M0434 31687400
.AN9CHK  ANOP                                                 MD  M0434 31687800
         AIF   (&N EQ 0).RESTRAN       ANY 3277-2 CONSOLES    MD  M0434 31688200
         PUNCH ' INCLUDE SYSPUNCH(IEECVH&N) ' 3277-2 DCM      MD  M0434 31688600
&N       SETA &N-1                                            MD  M0434 31688700
         AGO   .AN9CHK                                        MD  M0434 31688800
.RESTRAN ANOP                                                    S21003 31689021
&M       SETA 1                                                  S21003 31739021
.PASSER  ANOP                                                    S21003 31789021
&A(&M)   SETC  '&BRODA(&M)'                                      S21003 31839021
&D(&M)   SETC  '&BRODD(&M)'                                      S21003 31889021
&M       SETA  &M+1                                              S21003 31939021
         AIF  (&M LE 64).PASSER                                  S21003 31989021
.* THIS CODE INCLUDES TRANSIENT AREAS INTO THE SYSTEM            S21003 32039021
&N       SETA  1                                                 S21003 32089021
.CHECNAM ANOP                                                    S21003 32139021
         AIF  ('&D(&N)' EQ '').DOAGAIN                           S21003 32189021
         PUNCH ' INCLUDE SYSPUNCH(IEECV&D(&N)) '                 S21003 32239021
.DOAGAIN ANOP                                                    S21003 32289021
&N       SETA &N+1                                               S21003 32489021
         AIF  (&N LE 64).CHECNAM                                 S21003 32689021
.* THIS CODE INCLUDES PFK AREAS INTO THE SYSTEM                  S21003 32889021
         AIF   (&SGSCHDA(16) EQ 0).SECPROC                       S21003 33789021
         AGO   .KEYPCH                                           S21003 34689021
.SECPROC ANOP                                                    S21003 35589021
&N       SETA  1                                                 S21003 36489021
.TRYAGIN ANOP                                                    S21003 37389021
         AIF   (&SGSCNPA(&N) EQ 0).TRYMORE                       S21003 38289021
.KEYPCH  PUNCH '   INCLUDE SYSPUNCH(IEEPFKEY)  '                 S21003 39189021
         AGO  .ONE                                               S21003 40089021
.TRYMORE ANOP                                                    S21003 40989021
&N       SETA  &N+1                                              S21003 41889021
         AIF   (&N LE 31).TRYAGIN                                S21003 42789021
.*  THIS INCLUDES RESIDENT AREAS INTO SYSTEM                     S21003 43689021
.ONE     ANOP                                                    S21003 44589021
&TO       SETA  &GRCON-&TRCON                                    S21003 45489021
.PRNTNAM ANOP                                                    S21003 46389021
         AIF   (&TO EQ 0).STOPGPH                                S21003 47289021
         PUNCH '  INCLUDE SYSPUNCH(IEECVR&TO) '                  S21003 48189021
&TO      SETA  &TO-1                                             S21003 49089021
         AGO   .PRNTNAM                                          S21003 49989021
.STOPGPH ANOP                                                    S21003 50889021
         AIF   (NOT &SGSCHDB(35)).RADICE4  TEST DUMMY EXIT       S21003 51789021
         PUNCH ' INCLUDE CI505(IEECVCTE) '                       S21003 52689021
.RADICE4 PUNCH ' INCLUDE CI505(IEECMDSV,IEECMWSV) '              S21003 53589021
         AIF   (&SG5450A GT 0 OR &SGSCHDA(10) GT 0 OR &SGSCHDA(11) GT 0X54489021
                OR &SGSCHDA(17) GT 0 OR &SGSCHDA(18) GT 0 OR &SGDCLSB(8X54539021
               0)).DOMIN                                         M5508  54589021
.NODOM   PUNCH '  INCLUDE CI505(IEECVDOM)  '                     S21003 56289021
         AGO   .RADICE1                                          S21003 57189021
.DOMIN   PUNCH '  INCLUDE CI505(IEECMDOM) '                      S21003 58089021
         PUNCH ' INCLUDE SYSPUNCH(IEECVSUB) '                    S21003 59089021
.RADICE1 ANOP                                                       MCS 61439018
         AIF   (NOT &SGSCHDB(13)).OUT                               MCS 63439018
         PUNCH ' INCLUDE SYSPUNCH(IHB000) '                         MCS 65439018
.*       THIS INCLUDES THE 2740 DSP INTO THE NUCLEUS DURING STAGE 2 MCS 67439018
         PUNCH ' INCLUDE CQ513(IECTLOPN)'                           MCS 69439018
.*       THIS INCLUDES THE LINE OPEN ROUTINE INTO THE NUCLEUS IN STAGE2 71439018
         AIF   ( NOT &SGCPRGB(4) OR &SGSUPRB(21)).OUT               MCS 73439018
.*       IF MFT IS NOT PRESENT OR A RESIDENT ACCESS METHOD IS,EXIT  MCS 75439018
         PUNCH ' INCLUDE CQ513(IGG019MA,IGG019MB,IGG019M0)'         MCS 77439018
.*       THIS INCLUDES THE BTAM MODULES INTO THE NUCLEUS DURING STAGE 2 79439018
.OUT     MEND                                                       MCS 81439018
