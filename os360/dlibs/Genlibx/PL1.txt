         MACRO                                                          00300019
 PL1 &DESIGN=,&PUNCH=NODECK,&TYPERUN=LOAD,&SORCODE=EBCDIC,&SIZE=999999,X00600019
               &OBJLIST=NOLIST,&MSGLEV=FLAGW,&OPT=1,&SORLIST=SOURCE,   *00900019
               &CHARSET=CHAR60,&EXTLIST=NOEXTREF,&ATRLIST=NOATR,       X01200019
               &REFLIST=NOXREF,&SORMGIN=(2,72),&LINECNT=50,&STMDIAG=NOSX01500019
               TMT,&CMPTIME=NOMACRO,&COMPILE=COMP,&MACLIST=SOURCE2,&DEL*01800019
               ETE=,&OBJCODE=OBJOUT,&PAGECTL=,&MCPUNCH=NOMACDECK,&DICTYX02100019
               PE=NOEXTDIC,&OPRINT=OP,&LEVCNT=NONEST,&SYNTXLV=SYNCHKT   02400019
         COPY  SGGBLPAK                                                 02700019
         LCLA  &I,&L,&N,&M,&K                                           03000019
         LCLB  &B,&F,&H                                                 03300019
   LCLC  &C(46)                                                         03600019
 AIF (NOT &SGMENTB(30)).A1                                              03900019
         AGO   .E1                                                      04200019
.A1 AIF  ('&DESIGN' NE 'F').E2                                          04500019
    MNOTE *,'      PL/1 DESIGN LEVEL &DESIGN SPECIFIED'                 04800019
&SGMENTB(30)  SETB  1                                                   05100019
&F   SETB  (&SGPL11B(1))                                                05400019
&SGPL11B(1) SETB ('&DESIGN' EQ 'F')                                     05700019
&I       SETA  0                                                        06000019
&L       SETA  0                                                        06300019
.A2 AIF ('&PUNCH' NE 'NODECK' AND '&PUNCH' NE 'DECK').E3                06600019
&SGPL11B(&I+8) SETB ('&PUNCH' EQ 'DECK')                                06900019
&SGPL11B(&I+9) SETB ('&PUNCH' EQ 'NODECK')                              07200019
    MNOTE *,'      PUNCH OPTION FOR OBJECT DECK IS &PUNCH'              07500019
.A3 AIF ('&TYPERUN' NE 'NOLOAD' AND '&TYPERUN' NE 'LOAD').E4            07800019
&SGPL11B(&I+18) SETB ('&TYPERUN' EQ 'NOLOAD')                           08100019
&SGPL11B(&I+17) SETB ('&TYPERUN' EQ 'LOAD')                             08400019
    MNOTE *,'      RUN OPTION FOR OBJECT PROGRAM IS &TYPERUN'           08700019
.A4 AIF ('&SORCODE' NE 'EBCDIC' AND '&SORCODE' NE 'BCD').E5             09000019
&SGPL11B(&I+4) SETB ('&SORCODE' EQ 'BCD')                               09300019
&SGPL11B(&I+5) SETB ('&SORCODE' EQ 'EBCDIC')                            09600019
    MNOTE *,'      SOURCE CODE CHARACTER SET OPTION IS &SORCODE'        09900019
.A5  AIF  (T'&SIZE  NE  'N').E6                                         10200019
    AIF  (&SIZE LT 45056 OR &SIZE GT 999999).E6                         10500019
&SGPL11A(&L+3) SETA &SIZE                                               10800019
    MNOTE *,'      MAIN STORAGE FOR USE BY COMPILER IS &SIZE BYTES'     11100019
.A6 AIF ('&OBJLIST' NE 'NOLIST' AND '&OBJLIST' NE 'LIST').E7            11400019
&SGPL11B(&I+15) SETB ('&OBJLIST' EQ 'LIST')                             11700019
&SGPL11B(&I+16) SETB ('&OBJLIST' EQ 'NOLIST')                           12000019
    MNOTE *,'      LISTING OPTION FOR OBJECT PROGRAM IS &OBJLIST'       12300019
.A7 AIF ('&MSGLEV' NE 'FLAGW' AND '&MSGLEV' NE 'FLAGE' AND '&MSGLEV' NEX12600019
                'FLAGS').E8                                             12900019
&SGPL11B(&I+12) SETB ('&MSGLEV' EQ 'FLAGW')                             13200019
&SGPL11B(&I+13) SETB ('&MSGLEV' EQ 'FLAGE')                             13500019
&SGPL11B(&I+14) SETB ('&MSGLEV' EQ 'FLAGS')                             13800019
    MNOTE *,'      COMPILATION ERROR MESSAGE OPTION IS &MSGLEV'         14100019
.A8      AIF   ('&OPT' NE '0' AND '&OPT' NE '1' AND '&OPT' NE '2').E9   14400019
&SGPL11B(&I+25) SETB ('&OPT' EQ '0')                                    14700019
&SGPL11B(&I+26) SETB ('&OPT' EQ '1')                                    15000019
&SGPL11B(&I+41) SETB ('&OPT' EQ '2')                                    15300019
    MNOTE *,'      OBJECT PROGRAM OPTIMIZATION OPTION IS &OPT'          15600019
.A9 AIF ('&SORLIST' NE 'SOURCE' AND '&SORLIST' NE 'NOSOURCE').EA        15900019
&SGPL11B(&I+21) SETB ('&SORLIST' EQ 'SOURCE')                           16200019
&SGPL11B(&I+22) SETB ('&SORLIST' EQ 'NOSOURCE')                         16500019
    MNOTE *,'      LISTING OPTION FOR SOURCE PROGRAM IS &SORLIST'       16800019
.A91 AIF ('&MACLIST' NE 'SOURCE2' AND '&MACLIST' NE 'NOSOURCE2').EA1    17100019
&SGPL11B(&I+23) SETB ('&MACLIST' EQ 'SOURCE2')                          17400019
&SGPL11B(&I+24) SETB ('&MACLIST' EQ 'NOSOURCE2')                        17700019
    MNOTE *,'      LISTING OPTION FOR MACROS IS &MACLIST'               18000019
.A92 AIF ('&OBJCODE' NE 'OBJOUT' AND '&OBJCODE' NE 'OBJIN').EA2         18300019
&SGPL11B(&I+27) SETB ('&OBJCODE' EQ 'OBJIN')                            18600019
&SGPL11B(&I+28) SETB ('&OBJCODE' EQ 'OBJOUT')                           18900019
    MNOTE *,'      LISTING OPTION FOR OBJCODE IS &OBJCODE'              19200019
.AA AIF ('&CHARSET' NE 'CHAR60' AND '&CHARSET' NE 'CHAR48').EB          19500019
&SGPL11B(&I+6)  SETB ('&CHARSET' EQ 'CHAR60')                           19800019
&SGPL11B(&I+7)  SETB ('&CHARSET' EQ 'CHAR48')                           20100019
    MNOTE *,'      OPTION FOR NUMBER OF CHARACTERS IS &CHARSET'         20400019
.AB AIF ('&EXTLIST' NE 'NOEXTREF' AND '&EXTLIST' NE 'EXTREF').EC        20700019
&SGPL11B(&I+11) SETB ('&EXTLIST' EQ 'NOEXTREF')                         21000019
&SGPL11B(&I+10) SETB ('&EXTLIST' EQ 'EXTREF')                           21300019
    MNOTE *,'      LISTING OPTION FOR EXTERNAL ENTRIES IS &EXTLIST'     21600019
.AC AIF ('&ATRLIST' NE 'NOATR' AND '&ATRLIST' NE 'ATR').ED              21900019
&SGPL11B(&I+3)  SETB ('&ATRLIST' EQ 'NOATR')                            22200019
&SGPL11B(&I+2)  SETB ('&ATRLIST' EQ 'ATR')                              22500019
    MNOTE *,'      LISTING OPTION FOR IDENTIFIERS IS &ATRLIST'          22800019
.AD AIF ('&REFLIST' NE 'NOXREF' AND '&REFLIST' NE 'XREF').EE            23100019
&SGPL11B(&I+20) SETB ('&REFLIST' EQ 'NOXREF')                           23400019
&SGPL11B(&I+19) SETB ('&REFLIST' EQ 'XREF')                             23700019
    MNOTE *,'      LISTING OPTION FOR REFERENCES IS &REFLIST'           24000019
.AE AIF (T'&SORMGIN(1) NE 'N' OR T'&SORMGIN(2) NE 'N').EF               24300019
  AIF   (&SORMGIN(1) LT 1 OR &SORMGIN(1) GT &SORMGIN(2) OR &SORMGIN(2) *24600019
               GT 100).EF                                               24900019
&SGPL11A(&L+4)  SETA &SORMGIN(1)                                        25200019
&SGPL11A(&L+5)  SETA &SORMGIN(2)                                        25500019
    MNOTE *,'      MARGINS FOR COMPILER SCAN ARE &SORMGIN(1) AND &SORMGX25800019
               IN(2)'                                                   26100019
.AF  AIF  (T'&LINECNT NE 'N').EG                                        26400019
   AIF  (&LINECNT LT 10 OR &LINECNT GT 99).EG                           26700019
&SGPL11A(&L+1)  SETA &LINECNT                                           27000019
    MNOTE *,'      MAXIMUM LINES PER PAGE FOR COMPILER OUTPUT IS &LINECX27300019
               NT'                                                      27600019
.AG AIF ('&STMDIAG' NE 'STMT' AND '&STMDIAG' NE 'NOSTMT').EH            27900019
&SGPL11B(&I+35) SETB ('&STMDIAG' EQ 'STMT')                             28200019
&SGPL11B(&I+36) SETB ('&STMDIAG' EQ 'NOSTMT')                           28500019
    MNOTE *,'      SOURCE PROGRAM MESSAGE OPTION IS &STMDIAG'           28800019
.AG1 AIF ('&CMPTIME' NE 'MACRO' AND '&CMPTIME' NE 'NOMACRO').EI         29100019
&SGPL11B(&I+37) SETB ('&CMPTIME' EQ 'MACRO')                            29400019
&SGPL11B(&I+38) SETB ('&CMPTIME' EQ 'NOMACRO')                          29700019
    MNOTE *,'      COMPILE TIME PREPROCESSOR OPTION IS &CMPTIME'        30000019
.AG2 AIF ('&COMPILE' NE 'COMP' AND '&COMPILE' NE 'NOCOMP').EJ           30300019
&SGPL11B(&I+44) SETB ('&COMPILE' EQ 'COMP')                             30600019
&SGPL11B(&I+45) SETB ('&COMPILE' EQ 'NOCOMP')                           30900019
    MNOTE *,'      COMPILATION PREPROCESSING OPTION IS &COMPILE'        31200019
.AG3 AIF (T'&PAGECTL EQ 'O').AG31                                       31500019
 AIF (T'&PAGECTL NE 'N').EK                                             31800019
 AIF (&PAGECTL LT 1 OR &PAGECTL GT 100).EK                              32100019
 AIF (&PAGECTL GE &SORMGIN(1) AND &PAGECTL LE &SORMGIN(2)).EK           32400019
 AIF (&PAGECTL GT &SORMGIN(1) AND &PAGECTL LT &SORMGIN(2)).EK           32700019
&SGPL11A(6) SETA &PAGECTL                                               33000019
 MNOTE *,' THE ASA CONTROL CHARACTER WILL BE PLACED IN COLUMN &PAGECTL' 33300019
 AGO .AG4                                                               33600019
.AG31 MNOTE *,' NO SPACING CONTROL PROVIDED'                            33900019
&SGPL11A(6) SETA 0                                                      34200019
.AG4 AIF ('&MCPUNCH' NE 'MACDECK' AND '&MCPUNCH' NE 'NOMACDECK').EL     34500019
&SGPL11B(29) SETB ('&MCPUNCH' EQ 'MACDECK')                             34800019
&SGPL11B(30) SETB ('&MCPUNCH' EQ 'NOMACDECK')                           35100019
 MNOTE *,' DEFAULT OPTION FOR GENERATION OF MACRO DECK IS &MCPUNCH'     35400019
.AG5 AIF ('&DICTYPE' NE 'EXTDIC' AND '&DICTYPE' NE 'NOEXTDIC').EM       35700019
&SGPL11B(31) SETB ('&DICTYPE' EQ 'EXTDIC')                              36000019
&SGPL11B(32) SETB ('&DICTYPE' EQ 'NOEXTDIC')                            36300019
 MNOTE *,' TYPE OF DICTIONARY REFERENCING MECHANISM IS &DICTYPE'        36600019
.AG6 AIF ('&OPRINT' NE 'OP' AND '&OPRINT' NE 'NOOP').EN                 36900019
&SGPL11B(33) SETB ('&OPRINT' EQ 'OP')                                   37200019
&SGPL11B(34) SETB ('&OPRINT' EQ 'NOOP')                                 37500019
 MNOTE *,' OPTION FOR WRITING A COMPLETE LIST OF OPTIONS ON SYSPRINT IS*37800021
                &OPRINT'                                                38100021
.AG7 AIF ('&LEVCNT' NE 'NEST' AND '&LEVCNT' NE 'NONEST').EO             38400019
&SGPL11B(39) SETB ('&LEVCNT' EQ 'NEST')                                 38700019
&SGPL11B(40) SETB ('&LEVCNT' EQ 'NONEST')                               39000019
 MNOTE *,' SOURCE LISTING OPTION FOR DEPTH COUNTS OF BLOCK LEVEL AND NE*39300019
               STED DO GROUPS IS &LEVCNT'                               39600019
.AH      AIF   ('&SYNTXLV' NE 'SYNCHKE' AND '&SYNTXLV' NE 'SYNCHKS' ANDX39900019
               '&SYNTXLV' NE 'SYNCHKT').EP                              40200019
&SGPL11B(42)  SETB ('&SYNTXLV' EQ 'SYNCHKE')                            40500019
&SGPL11B(43)  SETB ('&SYNTXLV' EQ 'SYNCHKS')                            40800019
&SGPL11B(157) SETB ('&SYNTXLV' EQ 'SYNCHKT')                            41100019
 MNOTE *,' SYNTAX CHECKING OPTION IS &SYNTXLV'                          41400019
.AH1     AIF   (T'&DELETE EQ 'O').AN                                    41700019
&C(1)    SETC  'ATR'                                                    42000019
&C(2)    SETC  'NOATR'                                                  42300019
&C(3)    SETC  'BCD'                                                    42600019
&C(4)    SETC  'EBCDIC'                                                 42900019
&C(5)    SETC  'CHAR60'                                                 43200019
&C(6)    SETC  'CHAR48'                                                 43500019
&C(7)    SETC  'DECK'                                                   43800019
&C(8)    SETC  'NODECK'                                                 44100019
&C(9)    SETC  'EXTREF'                                                 44400019
&C(10)   SETC  'NOEXTREF'                                               44700019
&C(11)   SETC  'FLAGW'                                                  45000019
&C(12)   SETC  'FLAGE'                                                  45300019
&C(13)   SETC  'FLAGS'                                                  45600019
&C(14)   SETC  'LIST'                                                   45900019
&C(15)   SETC  'NOLIST'                                                 46200019
&C(16)   SETC  'LOAD'                                                   46500019
&C(17)   SETC  'NOLOAD'                                                 46800019
&C(18)   SETC  'XREF'                                                   47100019
&C(19)   SETC  'NOXREF'                                                 47400019
&C(20)   SETC  'SOURCE'                                                 47700019
&C(21)   SETC  'NOSOURCE'                                               48000019
&C(22)   SETC  'SOURCE2'                                                48300019
&C(23)   SETC  ''                                                       48600019
&C(24)   SETC  'OPT'                                                    48900019
&C(25)   SETC  'LINECNT'                                                49200019
&C(26)   SETC  ''                                                       49500019
&C(27)   SETC  'SIZE'                                                   49800019
&C(28)   SETC  'SORMGIN'                                                50100019
&C(29)   SETC  'DUMP'                                                   50400019
&C(30)   SETC  'STMT'                                                   50700019
&C(31)   SETC  'NOSTMT'                                                 51000019
&C(32)   SETC  'MACRO'                                                  51300019
&C(33)   SETC  'NOMACRO'                                                51600019
&C(34)   SETC  'COMP'                                                   51900019
&C(35)   SETC  'NOCOMP'                                                 52200019
&C(36)   SETC  'OBJIN'                                                  52500019
&C(37)   SETC  'OBJOUT'                                                 52800019
&C(38) SETC 'PAGECTL'                                                   53100019
&C(39) SETC 'MACDECK'                                                   53400019
&C(40) SETC ''                                                          53700019
&C(41) SETC 'EXTDIC'                                                    54000019
&C(42) SETC 'NOEXTDIC'                                                  54300019
&C(43) SETC 'OP'                                                        54600019
&C(44) SETC 'NOOP'                                                      54900019
&C(45) SETC 'NEST'                                                      55200019
&C(46) SETC 'NONEST'                                                    55500019
&N       SETA  N'&DELETE                                                55800019
&M       SETA  1                                                        56100019
.AI      ANOP                                                           56400019
&K       SETA  1                                                        56700019
         AIF   ('&DELETE(&M)' EQ '').AM                                 57000019
.AJ      AIF   (&K GT 46).AL                                            57300019
         AIF   ('&C(&K)' EQ '&DELETE(&M)').AK                           57600019
 AIF ('NOSOURCE2' EQ '&DELETE(&M)').AK1                                 57900019
 AIF ('NOMACDECK' EQ '&DELETE(&M)').AK2                                 58200019
 AIF  ('SYNCHKE' EQ '&DELETE(&M)').AK3                                  58500019
 AIF  ('SYNCHKS' EQ '&DELETE(&M)').AK4                                  58800019
 AIF  ('SYNCHKT' EQ '&DELETE(&M)').AK5                                  59100019
&K       SETA  &K+1                                                     59400019
         AGO   .AJ                                                      59700019
.AK MNOTE *,'      COMPILATION OPTION PARAMETER &DELETE(&M) DELETED'    60000019
&SGPL11B(&I+&K+47) SETB 1                                               60300019
         AGO   .AM                                                      60600019
.AK1 MNOTE *,'     COMPILATION OPTION PARAMETER &DELETE(&M) DELETED'    60900019
&SGPL11B(70) SETB 1                                                     61200019
         AGO   .AM                                                      61500019
.AK2 MNOTE *,'     COMPILATION OPTION PARAMETER &DELETE(&M) DELETED'    61800019
&SGPL11B(87) SETB 1                                                     62100019
 AGO .AM                                                                62400019
.AK3 MNOTE *,' COMPILATION OPTION  PARAMETER &DELETE(&M) DELETED'       62700019
&SGPL11B(158)  SETB 1                                                   63000019
         AGO   .AM                                                      63300019
.AK4 MNOTE *,' COMPILATION OPTION  PARAMETER &DELETE(&M) DELETED'       63600019
&SGPL11B(159)  SETB 1                                                   63900019
         AGO   .AM                                                      64200019
.AK5 MNOTE *,' COMPILATION OPTION  PARAMETER &DELETE(&M) DELETED'       64500019
&SGPL11B(160)  SETB 1                                                   64800019
         AGO   .AM                                                      65100019
.AL MNOTE 5,'* * * IEIPL1117 DELETE VALUE &DELETE(&M) INVALID'          65400019
&B       SETB  1                                                        65700019
.AM      ANOP                                                           66000019
&M       SETA  &M+1                                                     66300019
         AIF   (&M LE &N).AI                                            66600019
         AGO   .AO                                                      66900019
.AN   MNOTE *,'      ALL COMPILATION OPTION PARAMETERS MAY BE RESPECIFI*67200019
               ED AT COMPILE TIME'                                      67500019
.AO      AIF   (NOT &B).AP                                              67800019
&SGQUITB SETB  1                                                        68100019
.AP      ANOP                                                           68400019
&SGPL11B(1) SETB (&SGPL11B(1) OR &F)                                    68700019
.AQ      ANOP                                                           69000019
         MEXIT                                                          69300019
.E1 MNOTE 5,'* * * IEIPL1100 PL1 MACRO PREVIOUSLY ENTERRED'             69600019
&SGQUITB SETB 1                                                         69900019
         AGO   .AQ                                                      70200019
.E2 MNOTE 5,'* * * IEIPL1101 DESIGN VALUE &DESIGN INVALID'              70500019
&B       SETB  1                                                        70800019
         AGO   .A2                                                      71100019
.E3 MNOTE 5,'* * * IEIPL1102 PUNCH VALUE &PUNCH INVALID'                71400019
&B       SETB  1                                                        71700019
         AGO   .A3                                                      72000019
.E4 MNOTE 5,'* * * IEIPL1103 TYPERUN VALUE &TYPERUN INVALID'            72300019
&B       SETB  1                                                        72600019
         AGO   .A4                                                      72900019
.E5 MNOTE 5,'* * * IEIPL1104 SORCODE VALUE &SORCODE INVALID'            73200019
&B       SETB  1                                                        73500019
         AGO   .A5                                                      73800019
.E6 MNOTE 5,'* * * IEIPL1105 SIZE VALUE &SIZE INVALID'                  74100019
&B       SETB  1                                                        74400019
         AGO   .A6                                                      74700019
.E7 MNOTE 5,'* * * IEIPL1106 OBJLIST VALUE &OBJLIST INVALID'            75000019
&B       SETB  1                                                        75300019
         AGO   .A7                                                      75600019
.E8 MNOTE 5,'* * * IEIPL1107 MSGLEV VALUE &MSGLEV INVALID'              75900019
&B       SETB  1                                                        76200019
         AGO   .A8                                                      76500019
.E9 MNOTE 5,'* * * IEIPL1108 OPT VALUE &OPT INVALID'                    76800019
&B       SETB  1                                                        77100019
         AGO   .A9                                                      77400019
.EA MNOTE 5,'* * * IEIPL1109 SORLIST VALUE &SORLIST INVALID'            77700019
&B       SETB  1                                                        78000019
         AGO   .A91                                                     78300019
.EA1 MNOTE 5,'* * * IEIPL119 MACLIST VALUE &MACLIST INVALID'            78600019
         AGO   .A92                                                     78900019
.EA2 MNOTE 5,'* * * IEIPL129 OBJCODE VALUE &OBJCODE INVALID'            79200019
&B       SETB  1                                                        79500019
         AGO   .AA                                                      79800019
.EB MNOTE 5,'* * * IEIPL1110 CHARSET VALUE &CHARSET INVALID'            80100019
&B       SETB  1                                                        80400019
         AGO   .AB                                                      80700019
.EC MNOTE 5,'* * * IEIPL1111 EXTLIST VALUE &EXTLIST INVALID'            81000019
&B       SETB  1                                                        81300019
         AGO   .AC                                                      81600019
.ED MNOTE 5,'* * * IEIPL1112 ATRLIST VALUE &ATRLIST INVALID'            81900019
&B       SETB  1                                                        82200019
         AGO   .AD                                                      82500019
.EE MNOTE 5,'* * * IEIPL1113 REFLIST VALUE &REFLIST INVALID'            82800019
&B       SETB  1                                                        83100019
         AGO   .AE                                                      83400019
.EF MNOTE 5,'* * * IEIPL1114 SORMGIN VALUE &SORMGIN(1) OR &SORMGIN(2) IX83700019
               NVALID'                                                  84000019
&B       SETB  1                                                        84300019
         AGO   .AF                                                      84600019
.EG MNOTE 5,'* * * IEIPL1115 LINECNT VALUE &LINECNT INVALID'            84900019
&B       SETB  1                                                        85200019
         AGO   .AG                                                      85500019
.EH MNOTE 5,'* * * IEIPL1116 STMDIAG VALUE &STMDIAG INVALID'            85800019
&B       SETB  1                                                        86100019
         AGO   .AG1                                                     86400019
.EI MNOTE 5,'* * * IEIPL1117 CMPTIME VALUE &CMPTIME INVALID'            86700019
&B       SETB  1                                                        87000019
         AGO   .AG2                                                     87300019
.EJ MNOTE 5,'* * * IEIPL1118 COMPILE VALUE &COMPILE INVALID'            87600019
&B       SETB  1                                                        87900019
 AGO .AG3                                                               88200019
.EK MNOTE 5,'* * * IEIPL1119 PAGECTL VALUE &PAGECTL INVALID'            88500019
&B SETB 1                                                               88800019
 AGO .AG4                                                               89100019
.EL MNOTE 5,'* * * IEIPL1120 MCPUNCH VALUE &MCPUNCH INVALID'            89400019
&B SETB 1                                                               89700019
 AGO .AG5                                                               90000019
.EM MNOTE 5,'* * * IEIPL1121 DICTYPE VALUE &DICTYPE INVALID'            90300019
&B SETB 1                                                               90600019
 AGO .AG6                                                               90900019
.EN MNOTE 5,'* * * IEIPL1122 OPRINT VALUE &OPRINT INVALID'              91200019
&B SETB 1                                                               91500019
 AGO .AG7                                                               91800019
.EO MNOTE 5,'* * * IEIPL1123 LEVCNT VALUE &LEVCNT INVALID'              92100019
&B SETB 1                                                               92400019
         AGO   .AH                                                      92700019
.EP MNOTE 5,'* * * IEIPL124 SYNTXLV VALUE &SYNTXLV INVALID'             93000019
&B       SETB 1                                                         93300019
         AGO   .AH1                                                     93600019
         MEND                                                           93900019
