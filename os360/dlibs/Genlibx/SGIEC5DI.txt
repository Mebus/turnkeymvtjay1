         MACRO                                                          00020000
         SGIEC5DI                                                       00040000
         COPY  SGGBLPAK                                                 00060000
         AIF   (NOT &GETB(3)).ALLGEN                                    00066020
         AIF   (&SGDCLSB(69) OR &SGDCLSB(70)).RPSSUP                    00072020
         MEXIT                                                          00078020
.ALLGEN  ANOP                                                           00084020
         PUNCH ' COPY OUTDD=SVCLIB,INDD=DM509'                          00090019
         PUNCH ' SELECT MEMBER=(IGG019KC,IGG019KE,IGG019KG)'     S20201 00100020
         PUNCH ' SELECT MEMBER=(IGG019KR,IGG0193G,IGG0193F)'     S20201 00110020
         PUNCH ' SELECT MEMBER=(IGG019KA,IGG019KJ,IGG019KL)'     S20201 00120020
         PUNCH ' SELECT MEMBER=(IGG019KQ,IGG019KU,IGG0193A,IGG0193C)'   00140021
         PUNCH ' SELECT MEMBER=(IGG0203A,IGG019DC,IGG0191L)'     S20201 00190020
         PUNCH ' SELECT MEMBER=(IGG019BR,IGG019BS,IGG019BT,IGG0199L)'   00230019
         PUNCH ' SELECT MEMBER=(IGG0191M,IGG019KF,IGG019KH)'     S20201 00240020
         PUNCH ' SELECT MEMBER=(IGG019LG)'                       S20201 00250020
 PUNCH ' SELECT MEMBER=(IGC0005C)'                                      00260000
         PUNCH ' SELECT MEMBER=(IGG019KW,IGG019KY,IGG019LA,IGG019LC,IGGC00280000
               019LI)'                                                  00300000
         PUNCH ' SELECT MEMBER=(IGG019LE,IGG0193E)'                     00320000
  AIF    (&SGDCLSB(69) EQ 1 OR &SGDCLSB(70) EQ 1).RPSSUP         S20201 00321020
         PUNCH ' SELECT MEMBER=(IGG019DA,IGG019DB,IGG019DD)'     S20201 00322020
         PUNCH ' SELECT MEMBER=(IGG019KN)'                              00323020
         PUNCH ' SELECT MEMBER=(IGG019KI,IGG019KK,IGG019KM)'     S20201 00324020
         PUNCH ' SELECT MEMBER=(IGG019KO)'                       S20201 00325020
         MEXIT                                                          00326020
.RPSSUP  ANOP                                                    S20201 00327020
 PUNCH ' COPY  OUTDD=SVCLIB,INDD=DM509'                                 00327520
         PUNCH ' SELECT MEMBER=((IGGR19DA,IGG019DA,R),(IGGR19DB,IGG019D100328420
               B,R))'                                                   00329320
         PUNCH ' SELECT MEMBER=((IGGR19DD,IGG019DD,R),(IGGR19KN,IGG019K100330220
               N,R))'                                                   00331120
         PUNCH ' SELECT MEMBER=((IGGR19KI,IGG019KI,R),(IGGR19KK,IGG019K100332020
               K,R))'                                                   00332920
         PUNCH ' SELECT MEMBER=((IGGR19KM,IGG019KM,R),(IGGR19KO,IGG019K100333820
               O,R))'                                                   00334720
         MEND                                                           00340000
