         MACRO                                                          05000021
         SGIKF200                                                       10000021
         COPY  SGGBLPAK                                                 15000021
         LCLA  &BUFS                                                    20000021
&BUFS    SETA  &SGCBLUA(3)+7                                            25000021
&BUFS    SETA  &BUFS/8                                                  30000021
&BUFS    SETA  &BUFS*8                                                  35000021
&SGCTRLC(6)  SETC  'IKFCBL01'                                           40000021
         COPY  SGASMPAK                                                 45000021
 PUNCH ' SGIKF000 &SGCBLUB(3),&SGCBLUB(5),&SGCBLUB(6),&SGCBLUB(7),&SGCBX50000021
               LUB(1),&SGCBLUB(18),&SGCBLUB(10)&SGCBLUB(11),           X55000021
                                                  X'                    60000021
 PUNCH  '               &SGCBLUA(1),&BUFS,&SGCBLUA(2),&SGCBLUB(4),&SGCBX65000021
               LUB(15),&SGCBLUB(12),&SGCBLUB(16),&SGCBLUB(17),&SGCBLUB(X70000021
               8),&SGCBLUB(9),&SGCBLUB(14)'                             75000021
   PUNCH  ' END'                                                        80000021
   PUNCH  '/*'                                                          85000021
         MEND                                                           90000021
