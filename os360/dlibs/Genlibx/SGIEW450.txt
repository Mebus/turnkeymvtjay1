         MACRO                                                          04000000
         SGIEW450                                                       08000000
         COPY  SGGBLPAK                                                 12000000
&SGCTRLC(8) SETC '&SGCTRLC(31)'    SET LINKLIB DEVICE                   16000000
&SGCTRLC(7) SETC '&SGCTRLC(32)'    SET LINKLIB VOLID                    20000000
&SGCTRLC(9) SETC 'LINKLIB'         SET SYSLMOD NAME                     24000000
&SGCTRLC(10) SETC ',RENT'          SET                                  28000000
&SGCTRLC(11) SETC ',REFR'            LINKAGE EDITOR                     32000000
&SGCTRLC(12) SETC ',REUS'              ATTRIBUTES                       36000000
&SGCTRLC(13) SETC ',OL'                                                 40000000
         COPY  SGLEDPK1                                                 44000000
 PUNCH '//LD547  DD DSNAME=SYS1.LD547,DISP=SHR,VOLUME=(,RETAIN)'        48000019
         COPY  SGLEDPK2                                                 52000000
 PUNCH ' INCLUDE LD547(IEWLDIOC,IEWLDREL,IEWLDLIB,IEWLDIDY)'            56000020
 PUNCH ' INCLUDE SYSPUNCH(IEWLDDEF)'                                    60000000
 PUNCH ' ENTRY IEWLIOCA'                                                64000000
 PUNCH ' ALIAS IEWLOAD'                                                 66000020
 PUNCH ' NAME IEWLOADR(R)'                                              68000000
 PUNCH '/*'                                                             72000000
         MEND                                                           76000000
