         MACRO                                                          00020000
          FORTLIB &UNIT=,&VOLNO=,&DESIGN=,&UNTABLE=08,&OBJERR=06,      X00040000
               &ONLNRD=05,&ONLNPCH=07,&BOUNDRY=ALIGN,&OPTERR=EXCLUDE,&A*00050017
               DDNTRY=                                                  00060017
         COPY  SGGBLPAK                                                 00080000
         LCLB  &A                                                       00100000
         AIF   (NOT &SGMENTB(39)).A1                                    00140000
    MNOTE 5,'* * * IEIFOL101 FORTLIB MACRO PREVIOUSLY USED'             00160000
&SGQUITB SETB  1                                                        00180000
         MEXIT                                                          00200000
.A1      ANOP                                                           00220000
&SGMENTB(39)  SETB  1                                                   00240000
         AIF   (N'&UNIT EQ N'&VOLNO).A2                                 00260000
    MNOTE 5,'* * * IEIFOL102 UNIT OR VOLNO VALUE NOT SPECIFIED'         00280000
&A       SETB  1                                                        00300000
         AGO   .B1                                                      00320000
.A2      AIF   ('&UNIT' EQ '').B1                                       00340000
         AIF   (K'&UNIT LE 8).A3                                        00360000
    MNOTE 5,'* * * IEIFOL103 UNIT VALUE &UNIT INVALID'                  00380000
&A       SETB  1                                                        00400000
.A3      AIF   (K'&VOLNO LE 6).B1                                       00420000
    MNOTE 5,'* * * IEIFOL104 VOLNO VALUE &VOLNO INVALID'                00440000
&A       SETB  1                                                        00460000
.B1      AIF   ('&DESIGN' NE '').B2                                     00480000
    MNOTE 5,'* * * IEIFOL105 DESIGN VALUE NOT SPECIFIED'                00500000
&A       SETB  1                                                        00520000
         AGO   .B9                                                      00540000
.B2     AIF    ('&DESIGN' NE 'E').B3                                    00560000
&SGFORLB(1)  SETB  1                                                    00580000
 AGO  .B9B                                                              00600000
.B3      AIF   ('&DESIGN' NE 'G').B4                                    00620000
&SGFORLB(2)  SETB  1                                                    00640000
         AGO   .B9                                                      00660000
.B4      AIF   ('&DESIGN' NE 'H').B8                                    00680000
&SGFORLB(3)  SETB  1                                                    00700000
         AGO   .B9                                                      00720000
.B8 MNOTE 5,'* * * IEIFOL106 DESIGN VALUE &DESIGN INVALID'              00740000
&A       SETB  1                                                        00760000
.B9 AIF ('&BOUNDRY' NE 'ALIGN' AND '&BOUNDRY' NE 'NOALIGN').B9A         00780000
&SGFORLB(4) SETB ('&BOUNDRY' EQ 'ALIGN')                                00800000
 AGO  .B9B                                                              00820000
.B9A MNOTE 5,'* * * IEFOL114 BOUNDRY VALUE &BOUNDRY INVALID'            00840000
&A  SETB  1                                                             00860000
.B9B     AIF   (T'&UNTABLE NE 'N').BB                                   00870021
         AIF  (K'&UNTABLE EQ 2 AND &UNTABLE GE 8 AND &UNTABLE LE 99).C2 00880021
.BB      ANOP                                                           00890021
    MNOTE 5,'* * * IEIFOL107 UNTABLE VALUE &UNTABLE INVALID'            00900000
&A       SETB  1                                                        00920000
.C2      AIF   (T'&OBJERR NE 'N').CC                                    00930021
         AIF   (K'&OBJERR EQ 2 AND '&OBJERR' LE '&UNTABLE').C5          00940021
.CC MNOTE 5,'* * * IEIFOL109 OBJERR VALUE &OBJERR INVALID'              00950021
&A       SETB  1                                                        00980000
.C5      AIF   (T'&ONLNRD NE 'N').CA                                    00990021
         AIF   (K'&ONLNRD EQ 2 AND '&ONLNRD' LE '&UNTABLE' AND '&ONLNRD*01000021
               ' NE '&OBJERR').C6                                       01010021
         AGO   .CA                                                      01040000
.C6      AIF   (NOT &SGFORLB(1) OR &ONLNRD EQ 5).C7                     01060000
.CA MNOTE 5,'* * * IEIFOL111 ONLNRD VALUE &ONLNRD INVALID'              01080000
&A       SETB  1                                                        01100000
.C7      AIF   (T'&ONLNPCH NE 'N').CB                                   01110021
         AIF   (K'&ONLNPCH EQ 2 AND '&ONLNPCH' LE '&UNTABLE' AND '&ONLN*01120021
               PCH' NE '&OBJERR' AND '&ONLNPCH' NE '&ONLNRD').C8        01130021
         AGO   .CB                                                      01160000
.C8      AIF   (NOT &SGFORLB(1) OR &ONLNPCH EQ 7).D1                    01180000
.CB MNOTE 5,'* * * IEIFOL113 ONLNPCH VALUE &ONLNPCH INVALID'            01200000
 AGO .D1                                                                01210017
.C9      ANOP                                                           01220000
&SGQUITB SETB  1                                                        01240000
         MEXIT                                                          01260000
.D1 AIF ('&OPTERR' NE 'EXCLUDE' AND '&OPTERR' NE 'INCLUDE').D1E         01263017
 AIF ('&OPTERR' EQ 'INCLUDE' AND '&DESIGN' EQ 'E').D1E                  01266017
&SGFORLB(5) SETB ('&OPTERR' EQ 'INCLUDE')                               01269017
 AGO .D1A                                                               01272017
.D1E MNOTE 5,'* * * IEIFOL115 OPTERR VALUE &OPTERR INVALID'             01275017
&A SETB 1                                                               01278017
.D1A AIF (T'&ADDNTRY EQ 'O').D1C                                        01281017
         AIF   (T'&ADDNTRY NE 'N').D1AA                                 01282021
  AIF ('&ADDNTRY' GE '0' AND '&ADDNTRY' LE '598' AND '&OPTERR' EQ 'INCL*01284017
               UDE').D1B                                                01287017
.D1AA    ANOP                                                           01288021
 MNOTE 5,'* * * IEIFOL116 ADDNTRY VALUE &ADDNTRY INVALID'               01290017
&A SETB 1                                                               01293017
.D1B AIF (&A).C9                                                        01296017
&SGFORLA(5)  SETA  &ADDNTRY                                             01297017
.D1C ANOP                                                               01298017
&SGFORLA(1)  SETA  &UNTABLE                                             01300000
&SGFORLA(2)  SETA  &OBJERR                                              01320000
&SGFORLA(3)  SETA  &ONLNRD                                              01340000
&SGFORLA(4)  SETA  &ONLNPCH                                             01360000
         AIF   ('&UNIT' NE '').D2                                       01380000
    MNOTE *,'      THE LEVEL &DESIGN FORTRAN LIBRARY WILL RESIDE ON SYSX01400000
               RES'                                                     01420000
         AGO   .D3                                                      01440000
.D2      ANOP                                                           01460000
&SGCTRLC(21)  SETC  '&UNIT'                                             01480000
&SGCTRLC(22)  SETC  '&VOLNO'                                            01500000
    MNOTE *,'      FORTRAN LIBRARY LEVEL &DESIGN SPECIFIED'             01520000
.D3 MNOTE *,'      I/O UNIT TABLE WILL CONTAIN &UNTABLE LOGICAL UNITS'  01540000
   AIF  ('&DESIGN' NE 'E').D4                                           01560000
    MNOTE *,'      UNIT &OBJERR USED FOR ERROR MESSAGES AND DUMPS'      01580000
    AGO   .END                                                          01600000
.D4   ANOP                                                              01620000
    MNOTE *,'      UNIT &OBJERR USED FOR ''PRINT'' STATEMENTS,'         01640000
    MNOTE *,'          ERROR MESSAGES AND DUMPS'                        01660000
    MNOTE *,'      UNIT &ONLNRD USED FOR ''READ'' STATEMENTS'           01680000
    MNOTE *,'      UNIT &ONLNPCH USED FOR ''PUNCH'' STATEMENTS'         01700000
    MNOTE *,'      OBJECT TIME ERROR HANDLING FACILITY IS &OPTERR'      01702017
 AIF (NOT &SGFORLB(5)).D4A     OPTERR=INCLUDE NOT SPECIFIED             01704017
 AIF (T'&ADDNTRY EQ 'O').D4A     ADDNTRY NOT SPECIFIED                  01706017
 AIF (&A).D4A                   ADDNTRY IN ERROR                        01708017
    MNOTE *,'      &ADDNTRY ADDITIONAL ERROR ENTRIES IN OPTION TABLE'   01710017
.D4A  ANOP                                                              01712017
   AIF ('&BOUNDRY' EQ 'ALIGN').D5                                       01720000
    MNOTE *,'      BOUNDARY ALIGNMENT NOT REQUESTED'                    01740000
 AGO .END                                                               01760000
.D5 MNOTE *,'      BOUNDARY ALIGNMENT SPECIFIED'                        01780000
.END  MEND                                                              01800000
