         MACRO                                                          00020000
 ALGOL &PUNCH=NODECK,&TYPERUN=LOAD,&SORCODE=EBCDIC,&SORLIST=SOURCE,    *00040000
               &SIZE=45056,&PRECISN=SHORT                               00060000
         COPY  SGGBLPAK                                                 00080000
 AIF (&SGMENTB(25)).E1                                                  00100000
&SGMENTB(25)  SETB 1                                                    00120000
.A1     ANOP                                                            00140000
 AIF ('&PUNCH' NE 'NODECK' AND '&PUNCH' NE 'DECK').E2                   00160000
   MNOTE *,'   PUNCH OPTION IS &PUNCH'                                  00180000
&SGALGOB(1)  SETB  ('&PUNCH' EQ 'DECK')                                 00200000
.A2     ANOP                                                            00220000
 AIF ('&TYPERUN' NE 'LOAD' AND '&TYPERUN' NE 'NOLOAD').E3               00240000
   MNOTE *,'   TYPERUN OPTION IS &TYPERUN'                              00260000
&SGALGOB(2)  SETB  ('&TYPERUN' EQ 'NOLOAD')                             00280000
.A3     ANOP                                                            00300000
 AIF ('&SORCODE' NE 'EBCDIC' AND '&SORCODE' NE 'ISO').E4                00320000
   MNOTE *,'   SORCODE OPTION IS &SORCODE'                              00340000
&SGALGOB(3)  SETB  ('&SORCODE' EQ 'ISO')                                00360000
.A4     ANOP                                                            00380000
 AIF ('&SORLIST' NE 'SOURCE' AND '&SORLIST' NE 'NOSOURCE').E5           00400000
   MNOTE *,'   SORLIST OPTION IS &SORLIST'                              00420000
&SGALGOB(4)  SETB  ('&SORLIST' EQ 'NOSOURCE')                           00440000
.A5     ANOP                                                            00460000
 AIF (&SIZE LT 45056 OR &SIZE GT 999999 OR T'&SIZE NE 'N').E6           00480021
   MNOTE *,'   SIZE SPECIFIED IS &SIZE BYTES OF MAIN STORAGE'           00500000
&SGALGOA(1)  SETA  &SIZE                                                00520000
.A6     ANOP                                                            00540000
 AIF ('&PRECISN' NE 'SHORT' AND '&PRECISN' NE 'LONG').E7                00560000
   MNOTE *,'   PRECISN OPTION IS &PRECISN'                              00580000
&SGALGOB(5)  SETB  ('&PRECISN' EQ 'LONG')                               00600000
.A7     ANOP                                                            00620000
        MEXIT                                                           00640000
.E1 MNOTE 5,'* * * IEIALG101 ALGOL MACRO PREVIOUSLY USED'               00660000
&SGQUITB  SETB 1                                                        00680000
         AGO  .A1                                                       00700000
.E2 MNOTE 5,'* * * IEIALG102 PUNCH VALUE &PUNCH INVALID'                00720000
&SGQUITB  SETB 1                                                        00740000
         AGO  .A2                                                       00760000
.E3 MNOTE 5,'* * * IEIALG103 TYPERUN VALUE &TYPERUN INVALID'            00780000
&SGQUITB  SETB 1                                                        00800000
         AGO  .A3                                                       00820000
.E4 MNOTE 5,'* * * IEIALG104 SORCODE VALUE &SORCODE INVALID'            00840000
&SGQUITB  SETB 1                                                        00860000
         AGO  .A4                                                       00880000
.E5 MNOTE 5,'* * * IEIALG105 SORLIST VALUE &SORLIST INVALID'            00900000
&SGQUITB  SETB 1                                                        00920000
         AGO  .A5                                                       00940000
.E6 MNOTE 5,'* * * IEIALG106 SIZE VALUE &SIZE INVALID'                  00960000
&SGQUITB  SETB 1                                                        00980000
         AGO  .A6                                                       01000000
.E7 MNOTE 5,'* * * IEIALG107 PRECISN VALUE &PRECISN INVALID'            01020000
&SGQUITB  SETB 1                                                        01040000
         AGO  .A7                                                       01060000
        MEND                                                            01080000
