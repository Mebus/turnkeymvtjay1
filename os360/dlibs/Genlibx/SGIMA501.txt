         MACRO                                                          03000020
         SGIMA501                                                       06000020
.*A 691000                                                        M0801 06002021
.*A 690000-690500                                                S21014 06010021
.*C 240000                                                       S20205 06050020
         COPY SGGBLPAK              DEFINE SYSTEM GLOBAL SYMBOLS        09000020
 PUNCH ' COPY OUTDD=LINKLIB,INDD=DN554'                                 21000020
 PUNCH ' SELECT MEMBER=(IMAPTFLE,IMAPTF01,IMAPTF02)'             S20205 24000020
 PUNCH ' SELECT MEMBER=(IMASPZAP)'                                      30000020
 PUNCH ' SELECT MEMBER=(IMBMDMAP)'                                      33000020
 PUNCH ' SELECT MEMBER=(IMDPRLPA,IMDPRQCB,IMDPRDPS,IMDPRFSR)'           57000020
 PUNCH ' SELECT MEMBER=(IMDPRFUR,IMDPRFXT,IMDPRPMS,IMDPRNUC)'           60000020
 PUNCH ' SELECT MEMBER=(IMDPRPAL,IMDPRFUB,IMDPRPCR,IMDPRPDR)'           63000020
 PUNCH ' SELECT MEMBER=(IMDPRPJB,IMDDREAD,IMDTREAD,IMDPRLOD)'           66000020
 PUNCH ' SELECT MEMBER=(IMDPROOT,IMDSYS00,IMDSYS02,IMDSYS03)'    S21014 69000021
 PUNCH ' SELECT MEMBER=(IMDPRMST,IMDPRTSO,IMDPRSWP)'             S21014 69050021
 PUNCH ' SELECT MEMBER=(IMDUSRFF)'                                M0801 69100021
         MEND                                                           75000021
