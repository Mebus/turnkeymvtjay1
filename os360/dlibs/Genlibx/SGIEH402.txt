         MACRO                                                          00020000
         SGIEH402                                                       00040000
.* 012400                                                        A44303 00040421
.* 000637                                                      @PTM0054 00040821
.* 000732                                                        S20201 00042020
.*1922009200,009600,010000,012672,012699,012726,012753,012771      000I 00046018
.*1922012789                                                       000I 00052018
.* 012400                                                        A20878 00056018
.*C000638,000640,000680,000682,000702,000704,000724,000726       A57732 00057021
.* SYSGEN MACRO CHANGED FOR UTILITY TRANSFER IN RELEASE 19              00058019
.* 000744,000768                                                 A33288 00058120
.*                                                               A32407 00058220
.* 005900                                                         P3770 00058320
.*A000738-000748                                               @SA67449 00058521
.*C000749,000751,000753,000755,000757,000759,000761,000753     @SA67449 00058621
.*C000765,000767,000769                                        @SA67449 00058721
.*A000621,000622,000625,000643,000648,000651,000685,000686     @SA69575 00059121
.*A000689,000707,000708,000711                                 @SA69575 00059521
.*C000631,000636,000654,000658,000660,000662,000670,000675     @SA69575 00059921
.*C000692,000697,000714,000719                                 @SA69575 00060021
.*D000627-000630,000653,000655,000657,000664,000668,000672     @SA69575 00060421
.*D000691,000694,000713,000716                                          00060821
         COPY  SGGBLPAK                                                 00061121
&SGCTRLC(7)   SETC   '&SGCTRLC(32)'   VOLNO LINKLIB                     00061221
&SGCTRLC(8)   SETC   '&SGCTRLC(31)'   UNITNAME LINKLIB                  00061321
&SGCTRLC(9) SETC 'LINKLIB'             SET DATA SET NAME                00061421
&SGCTRLC(6) SETC 'IEBPTPCH'                                             00061521
&SGCTRLC(10) SETC ',OVLY'                                               00061621
&SGCTRLC(11)   SETC  ',LET'                                        000H 00061721
 COPY SGLEDPK1                                                          00061821
 PUNCH '//UT506 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.UT506'         00061921
 COPY SGLEDPK2                                                          00062021
 PUNCH ' INCLUDE UT506(IEBPPUN1,IEBPPMSG)'                              00062121
 PUNCH ' INCLUDE UT506(IEBPPAL1,IEBCCS02)'                     @SA69575 00062221
 PUNCH ' INCLUDE UT506(IEBPPCH1)'                              @SA69575 00062321
 AIF (&SGUTSZA GT 23551).A3                                    @SA69575 00062421
 PUNCH ' INSERT IEBPPTCH,IEBPPMSG'                             @SA69575 00062521
 PUNCH ' OVERLAY 1'                                            @SA69575 00062621
 PUNCH ' INSERT IEBPPAL1,IEBCCS02'                             @SA69575 00062921
 PUNCH ' OVERLAY 1'                                                     00063219
 PUNCH ' INSERT IEBPPCH1'                                      @SA69575 00063621
.A3 ANOP                                                       @PTM0054 00063721
 PUNCH ' ENTRY PRPCH        ** CANCEL OVERLAY FOR IEBPTPCH   **' A57732 00063821
 PUNCH ' NAME   IEBPTPCH(R) ** WHEN MSG IEW0201 IS GENERATED **' A57732 00064021
 PUNCH ' INCLUDE UT506(IEBGENRT,IEBGMESG)'                              00064219
 PUNCH ' INCLUDE UT506(IEBCCS02,IEBGSCAN)'                     @SA69575 00064321
 PUNCH ' INCLUDE UT506(IEBGENR3,IEBCONP2,IEBCONH2)'            @SA69575 00064421
 PUNCH ' INCLUDE UT506(IEBCONZ2,IEBEDIT2,IEBLENP2)'            @SA69575 00064521
 PUNCH ' INCLUDE UT506(IEBMOVE2)'                              @SA69575 00064621
 PUNCH ' INCLUDE UT506(IEBGENS3)'                              @SA69575 00064721
 PUNCH ' INCLUDE UT506(IEBGEN03)'                              @SA69575 00064821
 AIF (&SGUTSZA GT 32851).A5                                    @SA69575 00064921
 PUNCH ' INSERT IEBGENER,IEBMESG'                              @SA69575 00065021
 PUNCH ' OVERLAY 1'                                            @SA69575 00065121
 PUNCH ' INSERT IEBCCS02,IEBGSCAN'                             @SA69575 00065521
 PUNCH ' OVERLAY 1'                                            @SA69575 00065621
 PUNCH ' INSERT IEBGENR3,IEBCONP2,IEBCONH2'                    @SA69575 00065721
 PUNCH ' INSERT IEBCONZ2,IEBEDIT2,IEBLENP2'                    @SA69575 00066121
 PUNCH ' INSERT IEBMOVE2'                                      @SA69575 00066221
 PUNCH ' OVERLAY 2'                                                000H 00066619
 PUNCH ' INSERT IEBGENS3'                                      @SA69575 00067021
 PUNCH ' OVERLAY 2'                                                000H 00067419
 PUNCH ' INSERT IEBGEN03'                                      @SA69575 00067521
.A5  ANOP                                                      @SA69575 00067621
 PUNCH ' ENTRY IEBGENER     ** CANCEL OVERLAY FOR IEBGENER   **' A57732 00068021
 PUNCH ' NAME   IEBGENER(R) ** WHEN MSG IEW0201 IS GENERATED **' A57732 00068221
 PUNCH ' INCLUDE UT506(IEBCROOT,IEBCOMPM,IEBCULET)'                000H 00068419
 PUNCH ' INCLUDE UT506(IEBCCS02,IEBCANAL)'                     @SA69575 00068521
 PUNCH ' INCLUDE UT506(IEBCMAIN,IEBCQSAM)'                     @SA69575 00068621
 AIF (&SGUTSZA GT 30051).A9                                    @SA69575 00068721
 PUNCH ' INSERT COMPARE,MESSLIST,USERLAB'                      @SA69575 00068821
 PUNCH ' OVERLAY 1'                                            @SA69575 00068921
 PUNCH ' INSERT IEBCCS02,ANALY'                                @SA69575 00069321
 PUNCH ' OVERLAY 1'                                                     00069619
 PUNCH ' INSERT MAIN,QSAM'                                     @SA69575 00069721
.A9   ANOP                                                     @SA69575 00069821
 PUNCH ' ENTRY COMPARE      ** CANCEL OVERLAY FOR IEBCOMPR   **' A57732 00070221
 PUNCH ' NAME   IEBCOMPR(R) ** WHEN MSG IEW0201 IS GENERATED **' A57732 00070421
 PUNCH ' INCLUDE UT506(IEBUPDTE,IEBUPLOG,IEBUPDT2,IEBUPXIT)'       000H 00070619
 PUNCH ' INCLUDE UT506(IEBUPNIT)'                              @SA69575 00070721
 PUNCH ' INCLUDE UT506(IEBASCAN,IEBBSCAN)'                     @SA69575 00070821
 AIF (&SGUTSZA GT 35974).A11                                   @SA69575 00070921
 PUNCH ' INSERT IEBUPDTE,IEBUPLOG,IEBUPDT2,IEBUPXIT'           @SA69575 00071021
 PUNCH ' OVERLAY 1'                                            @SA69575 00071121
 PUNCH ' INSERT IEBUPNIT'                                      @SA69575 00071521
 PUNCH ' OVERLAY 1'                                                000H 00071819
 PUNCH ' INSERT IEBASCAN,IEBBSCAN'                             @SA69575 00071921
.A11 ANOP                                                      @SA69575 00072021
 PUNCH ' ENTRY IEBUPDTE     ** CANCEL OVERLAY FOR IEBUPDTE   **' A57732 00072421
 PUNCH ' NAME   IEBUPDTE(R) ** WHEN MSG IEW0201 IS GENERATED **' A57732 00072621
 PUNCH ' INCLUDE UT506(IEBDSCPY,IEBVMS)'                       @SA67449 00073821
 PUNCH ' INCLUDE UT506(IEBBAM,IEBDV1)'                         @SA67449 00073921
 PUNCH ' INCLUDE UT506(IEBSCN)'                                @SA67449 00074021
 PUNCH ' INCLUDE UT506(IEBVCT)'                                @SA67449 00074121
 PUNCH ' INCLUDE UT506(IEBIOE)'                                @SA67449 00074221
 PUNCH ' INCLUDE UT506(IEBDRD,IEBWSU,IEBDWR)'                  @SA67449 00074321
 PUNCH ' INCLUDE UT506(IEBVDM)'                                @SA67449 00074421
 PUNCH ' INCLUDE UT506(IEBMCM)'                                @SA67449 00074521
 PUNCH ' INCLUDE UT506(IEBDRB)'                                @SA67449 00074621
 PUNCH ' INCLUDE UT506(IEBVTM)'                                @SA67449 00074721
 PUNCH ' INCLUDE UT506(IEBVTT)'                                @SA67449 00074821
 PUNCH ' INSERT IEBDSCPY,IEBDSCP2,IEBDSMCA,IEBVMS,IEBVMTXT'    @SA67449 00074921
 PUNCH ' OVERLAY 1'                                                     00075021
 PUNCH ' INSERT IEBBAM,IEBDV1,IEBDV1MG'                        @SA67449 00075121
 PUNCH ' OVERLAY 1'                                                     00075221
 PUNCH ' INSERT IEBSCN'                                        @SA67449 00075321
 PUNCH ' OVERLAY 1'                                                     00075421
 PUNCH ' INSERT IEBVCT'                                        @SA67449 00075521
 PUNCH ' OVERLAY 1'                                                     00075621
 PUNCH ' INSERT IEBIOE,IEBIOEMG'                               @SA67449 00075721
 PUNCH ' OVERLAY 1'                                                     00075821
 PUNCH ' INSERT IEBDRD,IEBDRDR,IEBWSU,IEBWSUMG,IEBDWR,IEBDWRR' @SA67449 00075921
 PUNCH ' OVERLAY 2'                                                     00076021
 PUNCH ' INSERT IEBVDM'                                        @SA67449 00076121
 PUNCH ' OVERLAY 2'                                                     00076221
 PUNCH ' INSERT IEBMCM'                                        @SA67449 00076321
 PUNCH ' OVERLAY 2'                                                     00076421
 PUNCH ' INSERT IEBDRB'                                        @SA67449 00076521
 PUNCH ' OVERLAY 2'                                                     00076621
 PUNCH ' INSERT IEBVTM'                                        @SA67449 00076721
 PUNCH ' OVERLAY 2'                                                     00076821
 PUNCH ' INSERT IEBVTT'                                        @SA67449 00076921
 PUNCH ' ENTRY IEBDSCPY'                                                00077121
 PUNCH ' NAME    IEBCOPY(R)'                                            00077219
 PUNCH '/*'                                                        000H 00077419
&SGCTRLC(6)   SETC  ''                                                  00080000
&SGCTRLC(7)   SETC  '&SGCTRLC(32)'                                      00100000
&SGCTRLC(8)   SETC  '&SGCTRLC(31)'                                      00120000
&SGCTRLC(9)   SETC  'LINKLIB'                                           00140000
&SGCTRLC(10)  SETC ',REUS'                                              00160000
&SGCTRLC(11)  SETC ',LET'                                               00180000
 COPY SGLEDPK1                                                          00190017
 PUNCH '//UT506 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.UT506'         00200019
 COPY SGLEDPK2                                                          00210017
 PUNCH ' INCLUDE UT506(IEHMVESI)'                                       00220017
 PUNCH ' NAME       IEHMVESI(R)      '                                  00240000
 PUNCH ' INCLUDE UT506(IEHMVESH)'                                       00260017
 PUNCH ' NAME       IEHMVESH(R)      '                                  00280000
 PUNCH ' INCLUDE UT506(IEHMVESU) '                                      00300017
 PUNCH ' NAME       IEHMVESU(R)      '                                  00320000
 PUNCH ' INCLUDE UT506(IEHMVESO)'                                       00340017
 PUNCH ' NAME       IEHMVESO(R)      '                                  00360000
 PUNCH ' INCLUDE UT506(IEHMVESP)'                                       00380017
 PUNCH ' NAME       IEHMVESP(R)      '                                  00400000
 PUNCH ' INCLUDE UT506(IEHMVESC)'                                       00420017
 PUNCH ' NAME       IEHMVESC(R)      '                                  00440000
 PUNCH ' INCLUDE UT506(IEHMVESK)'                                       00460017
 PUNCH ' NAME       IEHMVESK(R)      '                                  00480000
 PUNCH ' INCLUDE UT506(IEHMVESR)'                                       00500017
 PUNCH ' NAME       IEHMVESR(R)      '                                  00520000
 PUNCH ' INCLUDE UT506(IEHMVESJ)'                                       00540017
 PUNCH ' NAME       IEHMVESJ(R)      '                                  00560000
 PUNCH '/*'                                                             00566019
&SGCTRLC(6) SETC 'IEHMVXSF'                                             00572019
&SGCTRLC(9)    SETC  'LINKLIB'                                          00578019
&SGCTRLC(10) SETC ',REUS'                                               00584019
 COPY SGLEDPK1                                                          00596019
 PUNCH '//UT506 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.UT506'         00602019
 COPY SGLEDPK2                                                          00608019
 PUNCH ' INCLUDE UT506(IEHMVXSF,IEHMVSSF)'                         I276 00614019
 PUNCH ' ENTRY   IEHMVXSF'                                              00620019
 PUNCH ' NAME    IEHMVXSF(R)'                                           00626019
 PUNCH ' INCLUDE UT506(IEHMVSRX,IEHMVSRM,IEHMVSSY)'                     00632019
 PUNCH ' INCLUDE UT506(IEHMVMSY)'                                       00638019
 PUNCH ' ENTRY   IEHMVESY'                                              00644019
 PUNCH ' NAME    IEHMVESY(R)'                                           00650019
 PUNCH ' INCLUDE UT506(IEHMVSSX,IEHMVSRY,IEHMVSTC)'                     00656019
 PUNCH ' INCLUDE UT506(IEHMVMRY)'                                       00662019
 PUNCH ' ENTRY   IEHMVESY'                                              00668019
 PUNCH ' NAME    IEHMVESV(R)'                                           00674019
 PUNCH ' INCLUDE UT506(IEHMVSRV,IEHMVSRZ,IEHMVSRK)'                     00680019
 PUNCH ' INCLUDE UT506(IEHMVMRZ)'                                       00686019
 PUNCH ' ENTRY   IEHMVESZ'                                              00692019
 PUNCH ' NAME    IEHMVESX(R)'                                           00698019
 PUNCH ' INCLUDE UT506(IEHMVSSZ,IEHMVSSV)'                              00704019
 PUNCH ' INCLUDE UT506(IEHMVMRZ)'                                       00710019
 PUNCH ' ENTRY   IEHMVESZ'                                              00716019
 PUNCH ' NAME    IEHMVESZ(R)'                                           00722019
 PUNCH ' INCLUDE UT506(IEHMVSRD,IEHMVSRM)'                              00728019
 PUNCH ' ENTRY   IEHMVERD'                                              00734019
 PUNCH ' NAME    IEHMVERD(R)'                                           00740019
 PUNCH ' INCLUDE UT506(IEHMVSRA,IEHMVSRK)'                              00746019
 PUNCH ' ENTRY   IEHMVERA'                                              00752019
 PUNCH ' NAME    IEHMVERA(R)'                                           00758019
 PUNCH ' INCLUDE UT506(IEHMVSTA,IEHMVSRK,IEHMVSRM)'                     00764019
 PUNCH ' INCLUDE UT506(IEHMVMTA)'                                       00770019
 PUNCH ' ENTRY   IEHMVETA'                                              00776019
 PUNCH ' NAME    IEHMVETA(R)'                                           00782019
 PUNCH ' INCLUDE UT506(IEHMVSTL,IEHMVSRK,IEHMVSRM)'                     00788019
 PUNCH ' INCLUDE UT506(IEHMVMTL)'                                       00794019
 PUNCH ' ENTRY   IEHMVETL'                                              00800019
 PUNCH ' NAME    IEHMVETL(R)'                                           00806019
 PUNCH ' INCLUDE UT506(IEHMVSSS)'                                  I276 00812019
 PUNCH ' ENTRY   IEHMVESS'                                              00818019
 PUNCH ' NAME    IEHMVESS(R)'                                           00824019
 PUNCH ' INCLUDE UT506(IEHMVETG)'                                       00830019
 PUNCH ' ENTRY IEHMVETG'                                                00836019
 PUNCH ' NAME IEHMVETG(R)'                                              00842019
 PUNCH ' INCLUDE UT506(IEHMVESM)'                                       00848019
 PUNCH ' ENTRY IEHMVESM'                                                00854019
 PUNCH ' NAME IEHMVESM(R)'                                              00860019
 PUNCH ' INCLUDE UT506(IEHMVESL)'                                       00866019
 PUNCH ' ENTRY IEHMVESL'                                                00872019
 PUNCH ' NAME IEHMVESL(R)'                                              00878019
 PUNCH ' INCLUDE UT506(IEHMVETJ)'                                       00884019
 PUNCH ' ENTRY IEHMVETJ'                                                00890019
 PUNCH ' NAME IEHMVETJ(R)'                                              00896019
 PUNCH ' INCLUDE UT506(IEHMVESQ,IEHMVMSQ)'                              00902019
 PUNCH ' ENTRY IEHMVESQ'                                                00908019
 PUNCH ' NAME IEHMVESQ(R)'                                              00914019
 PUNCH ' INCLUDE UT506(IEHMVESN,IEHMVMSN)'                              00920019
 PUNCH ' ENTRY IEHMVESN'                                                00926019
 PUNCH ' NAME IEHMVESN(R)'                                              00932019
 PUNCH ' INCLUDE UT506(IEHMVESA)'                                       00938019
 PUNCH ' ENTRY IEHMVESA'                                                00944019
 PUNCH ' NAME IEHMVESA(R)'                                              00950019
 PUNCH '/*'                                                             01020000
&SGCTRLC(9)    SETC 'LINKLIB'                                           01040000
&SGCTRLC(10)   SETC ',LET'                                              01060000
 COPY SGLEDPK1                                                          01070017
 PUNCH '//UT506 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.UT506'         01080019
 COPY SGLEDPK2                                                          01090017
 PUNCH ' INCLUDE UT506(IEHMVXSE)'                                       01100017
 PUNCH ' ALIAS      IEHMVESE         '                                  01120000
 PUNCH ' NAME       IEHMVXSE(R)      '                                  01140000
 PUNCH ' INCLUDE UT506(IEHMVEST)'                                       01200017
 PUNCH ' NAME       IEHMVEST(R)      '                                  01220000
 PUNCH ' INCLUDE UT506(IEHMOVE)'                                 A44303 01240021
 PUNCH ' ENTRY IEHMVESD '                                        A32407 01250020
 PUNCH ' NAME       IEHMOVE(R)       '                                  01260000
 PUNCH '/*'                                                       000G  01260416
&SGCTRLC(7)     SETC   '&SGCTRLC(32)'                              000G 01260916
&SGCTRLC(8)     SETC     '&SGCTRLC(31)'                            000G 01261816
&SGCTRLC(9)     SETC     'LINKLIB'                                 000G 01262716
&SGCTRLC(10)    SETC     ',RENT'                                        01263617
&SGCTRLC(11)    SETC    ',LET'                                     000G 01264516
 COPY SGLEDPK1                                                          01265017
 PUNCH '//UT506 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.UT506'         01265519
 COPY SGLEDPK2                                                          01266017
 PUNCH ' INCLUDE UT506(IEBISAM)'                                        01266517
 PUNCH ' NAME IEBISAM(R)'                                          000I 01267318
 PUNCH ' INCLUDE UT506(IEBISU,IEBISSO)'                                 01268117
 PUNCH ' ENTRY IEBISU '                                            000G 01269016
 PUNCH ' NAME IEBISU(R)'                                           000I 01269918
 PUNCH ' INCLUDE UT506(IEBISL,IEBISSI)'                                 01270817
 PUNCH ' ENTRY IEBISL '                                            000G 01271716
 PUNCH ' NAME IEBISL(R)'                                           000I 01272618
 PUNCH ' INCLUDE UT506(IEBISF,IEBISMES)'                                01273517
 PUNCH ' ENTRY IEBISF '                                            000G 01274416
 PUNCH ' NAME IEBISF(R)'                                           000I 01275318
 PUNCH ' INCLUDE UT506(IEBISC)'                                         01276217
 PUNCH ' NAME IEBISC(R)'                                           000I 01277118
 PUNCH ' INCLUDE UT506(IEBISPL)'                                        01278017
 PUNCH ' NAME IEBISPL(R)'                                          000I 01279018
 PUNCH '/*'                                                             01280000
&SGCTRLC(7)    SETC  '&SGCTRLC(32)'                                000H 01280817
&SGCTRLC(8)    SETC  '&SGCTRLC(31)'                                000H 01281617
&SGCTRLC(9)    SETC  'LINKLIB'                                     000H 01282417
&SGCTRLC(10)   SETC  ',RENT'                                       000H 01283217
&SGCTRLC(11)   SETC  ',REFR'                                       000H 01284017
 COPY SGLEDPK1                                                     000H 01284817
 PUNCH '//UT506 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.UT506'    000H 01285619
 COPY SGLEDPK2                                                     000H 01286417
 PUNCH ' INCLUDE UT506(IEBDG)'                                     000H 01287217
 PUNCH ' NAME IEBDG(R)'                                            000H 01288017
 PUNCH ' INCLUDE UT506(IEBDGCUP)'                                  000H 01288817
 PUNCH ' NAME IEBDGCUP(R)'                                         000H 01289617
 PUNCH ' INCLUDE UT506(IEBFDANL)'                                  000H 01290417
 PUNCH ' NAME IEBFDANL(R)'                                         000H 01291217
 PUNCH ' INCLUDE UT506(IEBFDTBL)'                                  000H 01292017
 PUNCH ' NAME IEBFDTBL(R)'                                         000H 01292817
 PUNCH ' INCLUDE UT506(IEBCRANL)'                                  000H 01293617
 PUNCH ' NAME IEBCRANL(R)'                                         000H 01294417
 PUNCH ' INCLUDE UT506(IEBCREAT)'                                  000H 01295217
 PUNCH ' NAME IEBCREAT(R)'                                         000H 01296017
 PUNCH ' INCLUDE UT506(IEBDGMSG)'                                  000H 01296817
 PUNCH ' NAME IEBDGMSG(R)'                                         000H 01297617
 PUNCH '/*'                                                        000H 01298417
         MEND                                                           01300000
