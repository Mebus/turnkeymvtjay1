         MACRO                                                          00070000
         SGIEW401                                                       00070100
         COPY  SGGBLPAK                                                 00071000
.* UPDATED 29 DEC 65                                                    00080000
         AIF   (&SGLINKB(1) OR &SGOVERB(1)).GENOVLY                     00100000
         MEXIT                                                          00120000
.GENOVLY    ANOP                                                        00140000
&SGCTRLC(7) SETC   '&SGCTRLC(32)'  VOLUME NO. FOR LINKLIB.              00160000
&SGCTRLC(8) SETC   '&SGCTRLC(31)'                                       00180000
&SGCTRLC(9) SETC   'LINKLIB'                                            00200000
&SGCTRLC(6) SETC   'IEWSZOVR'                                           00220000
&SGCTRLC(10)  SETC  ',RENT'                                             00240000
            AIF    (&SGOVERB(2) OR &SGOVERB(3)).OVR                     00260000
            AIF     (&SGCPRGB(6)).ASYNCH                                00280000
         AIF   (&SGCPRGB(8)).ASYNCH                                     00300000
&SGQUITB    SETB   1                                                    00320000
    MNOTE 5,'* * * IEIIEW400 ASYNCHRONOUS OVERLAY REQUIRES THE VMS CONT*00340000
               ROL PROGRAM.'                                            00360000
            AGO    .RETURN                                              00380000
.ASYNCH     ANOP                                                        00400000
            COPY  SGLEDPK1                                              00420017
            PUNCH  '//CI535 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.CIS00430019
               535  '                                                   00431019
            COPY   SGLEDPK2                                             00440017
            PUNCH  ' INCLUDE    CI535(IEWSWOVR) '                       00450017
            AGO    .PUNCH                                               00460000
.OVR        ANOP                                                        00480000
            COPY   SGLEDPK1                                             00500017
            PUNCH  '//CI505 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.CIS00510019
               505  '                                                   00511019
            COPY   SGLEDPK2                                             00512017
            AIF    (&SGOVERB(3)).BASWCK                                 00520000
            PUNCH  ' INCLUDE    CI505(IEWSYOVR) '                       00540017
            AGO    .PUNCH                                               00560000
.ERR         ANOP                                                       00580000
&SGQUITB SETB 1                                                         00600000
    MNOTE 5,'* * * IEIIEW400 ADVANCED OVERLAY IS NOT SELECTABLE ON E LE*00620000
               VEL (32K) MACHINES.'                                     00640000
             AGO    .RETURN                                             00660000
.BASWCK     ANOP                                                        00680000
             AIF    (&SGCPUB(20)).ERR                                   00700000
            PUNCH  ' INCLUDE    CI505(IEWSXOVR) '                       00720017
.PUNCH      ANOP                                                        00740000
            PUNCH     ' NAME   IEWSZOVR(R)  '                           00760000
            PUNCH  '/*  '                                               00780000
.RETURN  MEND                                                           00800000
