       MACRO                                                            00020000
       IOCHECK &LOOKFOR=                                                00040000
       COPY    SGGBLPAK                                                 00060000
 LCLA &LB,&HB,&I,&ADDR,&TB,&C,&FB,&TABDD,&COMEDD                        00080014
         ACTR  50000                                                    00090018
         AIF   (&SGCNTRA(3) EQ 0).BAD   NO IO DEVICES GIVEN             00100000
&C     SETA    &LOOKFOR                                                 00120000
&LB    SETA    1             LOWER BOUNDARY INDEX                       00140000
&HB    SETA    &SGCNTRA(3)   HIGHER BOUNDARY INDEX                      00160000
 AIF (&SGCTRLB(1)).LOOPSER  DO SERIAL SEARCH FOR OPTCHAN ENTRY          00170014
.*   IF  C  OUTSIDE  BOUNDARIES  OF  TABLE  TRANSFERT  TO  .BAD         00180000
       AIF     (&C GT (&SGDADRA(&HB)-&SGDADRA(&HB)/4096*4096)).BAD      00200000
       AIF     (&C LT (&SGDADRA(1)-&SGDADRA(1)/4096*4096)).BAD          00220000
.LOOP  AIF     (&LB GE &HB).BAD  NO ENTRY FOR THIS ADDR IN TABLE        00240000
&TB    SETA    (&SGDADRA(&HB)-&SGDADRA(&HB)/4096*4096)   TOP BOUNDARY   00260000
&FB    SETA    (&SGDADRA(&LB)-&SGDADRA(&LB)/4096*4096)   LOWER BOUNDARY 00280000
&I     SETA    (&LB+&HB)/2             INDEX  TO  MIDDLE  TABLE         00300000
&ADDR  SETA    (&SGDADRA(&I)-&SGDADRA(&I)/4096*4096)     MIDDLE ADDRESS 00320000
       AIF     (&C EQ &TB).HBX                                          00340000
       AIF     (&C EQ &ADDR).IHX                                        00360000
 AGO .NOPTCH              SKIP OPTCHAN ROUTINE                          00361014
.LOOPSER AIF (&LB GT &HB).BAD          ADDRESS NOT FOUND                00362018
&ADDR SETA (&SGDADRA(&LB)-&SGDADRA(&LB)/4096*4096)                      00363014
 AIF (&C EQ &ADDR).IHX                                                  00364014
 AIF (&LB EQ &SGCTRLA(2)).INDEX                                         00365014
&TABDD SETA &ADDR-&ADDR/X'100'*X'100'                                   00366014
&COMEDD SETA &C-&C/X'100'*X'100'                                        00367014
&I SETA &SGDADRA(&LB)/1048576   GET DEVICE POINTER                      00368018
 AIF (NOT &SGDAP1B(&I)).OPT2            OPTCHAN 1                       00369014
 AIF (&C/X'100' EQ 1).CUDEV             OPTCHAN AND DEV. CHAN 1         00370014
.OPT2 AIF (NOT &SGDAP2B(&I)).OPT3       OPTCHAN 2                       00371014
 AIF (NOT &SGOPSUB(&I)).NOTSUB1        NOT SELECTOR SUB CHANNEL         00371118
&HEX SETA X'D'                                                          00371218
 AIF (&COMEDD/X'10' EQ &HEX).CHAN      DUPLICATE C.U. POSITION          00371318
 AGO .OPT3                                                              00371418
.NOTSUB1 ANOP                                                           00371518
 AIF (&C/X'100' EQ 2).CUDEV             OPTCHAN AND DEV. CHAN 2         00372014
.OPT3 AIF (NOT &SGDAP3B(&I)).OPT4       OPTCHAN 3                       00373014
 AIF (NOT &SGOPSUB(&I)).NOTSUB2        NOT SELECTOR SUB CHANNEL         00373118
&HEX SETA X'E'                                                          00373218
 AIF (&COMEDD/X'10' EQ &HEX).CHAN      DUPLICATE C.U. POSITION          00373318
 AGO .OPT4                                                              00373418
.NOTSUB2 ANOP                                                           00373518
 AIF (&C/X'100' EQ 3).CUDEV             OPTCHAN AND DEV. CHAN 3         00374014
.OPT4 AIF (NOT &SGDAP4B(&I)).OPT5       OPTCHAN 4                       00375014
 AIF (NOT &SGOPSUB(&I)).NOTSUB3        NOT SELECTOR SUB CHANNEL         00375118
&HEX SETA X'F'                                                          00375218
 AIF (&COMEDD/X'10' EQ &HEX).CHAN      DUPLICATE C.U. POSITION          00375318
 AGO .INDEX                                                             00375418
.NOTSUB3 ANOP                                                           00375518
 AIF (&C/X'100' EQ 4).CUDEV             OPTCHAN AND DEV. CHAN 4         00376014
.OPT5 AIF (NOT &SGDAP5B(&I)).OPT6       OPTCHAN 5                       00377014
 AIF (&C/X'100' EQ 5).CUDEV             OPTCHAN AND DEV. CHAN 5         00378014
.OPT6 AIF (NOT &SGDAP6B(&I)).OPT7       NOT OPTCHAN 6                   00378120
 AIF (&C/X'100' EQ 6).CUDEV             OPTCHAN AND DEV. CHAN 6         00378220
.OPT7 AIF (NOT &SGDAP7B(&I)).OPT8       NOT OPTCHAN 7                   00378320
 AIF (&C/X'100' EQ 7).CUDEV             OPTCHAN AND DEV. CHAN 7         00378420
.OPT8 AIF (NOT &SGDAP8B(&I)).OPT9       NOT OPTCHAN 8                   00378520
 AIF (&C/X'100' EQ 8).CUDEV             OPTCHAN AND DEV. CHAN 8         00378620
.OPT9 AIF (NOT &SGDAP9B(&I)).OPTA       NOT OPTCHAN 9                   00378720
 AIF (&C/X'100' EQ 9).CUDEV             OPTCHAN AND DEV. CHAN 9         00378820
.OPTA AIF (NOT &SGDAPAB(&I)).OPTB       NOT OPTCHAN 10                  00378920
 AIF (&C/X'100' EQ 10).CUDEV            OPTCHAN AND DEV. CHAN 10        00379020
.OPTB AIF (NOT &SGDAPBB(&I)).OPTC       NOT OPTCHAN 11                  00379120
 AIF (&C/X'100' EQ 11).CUDEV            OPTCHAN AND DEV. CHAN 11        00379220
.OPTC AIF (NOT &SGDAPCB(&I)).OPTD       NOT OPTCHAN 12                  00379320
 AIF (&C/X'100' EQ 12).CUDEV            OPTCHAN AND DEV. CHAN 12        00379420
.OPTD AIF (NOT &SGDAPDB(&I)).OPTE       NOT OPTCHAN 13                  00379520
 AIF (&C/X'100' EQ 13).CUDEV            OPTCHAN AND DEV. CHAN 13        00379620
.OPTE AIF (NOT &SGDAPEB(&I)).OPTF       NOT OPTCHAN 14                  00379720
 AIF (&C/X'100' EQ 14).CUDEV            OPTCHAN AND DEV. CHAN 14        00379820
.OPTF AIF (NOT &SGDAPFB(&I)).INDEX      NOT OPTCHAN 15                  00379920
 AIF (&C/X'100' EQ 15).CUDEV            OPTCHAN AND DEV. CHAN 15        00380020
 AGO .INDEX                                                             00381014
.CHAN AIF (&ADDR/X'100' NE &C/X'100').INDEX  CHANNELS NOT THE SAME      00381218
&TABDD SETA &TABDD-&TABDD/X'10'*X'10'  DEVICE PORTION                   00381418
&COMEDD SETA &COMEDD-&COMEDD/X'10'*X'10'    COMPARISON                  00381618
.CUDEV AIF (&TABDD EQ &COMEDD).IHX                                      00382014
.INDEX  ANOP       MOVE TO NEXT ENTRY                                   00383014
&LB SETA &LB+1                                                          00384014
 AGO .LOOPSER                                                           00385014
.NOPTCH AIF (&C EQ &FB).LBX                                             00386014
       AIF     (&C  LT &ADDR).CHGBD         C  NOT  FOUND               00400000
.*              REDUCE  SIZE  OF  TABLE                                 00420000
&LB    SETA    &I+1                    LEAVE  LOWER  HALF  OF  TABLE    00440000
       AGO     .LOOP                                                    00460000
.CHGBD ANOP                                                             00480000
&HB    SETA    &I-1                    LEAVE  HIGHER  HALF  OF  TABLE   00500000
       AGO     .LOOP                                                    00520000
.BAD   ANOP              EXIT  ADDR  NOT  FOUND  IN  TABLE              00540000
&HEX   SETA    0                                                        00560000
       AGO     .MEND                                                    00580000
.HBX   ANOP                                                             00600000
&HEX   SETA    &HB      SET HEX TO VALUE OF HIGHER INDEX                00620000
       AGO     .MEND                                                    00640000
.IHX   ANOP                                                             00660000
 AIF (NOT &SGCTRLB(1)).SETIHX           ENTERED FOR OPTCHAN             00664014
&HEX SETA &ADDR     SET TO ADDRESS SHOWING CONFLICT                     00668014
 AGO .MEND                                                              00672014
.SETIHX ANOP                                                            00676014
&HEX   SETA    &I     SET HEX TO VALUE OF MIDDLE INDEX                  00680000
       AGO     .MEND                                                    00700000
.LBX   ANOP                                                             00720000
&HEX   SETA    &LB       SET HEX TO VALUE OF LOWER INDEX                00740000
.MEND  MEND                                                             00760000
