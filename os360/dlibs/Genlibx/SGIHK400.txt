         MACRO                                                          01000018
         SGIHK400                                                       02000018
.*0814160000-220000,340000,420000,480000,540000,600000             000B 03000018
         COPY  SGGBLPAK                                                 04000018
&SGCTRLC(7) SETC '&SGCTRLC(32)'    VOLUME NUMBER FOR LINKLIB            05000018
&SGCTRLC(8) SETC '&SGCTRLC(31)'    UNIT NAME FOR LINKLIB                06000018
&SGCTRLC(9) SETC 'LINKLIB'         DATA SET NAME                        07000018
&SGCTRLC(11) SETC ',RENT'                                               08000018
         COPY  SGLEDPK1                                            000B 09000018
  PUNCH '//RC536 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.RC536'   000B 10000000
  PUNCH '//CI505 DD DISP=SHR,VOLUME=(,RETAIN),DSNAME=SYS1.CI505'   000B 11000000
         COPY  SGLEDPK2                                            000B 12000018
         PUNCH ' INCLUDE  CI505(IEFLOCDQ)'                         000B 13000018
         PUNCH ' INCLUDE  CI505(IEFCNVRT)'                         000B 14000018
         PUNCH ' INCLUDE  CI505(IEFRDWRT)'                         000B 15000018
         PUNCH ' ALIAS LOC '                                            16000018
         PUNCH ' ALIAS LOCCAN '                                         17000018
         PUNCH ' ALIAS LOCDQ  '                                         18000018
         PUNCH ' NAME IEFLOCDQ(R)'                                      19000018
         PUNCH ' CHANGE IEFSD111(IEFQMDQ2),IEFSD110(IEFQMDQ2)'          20000018
         PUNCH ' INCLUDE  CI505(IEFQMDQQ)'                         000B 21000018
         PUNCH ' ENTRY IEFQMDQ2'                                        22000018
         PUNCH ' ALIAS IEFQMDQ2'                                        23000018
         PUNCH ' NAME IEFQMDQQ(R)'                                      24000018
         PUNCH ' INCLUDE  RC536(IHKCDBSH)'                         000B 25000018
         PUNCH ' ALIAS IHKCDMSH'                                        26000018
         PUNCH ' NAME IHKCDBSH(R)'                                      27000018
         PUNCH ' INCLUDE  RC536(IHKCDBTX)'                         000B 28000018
         PUNCH ' ALIAS IHKCDBTW'                                        29000018
         PUNCH ' NAME IHKCDBTX(R)'                                      30000018
         PUNCH ' INCLUDE  RC536(IHKCFSTA) '                        000B 31000018
         PUNCH ' ALIAS IHKCFSTB '                                  000A 32000018
         PUNCH ' NAME IHKCFSTA(R) '                                000A 33000018
         PUNCH ' INCLUDE  RC536(IHKCFOUT) '                        000B 34000018
         PUNCH ' ALIAS IHKCFQOP '                                  000A 35000018
         PUNCH ' NAME IHKCFOUT(R) '                                000A 36000018
         PUNCH ' INCLUDE RC536(IHKCHATS)'                          000B 37000018
         PUNCH ' ALIAS IHKCASHA'                                   000B 38000018
         PUNCH ' NAME IHKCASHT(R)'                                 000B 39000018
         PUNCH ' INCLUDE RC536(IHKCHSUP)'                          000B 40000018
         PUNCH ' NAME IHKCASHU(R)'                                 000B 41000018
         PUNCH ' INCLUDE RC536(IHKCHJIR)'                          000B 42000018
         PUNCH ' ALIAS IHKCASHD'                                   000B 43000018
         PUNCH ' NAME IHKCASHJ(R)'                                 000B 44000018
         PUNCH ' INCLUDE RC536(IHKCHIRP)'                          000C 45000018
         PUNCH ' ALIAS IHKCARJN'                                   000C 46000018
         PUNCH ' NAME IHKCHIRP(R)'                                 000C 47000018
         PUNCH ' INCLUDE RC536(IHKCHOSE)'                          000C 48000018
         PUNCH ' ALIAS IHKCAOSR'                                   000C 49000018
         PUNCH ' NAME IHKCHOSE(R)'                                 000C 50000018
         AIF   (&SGSCHDB(2)).OUT                                   000B 51000018
         PUNCH ' INCLUDE RC536(IHKBBNIT)'                          000B 52000018
         PUNCH ' NAME IHKBBNIT(R)'                                 000B 53000018
         PUNCH ' INCLUDE RC536(IHKQMNGR)'                          000B 54000018
         PUNCH ' NAME IHKQMNGR(R)'                                 000B 55000018
.OUT     ANOP                                                      000B 56000018
         PUNCH '/*'                                                     57000018
         MEND                                                           58000018
