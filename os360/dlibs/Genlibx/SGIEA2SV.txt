         MACRO                                                          00020000
         SGIEA2SV                                                       00040000
         COPY  SGGBLPAK                                                 00060000
         LCLA  &STORLO,&STORHI,&STORGAP,&CURRLO,&CURRHI,&CURRGAP        00080000
         LCLA  &NUMBER,&RESSUM,&TRSUM,&I,&TYPE,&REGSAVE                 00100000
         LCLA  &RESADD,&N,&M,&NUM(13),&TRADD,&ESA                       00120000
         LCLC  &BLK,&SVCNO,&TYPEC                                       00140000
         LCLC  &CC,&C(14),&CCC(20),&NAM(4),&EXT(4),&TR(10),&LENGTH      00160000
&SGCTRLC(6) SETC 'IEASVC00'                                             00180000
         COPY  SGASMPAK                                                 00200000
.*                                                                      00220000
.* COMPUTE NUMBER OF RESIDENT SVCS AND NUMBER OF TRANSIENT SVCS         00240000
.*                                                                      00260000
&I       SETA  1                                                        00280000
&RESSUM  SETA  2         ONE ENTRY  FOR NOP                             00300000
&BLK  SETC  '        '                                                  00320000
   MNOTE *,' * * * * * * * * SVC TABLE * * * * * * * *'                 00340000
   MNOTE *,'      NUMBER       TYPE    EXT. SAVE AREA '                 00360000
   MNOTE *,'                                          '                 00380000
   MNOTE *,'        0            1             0'                       00400000
.NEXT1   ANOP                                                           00420000
&TYPE    SETA  (4*&SGSVCAB(&I)+2*&SGSVCBB(&I)+1*&SGSVCCB(&I))           00440000
&REGSAVE SETA  (4*&SGSVCDB(&I)+2*&SGSVCEB(&I)+1*&SGSVCFB(&I))           00460000
     AIF   (&TYPE EQ 0).CONT1                                           00480000
&SVCNO  SETC  '&I'.'&BLK'(1,2)                                          00500000
&TYPEC  SETC  'NOP'                                                     00520000
&ESA   SETA 0                                                           00540000
    AIF  (&TYPE EQ 5).X0                                                00560000
&TYPEC  SETC  '&TYPE'                                                   00580000
    AIF  (&TYPE EQ 1).X0                                                00600000
&ESA    SETA   &REGSAVE-1                                               00620000
.X0      ANOP                                                           00640000
&TYPEC SETC '&TYPEC'.'&BLK'(1,2)                                        00660000
&TYPEC SETC '&TYPEC'(1,3)                                               00680000
&SVCNO   SETC  '&SVCNO'(1,3)                                            00700000
    MNOTE *,'        &SVCNO          &TYPEC           &ESA'             00720000
    AIF (&TYPE EQ 3 OR &TYPE EQ 4 OR &TYPE EQ 5).CONT1                  00740000
&RESSUM  SETA  &RESSUM+1                                                00760000
.CONT1   ANOP                                                           00780000
&I       SETA  &I+1                                                     00800000
         AIF   (&I LE 255).NEXT1                                        00820000
&TRADD   SETA  &RESSUM+1                                                00840000
&IEALMTA SETA  &RESSUM+1                                                00860000
.*                                                                      00880000
.* COMPUTE LARGEST GAP IN SVC NUMBERS                                   00900000
.*                                                                      00920000
&I       SETA  0                                                        00940000
.NEXT3   ANOP                                                           00960000
&I       SETA  &I+1                                                     00980000
&TYPE    SETA  (4*&SGSVCAB(&I)+2*&SGSVCBB(&I)+1*&SGSVCCB(&I))           01000000
         AIF   (&TYPE NE 0).NOGAP                                       01020000
&STORGAP SETA  &STORGAP+1                                               01040000
&STORHI  SETA  0                                                        01060000
         AIF   (&I LT 255).NEXT3                                        01080000
         AIF   (&STORGAP LT &CURRGAP).CONT3                             01100000
&CURRLO  SETA  &STORLO                                                  01120000
&CURRHI  SETA  256                                                      01140000
         AGO   .CONT3                                                   01160000
.NOGAP   ANOP                                                           01180000
&STORHI  SETA  &I                                                       01200000
         AIF   (&STORGAP LT &CURRGAP).CONT2                             01220000
&CURRLO  SETA  &STORLO                                                  01240000
&CURRHI  SETA  &STORHI                                                  01260000
&CURRGAP SETA  &STORGAP                                                 01280000
.CONT2   ANOP                                                           01300000
&STORGAP SETA  0                                                        01320000
&STORLO  SETA  &I                                                       01340000
         AIF   (&I LT 255).NEXT3                                        01360000
.CONT3   ANOP                                                           01380000
&IEAHIA  SETA  &CURRLO+1                                                01400000
&IEALOA  SETA  &CURRHI-1                                                01420000
.*                                                                      01440000
.* SET UP PREFIX TABLE FOR IBM SVC'S                                    01460000
.*                                                                      01480000
         PUNCH 'IHASVC00 CSECT'                                         01500000
         PUNCH '         ENTRY SVPRFX,SVCTBL,SVLMT'                     01520000
         PUNCH 'NOPEXIT  SR    15,15   COMPLETION CODE = ZERO'          01540000
         PUNCH '         BR    14      RETURN'                          01560000
         PUNCH 'SVPRFX   DS    0C'                                      01580000
&RESADD  SETA  3                                                        01600000
&I       SETA  1                                                        01620000
&NUM(1)  SETA  1                                                        01640000
&N       SETA  2                                                        01660000
.NEXT3A  ANOP                                                           01680000
&TYPE    SETA  (4*&SGSVCAB(&I)+2*&SGSVCBB(&I)+1*&SGSVCCB(&I))           01700000
         AIF   (&TYPE EQ 1 OR &TYPE EQ 2).RESPFX1                       01720000
         AIF   (&TYPE EQ 3 OR &TYPE EQ 4).TRPFX1                        01740000
         AIF   (&TYPE EQ 5).NOP1                                        01760000
&NUM(&N) SETA  0                                                        01780000
         AGO   .CONT3A                                                  01800000
.RESPFX1 ANOP                                                           01820000
&NUM(&N) SETA  &RESADD                                                  01840000
&RESADD  SETA  &RESADD+1                                                01860000
         AGO   .CONT3A                                                  01880000
.TRPFX1  ANOP                                                           01900000
&NUM(&N) SETA  &TRADD                                                   01920000
&TRADD   SETA  &TRADD+1                                                 01940000
         AGO   .CONT3A                                                  01960000
.NOP1    ANOP                                                           01980000
&NUM(&N) SETA  2                                                        02000000
.CONT3A  ANOP                                                           02020000
&N       SETA  &N+1                                                     02040000
         AIF   (&N LT 14).CONT3B                                        02060000
&N       SETA  1                                                        02080000
         PUNCH '         DC    AL1(&NUM(1),&NUM(2),&NUM(3),&NUM(4),&NUM*02100000
               (5),&NUM(6),&NUM(7),&NUM(8),&NUM(9),&NUM(10),&NUM(11),&N*02120000
               UM(12),&NUM(13))'                                        02140000
.CONT3B  AIF   ((&I+1) GE &IEAHIA).CONT3B1                              02160000
&I     SETA    &I+1                                                     02180000
   AGO   .NEXT3A                                                        02200000
.*                                                                      02220000
.* SET UP PREFIX TABLE FOR USER SVC'S                                   02240000
.*                                                                      02260000
.CONT3B1 AIF   ((&I+1) NE &IEAHIA).CONT7B                               02280000
         AIF   (&IEALOA EQ 255).NEXT8                                   02300000
&I       SETA  255                                                      02320000
         AGO   .NEXT3A                                                  02340000
.CONT7B  ANOP                                                           02360000
&I       SETA  &I-1                                                     02380000
         AIF   ((&I-1) GE &IEALOA).NEXT3A                               02400000
.NEXT8   ANOP                                                           02420000
         AIF   (&N EQ 1).NEXT8A3                                        02440000
&NUMBER  SETA  &N-1                                                     02460000
&N       SETA  1                                                        02480000
.NEXT8A0 ANOP                                                           02500000
         AIF   (&NUMBER EQ &N).NOCOMMA                                  02520000
&CC      SETC  ','                                                      02540000
         AGO   .NEXT8A1                                                 02560000
.NOCOMMA ANOP                                                           02580000
&CC      SETC  ''                                                       02600000
.NEXT8A1 ANOP                                                           02620000
&C(&N)   SETC  '&NUM(&N)'.'&CC'                                         02640000
&N       SETA  &N+1                                                     02660000
         AIF   (&N LE &NUMBER).NEXT8A0                                  02680000
.NEXT8A2 ANOP                                                           02700000
&C(&N)   SETC  ''                                                       02720000
&N       SETA  &N+1                                                     02740000
         AIF   (&N LT 15).NEXT8A2                                       02760000
         PUNCH '         DC    AL1(&C(1)&C(2)&C(3)&C(4)&C(5)&C(6)&C(7)&*02780000
               C(8)&C(9)&C(10)&C(11)&C(12)&C(13)&C(14))'                02800000
.NEXT8A3 ANOP                                                           02820000
&N       SETA  2                                                        02840000
&M       SETA  1                                                        02860000
.*                                                                      02880000
.* GENERATE RESIDENT PORTION OF SVC TABLE                               02900000
.*                                                                      02920000
         AIF   (&SGSUPRB(3) EQ 1).CONT27                                02940000
         PUNCH 'SVCTBLX  DS    0C'                                      02960000
         PUNCH 'SVCTBL   EQU   SVCTBLX-3'                               02980000
         AGO   .CONT28                                                  03000000
.CONT27  ANOP                                                           03020000
         PUNCH 'SVCTBLX  DS    0F'                                      03040000
         PUNCH 'SVCTBL   EQU   SVCTBLX-4'                               03060000
.CONT28  ANOP                                                           03080000
&NAM(1)  SETC  'IGC000+0'                                               03100000
&NAM(2)  SETC  'NOPEXIT'                                                03120000
&CCC(2)  SETC  ','                                                      03140000
&EXT(1)  SETC  'IGC000'                                                 03160000
&C(1)    SETC  ','                                                      03180000
&CCC(1) SETC ','                                                        03200000
&I       SETA  1                                                        03220000
.NEXT9   ANOP                                                           03240000
&TYPE    SETA  (4*&SGSVCAB(&I)+2*&SGSVCBB(&I)+1*&SGSVCCB(&I))           03260000
&REGSAVE SETA  (4*&SGSVCDB(&I)+2*&SGSVCEB(&I)+1*&SGSVCFB(&I))           03280000
         AIF   (&TYPE EQ 3 OR &TYPE EQ 4).NOADCON                       03300000
         AIF   (&TYPE EQ 0 OR &TYPE EQ 5).NOADCON                       03320000
&N       SETA  &N+1                                                     03340000
&CCC(&N) SETC  ','                                                      03360000
&M       SETA  &M+1                                                     03380000
         AIF   (&I LT 10).CONT9A                                        03400000
         AIF   (&I LT 100).CONT9B                                       03420000
&NAM(&N) SETC  'IGC'.'&I'.'+'.'&REGSAVE'                                03440000
&EXT(&M) SETC  'IGC'.'&I'                                               03460000
&C(&M)   SETC  ','                                                      03480000
         AGO   .CONT9C                                                  03500000
.CONT9A  ANOP                                                           03520000
&NAM(&N) SETC  'IGC00'.'&I'.'+'.'&REGSAVE'                              03540000
&EXT(&M) SETC  'IGC00'.'&I'                                             03560000
&C(&M)   SETC  ','                                                      03580000
         AGO   .CONT9C                                                  03600000
.CONT9B  ANOP                                                           03620000
&NAM(&N) SETC  'IGC0'.'&I'.'+'.'&REGSAVE'                               03640000
&EXT(&M) SETC  'IGC0'.'&I'                                              03660000
&C(&M)   SETC  ','                                                      03680000
         AGO   .CONT9C                                                  03700000
.CONT9C  ANOP                                                           03720000
         AIF   (&N LT 4).NOADCON                                        03740000
&N       SETA  0                                                        03760000
         AIF   (&SGSUPRB(3) EQ 1).CONT29                                03780000
         PUNCH '         DC    AL3(&NAM(1),&NAM(2),&NAM(3),&NAM(4))'    03800000
         AGO  .CONT30                                                   03820000
.CONT29  ANOP                                                           03840000
         PUNCH '         DC    A(&NAM(1),&NAM(2),&NAM(3),&NAM(4))'      03860000
.CONT30  ANOP                                                           03880000
         AIF   (&M EQ 0).NOADCON                                        03900000
.CONT9D  AIF   (&M LT 4).BLANK                                          03920000
         PUNCH '         EXTRN &EXT(1)&C(1)&EXT(2)&C(2)&EXT(3)&C(3)&EXT*03940000
               (4)'                                                     03960000
&M       SETA  0                                                        03980000
         AGO   .NOADCON                                                 04000000
.BLANK   ANOP                                                           04020000
&C(&M)   SETC  ''                                                       04040000
&M       SETA  &M+1                                                     04060000
&EXT(&M) SETC  ''                                                       04080000
&C(&M)   SETC  ''                                                       04100000
         AGO   .CONT9D                                                  04120000
.NOADCON ANOP                                                           04140000
         AIF   ((&I+1) EQ &IEAHIA).CONT10                               04160000
         AIF   ((&I+1) GT &IEAHIA).CONT12                               04180000
&I       SETA  &I+1                                                     04200000
         AGO   .NEXT9                                                   04220000
.CONT10  ANOP                                                           04240000
         AIF   (&IEALOA EQ 255).CONT14                                  04260000
&I       SETA  255                                                      04280000
         AGO   .NEXT9                                                   04300000
.CONT12  ANOP                                                           04320000
&I       SETA  &I-1                                                     04340000
         AIF   ((&I-1) GE &IEALOA).NEXT9                                04360000
.CONT14  ANOP                                                           04380000
         AIF   (&N EQ 0).CONT15                                         04400000
&CCC(&N) SETC  ''                                                       04420000
.CONT14B ANOP                                                           04440000
&N       SETA  &N+1                                                     04460000
&CCC(&N) SETC  ''                                                       04480000
&NAM(&N) SETC  ''                                                       04500000
         AIF   (&N LT 4).CONT14B                                        04520000
         AIF   (&SGSUPRB(3) EQ 1).CONT31                                04540000
         PUNCH '         DC    AL3(&NAM(1)&CCC(1)&NAM(2)&CCC(2)&NAM(3)&*04560000
               CCC(3)&NAM(4))'                                          04580000
         AGO   .CONT32                                                  04600000
.CONT31  ANOP                                                           04620000
         PUNCH '         DC      A(&NAM(1)&CCC(1)&NAM(2)&CCC(2)&NAM(3)&*04640000
               CCC(3)&NAM(4))'                                          04660000
.CONT32  ANOP                                                           04680000
         AIF   (&M EQ 0).CONT15                                         04700000
&C(&M)   SETC  ''                                                       04720000
.CONT14C ANOP                                                           04740000
&M       SETA  &M+1                                                     04760000
&EXT(&M) SETC  ''                                                       04780000
&C(&M)   SETC  ''                                                       04800000
         AIF   (&M LT 4).CONT14C                                        04820000
         PUNCH '         EXTRN &EXT(1)&C(1)&EXT(2)&C(2)&EXT(3)&C(3)&EXT*04840000
               (4)'                                                     04860000
.CONT15  ANOP                                                           04880000
.*                                                                      04900000
.* GENERATE TABLE FOR TRANSIENT SVC'S                                   04920000
.*                                                                      04940000
&LENGTH  SETC  'L1'                                                     04960000
         AIF   (&SGSUPRB(3) NE 1).CONT15A                               04980000
&LENGTH  SETC  ''                                                       05000000
         PUNCH 'SVLMT    DS    0F'                                      05020000
         AGO   .CONT15B                                                 05040000
.CONT15A ANOP                                                           05060000
         PUNCH 'SVLMT    DS    0C'                                      05080000
.CONT15B ANOP                                                           05100000
&I       SETA  1                                                        05120000
&N       SETA  0                                                        05140000
.NEXT10  ANOP                                                           05160000
&TYPE    SETA  (4*&SGSVCAB(&I)+2*&SGSVCBB(&I)+1*&SGSVCCB(&I))           05180000
&REGSAVE SETA  (4*&SGSVCDB(&I)+2*&SGSVCEB(&I)+1*&SGSVCFB(&I))           05200000
         AIF   (&TYPE EQ 1 OR &TYPE EQ 2 OR &TYPE EQ 5).CONT16          05220000
         AIF   (&TYPE EQ 0).CONT16                                      05240000
&N       SETA  &N+1                                                     05260000
&CCC(&N) SETC  ','                                                      05280000
&TR(&N)  SETC  '&REGSAVE'                                               05300000
         AIF   (&N LT 10).CONT16                                        05320000
         PUNCH '         DC    A&LENGTH.(&TR(1),&TR(2),&TR(3),&TR(4),&T*05340000
               R(5),&TR(6),&TR(7),&TR(8),&TR(9),&TR(10))'               05360000
&N       SETA  0                                                        05380000
.CONT16  ANOP                                                           05400000
         AIF   ((&I+1) EQ &IEAHIA).CONT18                               05420000
         AIF   ((&I+1) GT &IEAHIA).CONT20                               05440000
&I       SETA  &I+1                                                     05460000
         AGO   .NEXT10                                                  05480000
.CONT18  ANOP                                                           05500000
         AIF   (&IEALOA EQ 255).CONT22                                  05520000
&I       SETA  255                                                      05540000
         AGO   .NEXT10                                                  05560000
.CONT20  ANOP                                                           05580000
&I       SETA  &I-1                                                     05600000
         AIF   ((&I-1) GE &IEALOA).NEXT10                               05620000
.CONT22  AIF   (&N EQ 0).CONT26                                         05640000
&CCC(&N) SETC  ''                                                       05660000
.CONT25  ANOP                                                           05680000
&N       SETA  &N+1                                                     05700000
         AIF   (&N GT 10).CONT25A                                       05720000
&CCC(&N) SETC  ''                                                       05740000
&TR(&N)  SETC  ''                                                       05760000
         AGO   .CONT25                                                  05780000
.CONT25A ANOP                                                           05800000
         PUNCH '         DC    A&LENGTH.(&TR(1)&CCC(1)&TR(2)&CCC(2)&TR(*05820000
               3)&CCC(3)&TR(4)&CCC(4)&TR(5)&CCC(5)&TR(6)&CCC(6)&TR(7)&C*05840000
               CC(7)&TR(8)&CCC(8)&TR(9)&CCC(9)&TR(10))'                 05860000
.CONT26  ANOP                                                           05880000
         PUNCH '         END'                                           05900000
         PUNCH '/*'                                                     05920000
         MEND                                                           05940000
