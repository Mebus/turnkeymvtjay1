*        %GOTO TAIEBSL  ; /*                                            02000020
         MACRO                                                          04000020
         IKJTAIE                                                        06000020
TAIE     DSECT                                                          08000020
TAIEMSGL DS    CL2 .    MESSAGE LENGTH                                  24000020
TAIETGET DS    CL1 .    RET CODE FROM TGET ISSUED BY ATTN PROL LOG      26000020
*                       TO BE CHECKED BY USER ATTN RTN                  28000020
         DS    CL1 .    RESERVED                                        30000020
TAIEIAD  DS    1F .     RIGHT HALF OF INTERRUPT PSW                     32000020
TAIERSAV DS    CL64 .   REGS. STORED HERE WHEN AN INTERRUPT             34000020
*                       TO MAINLINE OR ATTEN. EXIT OCCURS               36000020
**/                                                                     38000020
         MEND                                                           40000020
* %TAIEBSL :  ;                                                         42000020
* DECLARE                                                               44000020
*   1 TAIE     BASED(TAIEPTR),                                          46000020
*    2 TAIEMSGL FIXED(15),           /* MESSAGE LENGTH               */ 62000020
*    2 TAIETGET PTR(8),              /* RET CODE FROM TGET ISSUED BY    64000020
*                                       ATTN PROL LOG TO BE CHECKED     66000020
*                                       BY USER ATTN RTN             */ 68000020
*    2 *        CHAR(1),             /* RESERVED                     */ 70000020
*    2 TAIEIAD  FIXED(31),           /* RIGHT HALF OF INTERRUPT PSW  */ 72000020
*    2 TAIERSAV CHAR(64);            /* REGS. STORED HERE WHEN AN       74000020
*                                       INTERRUPT TO MAINLINE OR        76000020
*                                       ATTEN. EXIT OCCURS           */ 78000020
*                                                                       80000020
