*        %GOTO TAXEBSL  ; /*                                            01000020
         MACRO                                                          02000020
         IKJTAXE                                                        03000020
TAXE     DSECT                                                          04000020
************   STANDARD IRB   *****************                         10000020
TAXEIRB  DS    24F .         IRB                                        11000020
TAXENIQE DS    A .           PTR NEXT AVAILABLE IQE                     12000020
************   STANDARD IQE   *********************                     13000020
TIQELNK  DS    A .           ADDR OF NEXT IQE ON IQE QUEUE              14000020
TIQEPARM DS    A .           PARM TO ASYNCHRONOUS EXIT ROUTINE          15000020
TIQEIRB  DS    A .           ADDR OF IRB TO BE SCHEDULED                16000020
TAXETCB  DS    A .           PTR TO TCB                                 17000020
************   WORK AREA OF IRB   *****************                     18000020
TAXELNK  DS    A .           PTR TO NEXT TAXE ON QUEUE                  19000020
**   THE FOLLOWING FIELD MUST BE ON A DOUBLE WORD BOUNDARY  **          20000020
TAXEXPSW DS    A .           LEFT HALF PSW FOR USER ATTN RTN            21000020
TAXEEXIT DS    A .           PTR TO USER ATTENTION EXIT RTN             22000020
TAXESTAT DS    0X .          STATUS OF PROGRAM ISSUING THE STAX SVC     23000020
TAXEFREQ EQU   X'20' .       STATUS FLAG FOR REQUESTED TAXE             24000020
TAXEFMOD EQU   X'40' .       STATUS FLAG FOR PROBLEM MODE               25000020
TAXEFKEY EQU   X'80' .       STATUS FLAG FOR PROBLEM KEY                26000020
TAXEPARM DS    A .           PTR TO PARAMETER LIST TO STAX              27000020
TAXETAIE DS    A .           PTR TO TAIE                                28000020
TAXEIBUF DS    A .           PTR TO USER INPUT BUFFER                   29000020
TAXEUSER DS    A .           PTR TO USER PARAMETER                      30000020
**/                                                                     31000020
         MEND                                                           32000020
* %TAXEBSL :  ;                                                         33000020
* DECLARE                                                               34000020
*   1 TAXE     BASED(TAXEPTR),                                          35000020
* /* *******   STANDARD IRB   *****************                      */ 41000020
*     2 TAXEIRB CHAR(96),         /*  IRB                */             42000020
*    2 TAXENIQE PTR(31),             /* PTR NEXT AVAILABLE IQE       */ 43000020
* /* *******   STANDARD IQE   *********************                  */ 44000020
*    2 TIQELNK  PTR(31),             /* ADDR OF NEXT IQE ON IQE         45000020
*                                       QUEUE                        */ 46000020
*    2 TIQEPARM PTR(31),             /* PARM TO ASYNCHRONOUS EXIT       47000020
*                                       ROUTINE                      */ 48000020
*    2 TIQEIRB  PTR(31),             /* ADDR OF IRB TO BE SCHEDULED  */ 49000020
*    2 TAXETCB  PTR(31),             /* PTR TO TCB                   */ 50000020
* /* *******   WORK AREA OF IRB   *****************                  */ 51000020
*    2 TAXELNK  PTR(31),             /* PTR TO NEXT TAXE ON QUEUE    */ 52000020
* /* THE FOLLOWING FIELD MUST BE ON A DOUBLE WORD BOUNDARY  **       */ 53000020
*    2 TAXEXPSW PTR(31),             /* LEFT HALF PSW FOR USER ATTN     54000020
*                                       RTN                          */ 55000020
*    2 TAXEEXIT PTR(31),             /* PTR TO USER ATTENTION EXIT      56000020
*                                       RTN                          */ 57000020
*    2 TAXEPARM PTR(31),             /* PTR TO PARAMETER LIST TO        58000020
*                                       STAX                         */ 59000020
*     3 TAXESTAT CHAR(1),            /* STATUS OF PROGRAM ISSUING       60000020
*                                       THE STAX SVC                 */ 61000020
*      4 *        BIT(1),            /* RESERVED                     */ 62000020
*      4 *        BIT(1),            /* RESERVED                     */ 63000020
*      4 TAXEFREQ BIT(1),            /* STATUS FLAG FOR REQUESTED       64000020
*                                       TAXE                         */ 65000020
*        6 TAXEFMOD BIT(1),          /* STATUS FLAG FOR PROBLEM MODE */ 66000020
*        6 TAXEFKEY BIT(1),          /* STATUS FLAG FOR PROBLEM KEY  */ 67000020
*      4 *        BIT(1),            /* RESERVED                     */ 68000020
*      4 *        BIT(1),            /* RESERVED                     */ 69000020
*      4 *        BIT(1),            /* RESERVED                     */ 70000020
*      4 *        BIT(1),            /* RESERVED                     */ 71000020
*      4 *        BIT(1),            /* RESERVED                     */ 72000020
*    2 TAXETAIE PTR(31),             /* PTR TO TAIE                  */ 73000020
*    2 TAXEIBUF PTR(31),             /* PTR TO USER INPUT BUFFER     */ 74000020
*    2 TAXEUSER PTR(31);             /* PTR TO USER PARAMETER        */ 75000020
*                                                                       76000020
