         MACRO                                                          00700020
         IKJTSB                                                         01400020
TSB      DSECT                                                          02100020
*********************************************************************** 02800020
*                                                                       03500020
*                                                                       04200020
*        THE TSB WILL CONTAIN INFORMATION PERTAINING TO A TERMINAL      04900020
*        USERS STATUS.  IT WILL BE OBTAINED DURING TSC INITIALIZATION   05600020
*        AND WILL RESIDE IN THE TSC REGION.  A TSB FOR EACH POSSIBLE    06300020
*        USER WILL BE MAINTAINED IN A CONTIGUOUS TABLE OF TSB'S.        07000020
*                                                                       07700020
*                                                                       08400020
*********************************************************************** 09100020
TSBSTAT  DS    X .                      TERMINAL STATUS BYTE            09800020
*                       BIT DEFINITIONS                                 10500020
*        BIT   6                        RESERVED                        11200020
TSBINUSE EQU   X'80' .                  TSB IN USE                      11900020
TSBLWAIT EQU   X'40' .                  KEYBOARD LOCKED DUE TO A LACK   12600020
*                                       OF INPUT BUFFERS                13300020
TSBDSPLY EQU   X'20' .                  TSB REPRESENTS A DISPLAY SCREEN 14000020
TSBNOBUF EQU   X'10' .                  INDICATES TPUT FOUND NO BUFFERS 14700020
TSBITOFF EQU   X'08' .                  PROHIBIT NON-SUPERVISORY INTER- 15400020
*                                       TERMINAL MSGS TO USERS TERMINAL 16100020
TSBDISC  EQU   X'04' .                  TSB HAS BEEN THRU LOGOFF        16800020
TSB3270  EQU   X'02' .                  3270 TERMINAL DISPLAY           16850000
TSBATNLD EQU   X'01' .                  ATTN FOR INPUT LINE DELETE      17500020
*                                                                       18200020
TSBTJB   DS    AL3 .                    ADDR OF TJB ASSOCIATED WITH     18900020
*                                       THIS TERMINAL.  IF ZERO, NO JOB 19600020
*                                       IS ASSOCIATED                   20300020
TSBFLG1  DS    X .                      FIRST FLAG BYTE                 21000020
*                       BIT DEFINITIONS                                 21700020
TSBANSR  EQU   X'80' .                  ATTN SIMULATION REQUESTED       23100020
TSBOFLSH EQU   X'40' .                  OUTPUT TRAILER Q IS TO BE       23800020
*                                       FLUSHED                         24500020
TSBOWIP  EQU   X'20' .                  A TPUT IS IN PROGRESS           25200020
TSBWOWIP EQU   X'10' .                  WAITING IN OWAIT IN PROGRESS    25900020
TSBIFLSH EQU   X'08' .                  INPUT QUEUE FLUSH IN PROGRESS   26600020
TSBTJOW  EQU   X'04' .                  TJID TPUT ENCOUNTERED OWIP      27300020
TSBTJIP  EQU   X'02' .                  A TJID TPUT IS IN PROGRESS      27600020
TSBTJBF  EQU   X'01' .                  TJID TPUT FOUND NO TS BUFFERS   28000020
*                                                                       28700020
TSBWTCB  DS    AL3 .                    ADDR OF TCB OF TASK WAITING ON  29100020
*                                       TSBECB                          29500020
TSBLNSZ  DS    X .                      PHYSICAL LINE SIZE OF TERMINAL  30100020
TSBOTBFP DS    AL3 .                    PTR TO TRAILER BUFFER(S) AFTER  30800020
*                                       HEADER BUFFER FOR MSG HAS BEEN  31500020
*                                       REMOVED                         32200020
TSBNOBF  DS    X .                      NO. OF BUFFERS ON OUTPUT QUEUE  32900020
TSBOBFP  DS    AL3 .                    PTR TO OUTPUT BUFFER QUEUE      33600020
TSBFLG2  DS    X .                      SECOND FLAG BYTE                34300020
*                       BIT DEFINITIONS                                 35000020
TSBBIPI  EQU   X'80' .                  PARTIAL LINE PROMPTING COMPLETE 35700020
TSBAUTON EQU   X'40' .                  AUTO PROMPTING REQUESTED        36400020
TSBBRKIN EQU   X'20' .                  BREAKIN HAS OCCURED             37100020
TSBAULST EQU   X'10' .                  AUTO LINE NUMBERING STARTED     37800020
TSBAUTOC EQU   X'08' .                  AUTO CHARACTER PROMPT STARTED   38500020
TSBSTAUT EQU   X'04' .                  PROMPT USER WITH NEXT LINE NO.  39200020
TSBSATN1 EQU   X'02' .                  BITS 6 AND 7 ARE USED TO IND    39900020
TSBSATN2 EQU   X'01' .                  THE NO. OF CHARS (1-4) IN THE   40600020
*                                       CHAR STRING FOR SIMULATED ATTN  41300020
*                                                                       42000020
TSBITBFP DS    AL3 .                    PTR TO INPUT TRAILER BUFFERS    42700020
*                                       RESULTING FROM TGET WITH        43400020
*                                       INSUFFICIENT BUFFER SIZE        44100020
TSBNIBF  DS    X .                      NO. OF BUFFERS ON INPUT QUEUE   44800020
TSBIBFP  DS    AL3 .                    PTR TO INPUT BUFFER QUEUE       45500020
TSBFLG3  DS    X .                      THIRD FLAG BYTE                 46200020
*                       BIT DEFINITIONS                                 46900020
*        BITS  4 - 7                    RESERVED                        47600020
TSBATTN  EQU   X'80' .                  ATTENTION HAS BEEN IGNORED      48300020
TSBTJMSG EQU   X'40' .                  TSOUTPUT PROCESSING TJID MSG    49000020
TSBSPIT  EQU   X'20' .                  STOP PROMTING IF TCLEARQ OR     49200020
*                                       STBREAK                         49400020
TSBNBKSP EQU   X'10' .                  NEXT CHAR IN USER'S BFFR IS A   49700020
*                                       BACKSPACE CHAR                  50400020
TSBNFLOP EQU   X'01' .                  TSB FLASHBACH BIT      @SA72908 50700021
*                                                                       51100020
TSBQCB   DS    AL3 .                    PTR TO TERMINAL DESTINATION QCB 51800020
TSBECB   DS    F .                      ECB FOR INTER-TERMINAL COM-     52500020
*                                       MUNICATION (TPUT WITH TJID)     53200020
TSBWTJID DS    H .                      TJID OF TASK WAITING ON TSBECB  53900020
TSBSTCC  EQU   * .                      SPECIAL USER CHAR FIELD         54600020
TSBLNDCC DS    CL1 .                    LINE DELETE CHARACTER           55300020
TSBCHDCC DS    CL1 .                    CHARACTER DELETE CHARACTER      56000020
TSBATNLC DS    CL2 .                    NO. OF SUCCESSIVE OUTPUT LINES  56700020
*                                       BETWEEN ATTENTION SIMULATION    57400020
TSBATNTC DS    CL2 .                    NUMBER OF CONTINUOUS 1-SECOND   58100020
*                                       TIME INTERVALS                  58800020
TSBLNNO  DS    CL1 .                    NO. OF LINES ON A DISPLAY       59500020
*                                       SCREEN                          60200020
*                                                                       60900020
         DS    CL1 .                    RESERVED                        61600020
*                                                                       62300020
TSBASRCE DS    CL2 .                    EQUIV. TO PRFSRCE IN TCAM BFR   63000020
*                                       PREFIX.  CONTAINS INFO SUCH AS  63700020
*                                       POLLING INDEX THAT IS TO BE PUT 64400020
*                                       IN EACH TCAM BFR BY TSOUTPUT    65100020
*                                       SIMULATION                      65800020
TSBATNCC DS    CL4 .                    CHARACTER STRING USED FOR       66500020
*                                       ATTENTION SIMULATION            67200020
TSBAUTOS DS    F .                      STARTING AND CURRENT SEQ NO.    67900020
*                                       FOR AUTO LINE NUMBERING         68600020
TSBAUTOI DS    F .                      INCREMENT VALUE FOR AUTO LINE   69300020
*                                       NUMBERING                       70000020
TSBERSDS DS    F .                      CHARS USED TO ERASE SCREEN      70700020
*                                                                       71400020
TSBCTCB  DS    F .                      TCB ADDRESS OF TASK CURRENTLY   71600020
*                                       DOING A TPUT                    71800020
TSBEND   EQU   * .                      MARKS THE END OF THE TSB        72100020
*                                                                       72800020
*        *************************************************************  73500020
*    0   *   TSBSTAT    *                  TSBTJB                    *  74200020
*        *************************************************************  74900020
*    4   *   TSBFLG1    *                  TSBWTCB                   *  75600020
*        *************************************************************  76300020
*    8   *   TSBLNSZ    *                  TSBOTBFP                  *  77000020
*        *************************************************************  77700020
*   12   *   TSBNOBF    *                  TSBOBFP                   *  78400020
*        *************************************************************  79100020
*   16   *   TSBFLG2    *                  TSBITBFP                  *  79800020
*        *************************************************************  80500020
*   20   *   TSBNIFB    *                  TSBIBFP                   *  81200020
*        *************************************************************  81900020
*   24   *   TSBFLG3    *                  TSBQCB                    *  82600020
*        *************************************************************  83300020
*   28   *                         TSBECB                            *  84000020
*        *************************************************************  84700020
*   32   *           TSBWTJID          *           TSBSTCC           *  85400020
*        *************************************************************  86100020
*   36   *           TSBATNLC          *           TSBATNTC          *  86800020
*        *************************************************************  87500020
*   40   *   TSBLNNO    *   RESERVED   *           TSBASRCE          *  88200020
*        *************************************************************  88900020
*   44   *                         TSBATNCC                          *  89600020
*        *************************************************************  90300020
*   48   *                         TSBAUTOS                          *  91000020
*        *************************************************************  91700020
*   52   *                         TSBAUTOI                          *  92400020
*        *************************************************************  93100020
*   56   *                         TSBERSDS                          *  93800020
*        *************************************************************  94500020
*   60   *                         TSBCTCB                           *  94700020
*        *************************************************************  94900020
*                                                                       95200020
         SPACE 3                                                        95900020
         MEND                                                           96600020
