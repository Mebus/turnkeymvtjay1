*        %GOTO UPTBSL ;  /*                                             01000020
         MACRO                                                          02000000
         IKJUPT                                                         03000000
.* A 0-999999                                                    S20031 03050020
*********************************************************************** 05000000
*    THE USER PROFILE TABLE (UPT) IS BUILT BY THE LOGON/LOGOFF        * 06000020
*    SCHEDULER FROM INFORMATION STORED IN THE UADS AND FROM THE       * 07000020
*    LOGON COMMAND.  IT IS IN CORE SHARED BY THE TMP AND THE          * 07050020
*    LOGON/LOGOFF SCHEDULER.  CP'S AND SERVICE ROUTINES CAN MODIFY    * 08000020
*    THE UPT, BUT THEY CANNOT FREE IT.                                * 09000020
*********************************************************************** 10000000
UPT      DSECT                                                          10050020
         DS    0F                                                       11000000
         DS    CL2      RESERVED                                        12000000
UPTUSER  DS    CL10     RESERVED FOR INSTALLATION USE                   13000000
UPTSWS   DS    X        USERS ENVIRONMENT SWITCHES                      14000000
*        EQU   X'80'    RESERVED                                        15000000
UPTNPRM  EQU   X'40'    NO PROMPTING IS TO BE DONE                      16000020
UPTMID   EQU   X'20'    PRINT MESSAGE IDENTIFIERS                       17000020
UPTNCOM  EQU   X'10'    NO USER COMMUNICATION ALLOWED VIA SEND COMMAND  18000020
UPTPAUS  EQU   X'08'    PAUSE FOR '?' WHEN IN NON-INTERACTIVE MODE      19000020
UPTALD   EQU   X'04'    ATTN HAS BEEN SPECIFIED AS LINE DELETE CHAR     21000020
UPTCDEL  DS    CL1      CHAR DELETE CHARACTER                           23000000
UPTLDEL  DS    CL1      LINE DELETE CHARACTER                           24000000
         DS    CL1      RESERVED                                        25000000
**/                                                                     26000000
         MEND                                                           27000000
* %UPTBSL: ;                                                            28000020
* /* *************************************************************** */ 31000000
* /* THE USER PROFILE TABLE (UPT) IS BUILT BY THE LOGON/LOGOFF       */ 32000020
* /* SCHEDULER FROM INFORMATION STORED IN THE UADS AND FROM THE      */ 33000020
* /* LOGON COMMAND.  IT IS IN CORE SHARED BY THE TMP AND THE         */ 33050020
* /* LOGON/LOGOFF SCHEDULER.  CP'S AND SERVICE ROUTINES CAN MODIFY   */ 34000020
* /* THE UPT, BUT THEY CANNOT FREE IT.                               */ 35000020
* /* *************************************************************** */ 36000000
* DECLARE                                                               36050020
*   1 UPT BASED(UPTPTR),                                                36100020
*    2 *        CHAR(2) BDY(WORD),   /* RESERVED                     */ 39000020
*    2 UPTUSER  CHAR(10),            /* RESERVED FOR INSTALLATION    */ 40000020
*    2 UPTSWS   CHAR(1),             /* USERS ENVIRONMENT SWITCHES   */ 42000020
*     3 *        BIT(1),             /* RESERVED                     */ 44000000
*     3 UPTNPRM  BIT(1),             /* NO PROMPTING TO BE DONE      */ 45000020
*     3 UPTMID   BIT(1),             /* PRINT MESSAGE IDENTIFIERS    */ 47000020
*     3 UPTNCOM  BIT(1),             /* NO USER COMMUNICATION           49000020
*                                       ALLOWED VIA SEND COMMAND     */ 50000020
*     3 UPTPAUS  BIT(1),             /* PAUSE FOR '?' WHEN IN NON-      51000020
*                                       INTERACTIVE MODE             */ 52000020
*     3 UPTALD   BIT(1),             /* ATTN HAS BEEN SPECIFIED AS      54000020
*                                       THE LINE DELETE CHARACTER    */ 55000020
*     3 *        BIT(1),             /* RESERVED                     */ 58000000
*     3 *        BIT(1),             /* RESERVED                     */ 59000000
*    2 UPTCDEL  CHAR(1),             /* CHAR DELETE CHARACTER        */ 60000000
*    2 UPTLDEL  CHAR(1),             /* LINE DELETE CHARACTER        */ 61000000
*    2 *        CHAR(1);             /* RESERVED                     */ 62000000
*                                                                       63000000
