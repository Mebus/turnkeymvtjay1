         MACRO                                                          02000000
&NAME    SPAUTOPT &A                                                    04000000
.********************************************************************** 06000000
.*                                                                    * 08000000
.*                                                                    * 10000000
.*RELEASE 23 DELETIONS                                                * 12000000
.*                                                                    * 14000000
.*RELEASE 22 DELETIONS                                                * 16000000
.*                                                                    * 18000000
.*RELEASE 21 DELETIONS                                                * 20000000
.*                                                                    * 22000000
.*STATUS - CHANGE LEVEL 000                                           * 24000000
.*                                                                    * 26000000
.*NAME - SPAUTOPT                                                     * 28000000
.*                                                                    * 30000000
.*FUNCTION - THE SPAUTOPT MACRO ALLOWS THE USER TO TERMINATE AUTOMA-  * 32000000
.*   TIC LINE NUMBERING OR AUTOMATIC CHARACTER PROMPT.                * 34000000
.*                                                                    * 36000000
.*       THE MACRO PROTOTYPE IS ABOVE                                 * 38000000
.*                                                                    * 40000000
.*       THE STANDARD FORM OF THE EXPANSION IS BELOW                  * 42000000
.*                                                                    * 44000000
.*&NAME  SR    1,1                      PREPARE PARM                  * 46000000
.*       LA    0,13                     LOAD ENTRY CODE               * 48000000
.*       SLL   0,24                     PUT ENTRY CODE IN HIGH ORDER  * 50000000
.*                                      BYTE OF REGISTER              * 52000000
.*       SVC   94                       ISSUE SVC                     * 54000000
.*                                                                    * 56000000
.********************************************************************** 58000000
         AIF   ('&A' NE '').ERROR                                       60000000
&NAME    SR    1,1                      PREPARE PARM                    62000000
         LA    0,13                     LOAD ENTRY CODE                 64000000
         SLL   0,24                     PUT ENTRY CODE IN HIGH ORDER    66000000
.*                                      BYTE IN REG                     68000000
         SVC   94                       ISSUE SVC                       70000000
         MEXIT                                                          72000000
.ERROR   ANOP                                                           74000000
         IHBERMAC 36,,&A                                                76000000
         MEXIT                                                          78000000
         MEND                                                           80000000
