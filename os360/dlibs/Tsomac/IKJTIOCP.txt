         MACRO                                                          01000000
         IKJTIOCP                                                       02000000
TIOCRPT  DSECT                                                          03000000
*********************************************************************** 04000000
*                                                                       05000000
*                                                                       06000000
*        THE TIOC REFERENCE POINTER TABLE CONTAINS INFORMATION REQUIRED 07000000
*        BY THE TIOC TASK, THE CEA, AND THE SVC ROUTINES.  IT RESIDES   08000000
*        IN THE TSC REGION AND IS CREATED AND INITIALIZED BY THE TIOC   09000000
*        CONTROL BLOCK INITIALIZATION MODULE.                           10000000
*                                                                       11000000
*                                                                       12000000
*********************************************************************** 13000000
         DS    0D                                                       14000000
TIOCFBFF DS    A .                      FREE TS BUFFER QUEUE            15000000
TIOCNBF  DS    CL2 .                    NUMBER OF TS BUFFERS            16000000
TIOCNFBF DS    CL2 .                    NUMBER OF TS BFRS ON FREE QUEUE 17000000
TIOCBFSZ DS    CL2 .                    TS BUFFER SIZE - IN BYTES       18000000
TIOCLDCU DS    CL2 .                    NUMBER OF TERMINALS AT LAST     19000000
*                                       BUFFER CALCULATION              20000000
TIOCOMAX DS    CL2 .                    MAXIMUM NUMBER OF OUTPUT BFRS   21000000
*                                       ALLOWED EACH TERMINAL           22000000
TIOCIMAX DS    CL2 .                    MAXIMUM NUMBER OF INPUT BFRS    23000000
*                                       ALLOWED EACH TERMINAL           24000000
TIOCOWTH DS    CL2 .                    OWAIT THRESHOLD                 25000000
TIOCRSTH DS    CL2 .                    RESTART THRESHOLD               26000000
TIOCFLG  DS    X .                      FLAG BYTE                       27000000
*                       BIT DEFINITIONS                                 28000000
*        BITS  1-2,5-7                  RESERVED                        29000000
TIOCSYLW EQU   X'80' .                  SYSTEM IS IN LWAIT              30000000
TIOCTJBF EQU   X'10' .                  TPUT W/TJID FOUND NO TS BUFFERS 31000000
TIOCNOBF EQU   X'08' .                  TPUT FOUND NO TS BUFFERS ON     32000000
*                                       EITHER FREE OR OUTPUT QUEUE     33000000
TIOCLOW  EQU   X'04' .                  LOGOFF WAITING FOR TIOC         33300020
*                                       TO FINISH PROCESSING            33600020
TIOCUSCH DS    CL1 .                    % LOGGED ON USER CHANGE.  THIS  34000000
*                                       INDICATES WHEN TO RECALCULATE   35000000
*                                       TIOCAIMX AND TIOCAOMX           36000000
TIOCUSCT DS    H .                      TIOC USER COUNT                 37000020
TIOCAOMX DS    CL2 .                    ADJUSTED (ACCORDING TO CURRENT  38000000
*                                       USAGE) MAXIMUM NUMBER OF OUTPUT 39000000
*                                       BUFFERS ALLOWED EACH TERMINAL   40000000
TIOCAIMX DS    CL2 .                    ADJUSTED (ACCORDING TO CURRENT  41000000
*                                       USAGE) MAXIMUM NUMBER OF INPUT  42000000
*                                       BUFFERS ALLOWED EACH TERMINAL   43000000
TIOCUSLW DS    CL2 .                    NO. OF BFRS THAT ARE RESERVED   44000000
*                                       ON THE FREE QUEUE.  LESS THAN   45000000
*                                       THIS AMOUNT RESULTS IN AN LWAIT 46000000
TIOCUSSL DS    CL2 .                    NO. OF USERS THAT CONSTITUTES   47000000
*                                       SLACK TIME                      48000000
TIOCTSBS DS    X .                      SIZE OF TSB'S IN SYSTEM SET BY  49000000
*                                       START TS                        50000000
TIOCTSB  DS    AL3 .                    ADDRESS OF THE TSB TABLE        51000000
TIOCSAVE DS    CL48 .                   REGISTER SAVE AND WORK AREA     51500020
TIOCLECB DS    F .                      LOGOFF WAITS ON THIS            51600020
*                                       ECB UNTIL TIOC                  51700020
*                                       FINISHES PROCESSING             51800020
*                                       THE LAST USER                   51900020
TIOCEND  EQU   * .                      MARK THE END OF TIOCRPT         52000000
*                                                                       53000000
*        *************************************************************  54000000
*    0   *                         TIOCFBFF                          *  55000000
*        *************************************************************  56000000
*    4   *           TIOCNBF           *           TIOCNFBF          *  57000000
*        *************************************************************  58000000
*    8   *           TIOCBFSZ          *           TIOCLDCU          *  59000000
*        *************************************************************  60000000
*   12   *           TIOCOMAX          *           TIOCIMAX          *  61000000
*        *************************************************************  62000000
*   16   *           TIOCOWTH          *           TIOCRSTH          *  63000000
*        *************************************************************  64000000
*   20   *   TIOCFLG    *   TIOCUSCH   *           TIOCUSCT          *  65000020
*        *************************************************************  66000000
*   24   *           TIOCAOMX          *           TIOCAIMX          *  67000000
*        *************************************************************  68000000
*   28   *           TIOCUSLW          *           TIOCUSSL          *  69000000
*        *************************************************************  70000000
*   32   *   TIOCTSBS   *                  TIOCTSB                   *  71000000
*        *************************************************************  72000000
*   36   *   TIOCSAVE (48 BYTES)                                     *  72300020
*        *************************************************************  72600020
*   84   *                         TIOCLECB                          *  72700020
*        *************************************************************  72800020
*                                                                       73000000
         SPACE 3                                                        74000000
         MEND                                                           75000000
