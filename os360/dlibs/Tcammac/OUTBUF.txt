         MACRO                                                          02000000
&NAME    OUTBUF &PATH=                                                  04000000
         GBLA  &IEDQZE(64)                                              06000000
         GBLB  &IEDQZA(64)                                              08000000
         GBLC  &IEDQZC,&IEDQZJ                                          10000000
         LCLB  &NOGO                                                    11000000
.*             VERSION DATED SEPTEMBER 28, 1970                         12000000
         SPACE                                                          14000000
&IEDQZA(6) SETB 1                                                       16000000
         AIF   (NOT &IEDQZA(46)).ERR1                                   17000000
         AIF   (NOT &IEDQZA(28)).SEQ                                    18000000
.ERR1    MNOTE 12,'***  IHB070  SEQUENCE ERROR - MUST FOLLOW STARTMH MA*19000000
               CRO'                                                     22000000
&NOGO    SETB  1                                                        24000000
.SEQ     AIF   (NOT &IEDQZA(1)).PARM                                    26000000
         MNOTE 12,'***  IHB070  SEQUENCE ERROR - MUST NOT FOLLOW INCOMIN28000000
               NG DELIMITER GROUP'                                      30000000
&NOGO    SETB  1                                                        32000000
.PARM    AIF   ('&PATH' EQ '').GLOB                                     34000000
         AIF   ('&PATH(1)' EQ '' OR '&PATH(2)' EQ '').ERR               35000000
         AIF   ('&PATH(2)' EQ '0' OR '&PATH(2)' EQ 'X''0''' OR '&PATH(2*36000000
               )' EQ 'X''00''' OR '&PATH(2)'(1,2) EQ 'XL').ERR          37000000
         IEDQMASK &PATH(2),1                                            38000000
         AIF   (NOT &IEDQZA(24)).GLOB                                   39000000
.ERR     MNOTE 12,'***  IHB300  PATH OPERAND INVALID AS SPECIFIED'      40000000
&NOGO    SETB  1                                                        41000000
.GLOB    AIF   (&NOGO).MEND                                             42000000
         AIF   ('&NAME' EQ '').NON                                      43000000
&NAME    DS    0H                                                       48000000
.NON     ANOP                                                           50000000
&IEDQZA(1) SETB 0                                                       52000000
&IEDQZA(2) SETB 1                                                       54000000
&IEDQZA(3) SETB 0                                                       56000000
&IEDQZA(4) SETB 0                                                       58000000
&IEDQZA(5) SETB 0                                                       60000000
&IEDQZC  DS    0H                                                       62000000
&IEDQZC  SETC  'IEDS'.'&SYSNDX'                                         64000000
         AIF   (NOT &IEDQZA(20) OR '&IEDQZJ' EQ '').A                   67000000
&IEDQZJ  DS    0H                                                       70000000
.A       AIF   ('&PATH' EQ '').MEND                                     73000000
         LOCOPT &PATH(1)                                                76000000
         AIF   (&IEDQZA(61)).MEND                                       78000000
         LTR   15,15 .                  OPTION FIELD FOUND              80000000
         BZ    &IEDQZC .                NO - GO TO NEXT DELIM           82000000
         TM    0(15),&PATH(2) .         PATH SWITCHES ON                84000000
         BZ    &IEDQZC .                NO - GO TO NEXT DELIM           86000000
.MEND    SPACE 2                                                        88000000
&IEDQZJ  SETC  ''                                                       90000000
&IEDQZA(20) SETB 0                                                      94000000
         MEND                                                           96000000
