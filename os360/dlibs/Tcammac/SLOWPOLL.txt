         MACRO                                                          00700022
&NAME    SLOWPOLL &MASK,&CONNECT=OR,&SECONDS=60                         01400022
.*                                                                      02100022
.* VERSION = 00, LEVEL = 00                                             02800022
.*                                                                      03500022
         GBLA  &IEDQZE(64)                                              04200022
         GBLB  &IEDQZA(64)                                              04900022
         LCLA  &ALG                                                     05600022
         LCLB  &NOGO                                                    06300022
.*                                                                      07000022
.*                                                                      07700022
.********************************************************************** 08400022
.*                                                                    * 09100022
.* THE SLOWPOLL MACRO -                                               * 09800022
.*   * DELAYS FURTHER POLLING WHEN A LINE ERROR OCCURS,               * 10500022
.*   * IS OPTIONAL IN AN INMESSAGE SUBGROUP OF AN MH (AND NOT         * 11200022
.*     PERMITTED IN ANY OTHER SUBGROUP),                              * 11900022
.*   * MAY BE USED MORE THAN ONCE IN A SUBGROUP.                      * 12600022
.*                                                                    * 13300022
.* SLOWPOLL SUSPENDS FURTHER POLLING ON A GIVEN LINE WHEN ERRORS      * 14000022
.* SPECIFIED BY THE ERROR MASK HAVE OCCURRED. THE BITS SPECIFIED BY   * 14700022
.* THE ERROR MASK OPERAND ARE COMPARED TO THE SETTING OF THE BITS IN  * 15400022
.* THE MESSAGE ERROR RECORD FOR THIS MESSAGE - IF SPECIFIED BITS IN   * 16100022
.* THE MESSAGE ERROR RECORD ARE ON, THE POLLING WILL BE SUSPENDED. BY * 16800022
.* SPECIFYING AN ALL-ZERO MASK, OR BY OMITTING THE MASK OPERAND, THE  * 17500022
.* ACTION WILL BE UNCONDITIONAL. POLLING WILL RESUME AFTER THE LENGTH * 18200022
.* OF TIME SPECIFIED IN THE SECONDS OPERAND.                          * 18900022
.*                                                                    * 19600022
.* ****************************************************************** * 20300022
.* *        *        *                 .   .             .       .  * * 21000022
.* *(SYMBOL)*SLOWPOLL*(MASK)(,CONNECT=. AND .)(,SECONDS=. INTEGER .)* * 21700022
.* *        *        *                . OR  .           . 60      . * * 22400022
.* *        *        *                 .## .             .##     .  * * 23100022
.* ****************************************************************** * 23800022
.*                                                                    * 24500022
.*   SYMBOL -                                                         * 25200022
.*     FUNCTION - NAME OF THE MACRO.                                  * 25900022
.*     DEFAULT - NONE. SPECIFICATION IS OPTIONAL.                     * 26600022
.*     FORMAT - MUST CONFORM TO THE RULES FOR ASSEMBLER LANGUAGE      * 27300022
.*              SYMBOLS (SEE THE SYMBOL ENTRY IN THE GLOSSARY).       * 28000022
.*                                                                    * 28700022
.*   MASK -                                                           * 29400022
.*     FUNCTION - SPECIFIES THE FIVE-BYTE BIT CONFIGURATION USED TO   * 30100022
.*                TEST THE MESSAGE ERROR RECORD FOR THE MESSAGE (THE  * 30800022
.*                MESSAGE ERROR RECORD IS DESCRIBED IN APPENDIX B).   * 31500022
.*     DEFAULT - NONE. SPECIFICATION OPTIONAL.                        * 32200022
.*     FORMAT - DECIMAL OR HEXADECIMAL. IF HEXADECIMAL FORMAT IS      * 32900022
.*              USED, FRAMING CHARACTERS MUST BE USED. IF X'' IS      * 33600022
.*              USED, LEADING ZEROES MUST BE CODED, IF XL5'' IS USED, * 34300022
.*              LEADING ZEROES MAY BE OMITTED.                        * 35000022
.*     MAXIMUM - 16777215 OR A HEXADECIMAL FIELD FIVE BYTES IN        * 35700022
.*               LENGTH.                                              * 36400022
.*     NOTES - OMITTING THIS OPERAND OR SPECIFYING AN ALL-ZERO MASK   * 37100022
.*             CAUSES UNCONDITIONAL EXECUTION.                        * 37800022
.*                                                                    * 38500022
.*            .   .                                                   * 39200022
.*   CONNECT=. AND . -                                                * 39900022
.*           . OR  .                                                  * 40600022
.*            .## .                                                   * 41300022
.*     FUNCTION - SPECIFIES THE TYPE OF LOGICAL CONNECTION TO BE MADE * 42000022
.*                BETWEEN THE MASK AND THE MESSAGE ERROR RECORD.      * 42700022
.*     DEFAULT - CONNECT=OR.                                          * 43400022
.*     FORMAT - AND OR OR.                                            * 44100022
.*     NOTES - AND SPECIFIES THAT THE MACRO IS TO BE EXECUTED ONLY IF * 44800022
.*             ALL OF THE BITS SPECIFIED BY MASK ARE ON IN THE        * 45500022
.*             MESSAGE ERROR RECORD. OR SPECIFIES THAT THE MACRO IS   * 46200022
.*             TO BE EXECUTED IF ANY BIT SPECIFIED BY MASK IS ON IN   * 46900022
.*             THE MESSAGE ERROR RECORD.                              * 47600022
.*                                                                    * 48300022
.*            .       .                                               * 49000022
.*   SECONDS=. INTEGER . -                                            * 49700022
.*           . 60      .                                              * 50400022
.*            .##     .                                               * 51100022
.*     FUNCTION - SPECIFIES THE LENGTH OF TIME, EXPRESSED IN SECONDS, * 51800022
.*                DURING WHICH POLLING ON THE LINE WILL BE SUSPENDED. * 52500022
.*     DEFAULT - SECONDS=60.                                          * 53200022
.*     FORMAT - DECIMAL.                                              * 53900022
.*     MINIMUM - 1.                                                   * 54600022
.*     MAXIMUM - 65535.                                               * 55300022
.*                                                                    * 56000022
.********************************************************************** 56700022
.*                                                                      57400022
.*                                                                      58100022
         AIF   (NOT &IEDQZA(9)).B20                                     58800022
         MNOTE 12,'***  IHB312  MUST FOLLOW STARTMH MACRO WITH TSOMH=NO*59500022
               '                                                        60200022
         AGO   .MEND                                                    60900022
.*                                                                      61600022
.B20     AIF   (&IEDQZA(5)).B30                                         62300022
         MNOTE 12,'***  IHB070  SEQUENCE ERROR-MUST FOLLOW INMSG OR OUT*63000022
               MSG MACRO'                                               63700022
&NOGO    SETB  1                                                        64400022
.B30     AIF   ('&MASK' EQ '').B50                                      65100022
         IEDQMASK &MASK,5                                               65800022
         AIF   (&IEDQZA(24)).B40                                        66500022
         AIF   ('&MASK'(1,1) NE 'X').B50                                67200022
         AIF   ('&MASK'(2,1) EQ 'L' AND '&MASK'(3,1) NE '5').B40        67900022
         AIF   ('&MASK'(2,1) NE 'L' AND K'&MASK NE 13).B40              68600022
         AGO   .B50                                                     69300022
.*                                                                      70000022
.B40     MNOTE 12,'***  IHB300  MASK OPERAND INVALID AS SPECIFIED'      70700022
&NOGO    SETB  1                                                        71400022
.B50     AIF   (&SECONDS GT 0 AND &SECONDS LT 65536).B60                72100022
         MNOTE 12,'***  IHB300  SECONDS OPERAND INVALID AS SPECIFIED'   72800022
&NOGO    SETB  1                                                        73500022
.B60     AIF   ('&CONNECT' EQ 'AND' OR '&CONNECT' EQ 'OR').B70          74200022
         MNOTE 12,'***  IHB300  CONNECT OPERAND INVALID AS SPECIFIED'   74900022
&NOGO    SETB  1                                                        75600022
.B70     AIF   (&NOGO).MEND                                             76300022
         SPACE 1                                                        77000022
         IEDQVCON 60,IEDQB4                                             77700022
         AIF   ('&MASK' NE '0' AND '&MASK' NE 'X''0000000000''' AND '&M*78400022
               ASK' NE 'XL5''0000000000''' AND '&MASK' NE '').C50       79100022
&NAME    DC    AL1(&IEDQZE(60)+1),AL1(8),AL2(0)                         79800022
         AGO   .C80                                                     80500022
.*                                                                      81200022
.C50     AIF   ('&CONNECT' EQ 'OR').C60                                 81900022
&ALG     SETA  1                                                        82600022
.C60     ANOP                                                           83300022
&NAME    DC    AL1(&IEDQZE(60)),AL1(12+&ALG),AL1(0)                     84000022
         AIF   ('&MASK'(1,1) EQ 'X').C70                                84700022
         DC    FL5'&MASK'          ERROR MASK                           85400022
         AGO   .C80                                                     86100022
.*                                                                      86800022
.C70     DC    &MASK               ERROR MASK                           87500022
.C80     DC    AL2(&SECONDS),AL2(0)                                     88200022
.MEND    SPACE 1                                                        88900022
         MEND                                                           89600022
