         MACRO                                                          00700020
&NAME    TPROCESS   &PCB=,&QUEUES=,&ALTDEST=,&OPDATA=,&SECTERM=NO,     *01400020
               &SYNC=NO,&CKPTSYN=NO,&X=(A,B,C,D,E,F),&SECURE=NO, S22024*02100022
               &QS=(DN,DR,MO,MR,MN,34,18,66,82,98),&LEVEL=(0),         *02800020
               &RECDEL=00,&DATE=NO,&QBACK=NO                     S22027 03500022
         GBLB  &IEDQZA(64)                                              04200020
         GBLB  &IEDQFL(16)                                       S22025 04500022
         GBLA  &IEDQAR(64)                                              04900020
         GBLC  &IEDQSV(8)                                               05600020
         LCLA  &A,&L                                                    06300020
         LCLB  &NO,&B(8)                                                07000020
         LCLB  &SEC                                              S22024 07010022
         LCLC  &V                                                       07700020
.*  CHANGE ACTIVITY AS FOLLOWS                                          07720005
.******************* MICROFICHE FLAGS *********************** SUPT CODE 07723005
.*A553000                                                        S22027 07726005
.*C035000,553000                                                 S22027 07726105
.*A840000                                                        S99528 07726205
.*A080000,493000,500000                                          S22029 07726305
.*A490000,497000                        CHANGED 10/15/71         A44888 07726405
.*C868000                                                        S21101 07726505
.*A519000-524000,556000                                          S22025 07726605
.*C035000,511000                                                 S22025 07726705
.*D707000                                                        S22025 07726805
.*A602000                                                       SA63593 07726905
&IEDQFL(9) SETB 0                                                S22025 08360022
.*A070000,553020-553120                CHANGED 09/25/72          S22024 08370022
.*C553900,555100                       CHANGED 09/25/72          S22024 08380022
.*C521000-524000                                               @OY15040 08380157
         AIF   (&IEDQAR(10) EQ 0).C0                                    08400020
         AIF   ('&NAME' EQ '&IEDQSV(1)' AND &IEDQAR(10) EQ 2).OPT       09100020
         IEDQGCH 2                                                      09800020
.C0      AIF   (&IEDQZA(55)).C1                                         10500020
         AIF   (&IEDQZA(54) EQ &IEDQZA(55) AND &IEDQZA(53) EQ &IEDQZA(5*11200020
               4)).NOTAB                                                11900020
         MNOTE 12,'*** IHB315 SEQUENCE ERROR- MUST PRECEDE LAST ENTRY  *12600020
                SPECIFIED BY TTABLE MACRO'                              13300020
         AGO   .SETNO                                                   14000020
.NOTAB   MNOTE 12,'*** IHB070 SEQUENCE ERROR- MUST FOLLOW TTABLE MACRO' 14700020
.SETNO   ANOP                                                           15400020
&NO      SETB  1                                                        16100020
.C1      AIF   ('&NAME' NE '').C1A                                      16800020
         MNOTE 12,'*** IHB076 MACRO NAME FIELD BLANK- NAME REQUIRED'    17500020
&NO      SETB  1                                                        18200020
.C1A     AIF   ('&PCB' NE '').C2                                        18900020
         MNOTE 12,'*** IHB004 REQUIRED OPERAND(S) NOT SPECIFIED'        19600020
         SPACE 2                                                        20300020
         MEXIT                                                          21000020
.C2      AIF   (&NO).C3                                                 21700020
         AIF   (K'&NAME LE &IEDQAR(1)-3).C3                             22400020
&A       SETA  &IEDQAR(1)-3                                             23100020
         MNOTE 4,'*** IHB302 NAME FIELD LONG- TRUNCATED TO &A CHARS'    23800020
         SPACE 2                                                        24500020
&A       SETA  0                                                        25200020
.C3      AIF   ('&CKPTSYN' EQ 'NO').C3A                                 25900020
&B(6)    SETB  1                                                        26600020
         AIF   ('&CKPTSYN' EQ 'YES').C3B                                27300020
         MNOTE 12,'*** IHB002 INVALID CKPTSYN OPERAND SPECIFIED- &CKPTS*28000020
               YN'                                                      28700020
&NO      SETB  1                                                        29400020
         AGO   .C3B                                                     30100020
.C3A     AIF   ('&SYNC' EQ 'NO').C3B                                    30800020
&B(6)    SETB  1                                                        31500020
         AIF   ('&SYNC' EQ 'YES').C3B                                   32200020
         MNOTE 12,'*** IHB002 INVALID SYNC OPERAND SPECIFIED- &SYNC'    32900020
&NO      SETB  1                                                        33600020
.C3B     ANOP                                                           34300020
&IEDQSV(4)  SETC  '&SYSECT'                                             35000020
         AIF   ('&QUEUES' EQ '').PUT                                    35700020
&B(5)    SETB  1                                                        36400020
.C4      AIF   (&A GT 4).ERR3                                           37100020
&A       SETA  &A+1                                                     37800020
         AIF   ('&QUEUES' NE '&QS(&A)').C4                              38500020
&A       SETA  &QS(&A+5)                                                39200020
         AIF   (&A LE 34).C5                                            39900020
         AIF   (NOT &IEDQZA(42)).ERR2                                   40600020
.C5      AIF   (K'&RECDEL EQ 2).C6                                      41300020
         MNOTE 12,'*** IHB002 INVALID RECDEL OPERAND SPECIFIED- &RECDEL*42000020
               '                                                        42700020
&NO      SETB  1                                                        43400020
.C6      AIF   (&L+1 GT N'&LEVEL-1).C7                                  44100020
&L       SETA  &L+1                                                     44800020
         AIF   ('&LEVEL(&L+1)' GT '&LEVEL(&L)').C6                      45500020
         MNOTE 12,'*** IHB002 INVALID LEVEL OPERAND SPECIFIED- &LEVEL(&*46200020
               L)'                                                      46900020
&NO      SETB  1                                                        47600020
         AGO   .C6                                                      48300020
.C7      AIF   (&NO).EXT                                                49000020
&IEDQZA(39) SETB 1                                               A44888 49300021
&IEDQZA(17) SETB 1                                               S22029 49500022
         IEDQTQ &PCB,&A,0,0,&LEVEL                                      49700020
&IEDQZA(39) SETB 0                                               A44888 50000021
&IEDQAR(5) SETA 0                                               SA51075 50200000
&IEDQZA(17) SETB 0                                               S22029 50200122
&NAME    IEDQTT                                                         50400020
         AIF   ('&OPDATA' EQ '').C7A                             S22025 51100000
&B(7)    SETB  1                                                        51800020
.C7A     AIF   ('&DATE' EQ 'NO').GEN                             S22025 51900000
         AIF   ('&DATE' NE 'YES').DATERR                       @OY15040 52100057
&B(4)    SETB  1                                               @OY15040 52160057
         AGO   .GEN                                            @OY15040 52220057
.DATERR  ANOP                                                           52280057
         MNOTE 4,'*** IHB300 DATE OPERAND INVALID AS SPECIFIED, NO ASSU*52340057
               MED'                                                     52400057
.GEN     DC    BL1'001&B(4)&B(5)&B(6)&B(7)&B(8)',AL3(&IEDQSV(2)),2H'01' 52500020
         AIF   ('&ALTDEST' EQ '').NAL                                   53200020
&A       SETA  82-&IEDQAR(1)                                            53900020
         DC    AL2((&ALTDEST-IEDQTNT-&A)/&IEDQAR(1))                    54600020
.ST      ANOP                                                    S22027 55300022
         AIF   ('&SECURE' EQ 'NO').L1                            S22024 55302022
         AIF   ('&SECURE' NE 'YES').SECERR                       S22024 55304022
&SEC     SETB  1                                                 S22024 55306022
         AGO   .L1                                               S22024 55308022
.SECERR  MNOTE 4,'*** IHB086 SECURE INVALID KEYWORD, NO ASSUMED' S22024 55310022
.L1      ANOP                                                    S22024 55312022
         AIF   ('&QBACK' EQ 'NO').ST1A                           S22027 55330022
         AIF   ('&QBACK' NE 'YES').QBERR                         S22027 55360022
         DC    BL2'000000000000000&SEC' .                        S22024 55390022
         DC    XL3'0',XL1'01',XL1'&RECDEL' .                     S22024 55400022
         AGO   .ST1                                              S22027 55420022
.QBERR   MNOTE 12,'*** IHB300 QBACK OPERAND INVALID AS SPECIFIED'       55450022
.ST1A    ANOP                                                           55480022
         DC    BL2'000000000000000&SEC' .                        S22024 55510022
         DC    XL4'0',XL1'&RECDEL' .                             S22024 55550022
.ST1     ANOP                                                    S22025 55600000
         AIF   (&IEDQZA(53)).ST2                                        56000020
&IEDQZA(53) SETB  1                                                     56700020
         ORG   IEDOCQPT                                                 57400020
         DC    V(IEDQEU)                                                58100020
         ORG                                                            58800020
.ST2     ANOP                                                           59500020
&IEDQZA(54)  SETB  1                                                    60200020
IEDQSTCS CSECT .                                                SA63593 60500005
IEDQQCBC CSECT                                                          60900020
         ORG   &IEDQSV(2)+28                                            61600020
&V       SETC  'IED'.'&IEDQAR(4)'.'&X(&IEDQAR(7))'                      62300020
         DC    A(&V)                                                    63000020
         ORG                                                            63700020
&IEDQSV(4) CSECT                                                        64400020
         AIF   (N'&OPDATA EQ 0).CSE                                     65100020
.OPT     IEDQTO  &OPDATA                                                65800020
         AIF   (&IEDQAR(5) EQ 0).CSE                                    66500020
&IEDQAR(10) SETA 2                                                      67200020
         AGO   .EXT                                                     67900020
.NAL     DC    AL2(0) .                NO ALTERNATE DESTINATION         68600020
         AGO   .ST                                                      69300020
.PUT     ANOP                                                           70000020
&IEDQZA(38)  SETB  1                                                    71400020
&A       SETA  2                                                        72100020
         AIF   ('&SECTERM' EQ 'NO').C5                                  72800020
&B(8)    SETB  1                                                        73500020
         AIF   ('&SECTERM' EQ 'YES').C5                                 74200020
         MNOTE 12,'*** IHB002 INVALID SECTERM OPERAND SPECIFIED- &SECTE*74900020
               RM'                                                      75600020
&NO      SETB  1                                                        76300020
         AGO   .C5                                                      77000020
.ERR2    MNOTE 12,'*** IHB318 QUEUES OPERAND &QUEUES INVALID WITH MSUNI*77700020
               TS=0'                                                    78400020
&NO      SETB  1                                                        79100020
         AGO   .C5                                                      79800020
.ERR3    MNOTE 12,'*** IHB002 INVALID QUEUES OPERAND SPECIFIED- &QUEUES*80500020
               '                                                        81200020
&NO      SETB  1                                                        81900020
         AGO   .C5                                                      82600020
.CSE     DC    XL3'0'                                                   83300020
         AIF   ('&NAME' NE '&IEDQSV(5)').EXT                            84000020
         IEDNOL ,                                                S99528 84010022
&IEDQZA(55) SETB   0                                                    84700020
         AIF   (&IEDQAR(33) EQ 0 AND &IEDQAR(34) EQ 0).CSND             85400020
         ORG   IEDNADDR                                                 86100020
         DC    A(&IEDQAR(34)*4+1),A(&IEDQAR(33)*4+3) .           S21101 86800020
         ORG                                                            87500020
.CSND    AIF   (&IEDQAR(3) EQ 0).EXT                                    88200020
IEDQOPT  CSECT                                                          88900020
IEDQOPTN DC    AL2(&IEDQAR(15))                                         89600020
&IEDQSV(4) CSECT                                                        90300020
.EXT     SPACE 2                                                        91000020
         MEND                                                           91700020
