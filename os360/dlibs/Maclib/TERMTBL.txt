         MACRO                                                          01000013
&ANYNAME TERMTBL &ENTRY,&OPSZE,&OPCTL=,&CPINTV=,&CKPART=                02000013
         GBLA  &ACLOCX(21),&TSIZE,&QNAME                                03000013
         GBLC  &SECT                                                    04000013
         LCLA  &INT                                                     05000013
         LCLB  &NOGO                                                    06000013
         LCLC  &T                                                       07000013
.*3411140000                                                      18520 07500017
&QNAME   SETA  1                                                        08000013
         AIF   ('&ENTRY' NE '').OK                                      09000013
         MNOTE 12,'***  IHB001  LAST ENTRY OPERAND REQ''D-NOT SPECIFIEDX10000013
               '                                                        11000013
&NOGO    SETB  1                                                        12000013
.OK      ANOP                                                           13000013
&INT     SETA  0                                                        14000017
         AIF   ('&CPINTV' EQ '').TRYREQ                                 15000013
&T       SETC  '&CPINTV'                                                16000013
&INT     SETA  &T                                                       17000013
         AIF   (&INT LT 61 AND &INT GT 0).GOOD                          18000013
         MNOTE 12,'***  IHB002  INVALID INTERVAL OPERAND SPECIFIED &CPIX19000013
               NTV'                                                     20000013
&NOGO    SETB  1                                                        21000013
         AGO   .GOOD                                                    22000013
.TRYREQ  AIF   ('&CKPART' EQ '').GOOD                                   23000013
&T       SETC  '&CKPART'                                                24000013
&INT     SETA  &T                                                       25000013
&INT     SETA  &INT+128                                                 26000013
         AIF   (&INT LT 144 AND &INT GT 128).GOOD                       27000013
         MNOTE 12,'***  IHB002  INVALID CKPART OPERAND SPECIFIED &INT'  28000013
&NOGO    SETB  1                                                        29000013
.GOOD    ANOP                                                           30000013
&TSIZE   SETA  K'&ENTRY                                                 31000013
         AIF   ('&OPSZE' EQ '').OPT                                     32000013
&TSIZE   SETA  &OPSZE                                                   33000013
         AIF   (&TSIZE LT 9 AND K'&ENTRY LE &TSIZE).OPT                 34000013
         MNOTE 12,'***  IHB002  INVALID SIZE OPERAND SPECIFIED'         35000013
         MEXIT                                                          36000013
.OPT     AIF   (&NOGO).END                                              37000013
         ENTRY TERMTBL                                                  38000013
         DS    0F                                                       39000013
         DC    AL1(&INT)          CHECK POINT INTERVAL OR REQUEST VALUE 40000013
         AIF   ('&OPCTL' EQ '').NOOP                                    41000013
         DC    AL3(&OPCTL)        ADDRESS OF OPCTL MACRO                42000013
         AGO   .SKIP                                                    43000013
.NOOP    DC    AL3(0)             FILLER                                44000013
.SKIP    ANOP                                                           45000013
&ACLOCX(1) SETA &TSIZE+10                                               46000013
TERMTBL  DC    A(&ENTRY)               LAST TERM TABLE ENTRY            47000013
         DC    AL1(&TSIZE)             LARGEST ENTRY SIZE               48000013
         DC    AL3(0)                                                   49000013
&SECT    SETC  '&SYSECT'                                                50000013
IECKTERM DSECT                                                          51000013
         DS    XL10                                                     52000013
         DS    CL&TSIZE                                                 53000013
.END     MEND                                                           54000013
