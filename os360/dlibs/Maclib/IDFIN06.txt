         MACRO                                                          00200000
         IDFIN06                                                        00400000
         COPY IDFGBL                                                    00600000
         AIF   (NOT &PIB(48)).TR                                        00800000
         MNOTE *,'IDF100 IN TRACE MODE ENTERING IDFIN06'                01000000
.TR      ANOP                                                           01200000
         AIF   (&PIB(11)).FLD6                                          01400000
         AIF   (&PIB(2)).FLD1                                           01600000
         AIF   (&PIB(12) OR &B(20)).FLD6                          FSJW  01800000
.********************************************************************** 02000000
.*                                                                    * 02200000
.*                           DATA TYPE BYTE GENERATOR                 * 02400000
.*                                                                    * 02600000
.********************************************************************** 02800000
&K       SETA  55                                                       03000000
&DSCB(&K) SETB (&FB(1))                                                 03200000
&K       SETA  &K+5                                                     03400000
&DSCB(&K) SETB (&FB(2))                                                 03600000
&K       SETA  &K+5                                                     03800000
&DSCB(&K) SETB (&FB(3))                                                 04000000
&M       SETA  17                                                       04200000
&B(1)    SETB  (NOT (&B(10) OR &B(13)) AND N'&SYSLIST(&M) NE 0)         04400000
&FB(6)   SETB  (&FB(8) OR &FB(9) OR &FB(10) OR &FB(11) OR &FB(12)      X04600000
               OR &B(1) OR &FB(14))                                     04800000
&M       SETA  18                                                       05000000
&B(2)    SETB  (NOT &B(13) AND N'&SYSLIST(&M) NE 0)                     05200000
&M       SETA  19                                                       05400000
&B(3)    SETB  (NOT (&B(10) OR &B(13)) AND N'&SYSLIST(&M) NE 0)         05600000
&M       SETA  20                                                       05800000
&B(4)    SETB  ((&FB(4) OR &FB(5) OR &FB(17)) AND N'&SYSLIST(&M) NE 0)  06000000
&FB(7)   SETB  (&FB(15) OR &FB(17) OR &B(1) OR &B(2) OR                X06200000
               &B(3) OR &B(4))                                          06400000
&FA(1)   SETA  &PIA(6)                                                  06600000
&A(1)    SETA  64*&FB(1)+32*&FB(2)+16*&FB(3)+8*&FB(4)+4*&FB(5)+2*&FB(6)X06800000
               +&FB(7)                                                  07000000
         IDFASM A                                                       07200000
.*********************************************************************  07400000
.*                                                                    * 07600000
.*                           CHARACTER COUNT GENERATOR                * 07800000
.*                                                                    * 08000000
.********************************************************************** 08200000
&A(1)    SETA  &CTA(6)                                                  08400000
         IDFASM A                                                       08600000
         AIF   (NOT &B(13)).FLD2                                        08800000
.********************************************************************** 09000000
.*                                                                    * 09200000
.*                           EMITTED DATA GROUP GENERATOR             * 09400000
.*                                                                    * 09600000
.********************************************************************** 09800000
.FLD1    ANOP                                                           10000000
&M       SETA  1                                                        10200000
         AIF   (&PIB(2) AND &PIA(10) NE &M).FLD5                        10400000
&PIB(1)  SETB  (T'&SYSLIST(&M,1) EQ 'O')                                10600000
         AIF   (NOT &PIB(1)).I0216                                      10800000
         IDFMSG1 102                                                    11000000
.I0216   ANOP ,                                                         11200000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))                                    11400000
&PIB(2)  SETB  0                                                        11600000
         AIF   (&PIB(1)).FLD2                                           11800000
&PIB(1)  SETB  ('&SYSLIST(&M,1)'(1,1) NE '''')                          12000000
&N       SETA  0                                                        12200000
         AIF   (NOT &PIB(1)).I0217                                      12400000
         IDFMSG2 200                                                    12600000
.I0217   ANOP ,                                                         12800000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))                                    13000000
         AIF   (&PIB(1)).FLD2                                           13200000
&TB(3)   SETB  0                                                        13400000
&TB(4)   SETB  0                                                        13600000
&TB(5)   SETB  0                                                        13800000
&I       SETA  2                                                        14000000
         AIF   (&PIB(23)).FLD1A                                         14200000
         IDFTR &SYSLIST(&M,1)                                           14400000
         AGO   .FLD1B                                                   14600000
.FLD1A   IDFTRK &SYSLIST(&M,1)                                          14800000
.FLD1B   ANOP                                                           15000000
&PIB(1)  SETB  (&TB(4))                                                 15200000
         AIF   (NOT &PIB(1)).I0218                                      15400000
&N       SETA  1                                                        15600000
         IDFMSG1 103                                                    15800000
.I0218   ANOP ,                                                         16000000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))                                    16200000
         AIF (&PIB(1)).FLD2                                             16400000
&PIB(1)  SETB  (&TB(6))                                                 16600000
         AIF   (NOT &PIB(1)).I0219                                      16800000
         IDFMSG3 501                                                    17000000
.I0219   ANOP ,                                                         17200000
&PIB(1)  SETB  (&TB(7))                                                 17400000
         AIF   (NOT &PIB(1)).I0220                                      17600000
         IDFMSG3 502                                                    17800000
.I0220   ANOP ,                                                         18000000
&PIB(1)  SETB  (&TB(8))                                                 18200000
         AIF   (NOT &PIB(1)).I0221                                      18400000
         IDFMSG3 503                                                    18600000
.I0221   ANOP ,                                                         18800000
&PIA(10) SETA  &M                                                       19000000
&PIB(2)  SETB  (N'&SYSLIST(&M) EQ 2 AND '&SYSLIST(&M,2)' EQ 'C')        19200000
         AIF   (&PIB(2)).FLD5                                           19400000
&N       SETA 1                                                         19600000
&PIB(1)  SETB  (N'&SYSLIST(&M) GT &N)                                   19800000
         AIF   (NOT &PIB(1)).I0222                                      20000000
         IDFMSG1 104                                                    20200000
.I0222   ANOP ,                                                         20400000
&PIB(1)  SETB  (&I LT K'&SYSLIST(&M,1))                           FSJW  20600000
         AIF   (NOT &PIB(1)).I0222A                               FSJW  20800000
         IDFMSG1 105                 EXCESS CHARS                 FSJW  21000000
.I0222A  ANOP  ,                                                  FSJW  21200000
&A(1)    SETA  &FA(1)+4+(&FA(1)-&FA(1)/486*486)/476*18                  21400000
&A(1)    SETA  &PIA(6)-&A(1)-(&PIA(6)/486-&A(1)/486)*18                 21600000
&A(1)    SETA  &A(1)/2                                                  21800000
&A(5)    SETA  1                                                        22000000
&A(6)    SETA  127                                                      22200000
&PIB(1)  SETB  (&A(1) LT &A(5) OR &A(1) GT &A(6))                       22400000
         AIF   (NOT &PIB(1)).I0223                                      22600000
         IDFMSG3 536                                                    22800000
.I0223   ANOP ,                                                         23000000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))                                    23200000
         AIF (&PIB(1)).FLD2                                             23400000
         AIF   (&A(1) EQ &CTA(6)).FLD2                                  23600000
&PIA(6)  SETA  &FA(1)+2+(&FA(1)-&FA(1)/486*486)/478*18                  23800000
         IDFASM A                                                       24000000
&CTA(6)  SETA  &A(1)                                                    24200000
.FLD2    ANOP                                                           24400000
.********************************************************************** 24600000
.*                                                                    * 24800000
.*                           VALIDITY BYTE GENERATOR                  * 25000000
.*                                                                    * 25200000
.********************************************************************** 25400000
&PIA(6)  SETA  &PIA(3)+&PIA(4)+&PIA(4)/480*18                           25600000
&FA(2)   SETA  &PIA(6)                                                  25800000
         AIF   (NOT &FB(6)).FLD3                                        26000000
&M       SETA  17                                                       26200000
&FB(13)  SETB  (N'&SYSLIST(&M) NE 0)                                    26400000
&A(1)    SETA  64*&FB(8)+32*&FB(9)+16*&FB(10)+8*&FB(11)+4*&FB(12)+2*&FBX26600000
               (13)+&FB(14)                                             26800000
         IDFASM A                                                       27000000
.FLD3    AIF   (NOT &FB(7)).FLD4                                        27200000
.********************************************************************** 27400000
.*                                                                    * 27600000
.*                           FUNCTION BYTE GENERATOR                  * 27800000
.*                                                                    * 28000000
.********************************************************************** 28200000
&FB(16)  SETB  0                        RESET COMPARE BIT               28400000
&A(1)    SETA  64*&FB(15)+16*&FB(17)                                    28600000
         IDFASM A                                                       28800000
.FLD4    AIF   (NOT &FB(12)).FLD5                                       29000000
&A(1)    SETA  &CTA(7)                                                  29200000
         IDFASM A                                                       29400000
.FLD5    ANOP                                                           29600000
.********************************************************************** 29800000
.*                                                                    * 30000000
.*                     COMPARE OPERAND PROCESSOR                      * 30200000
.*                                                                    * 30400000
.********************************************************************** 30600000
&M       SETA  17                       POINT TO COMPARE IN CALL        30800000
&PIB(1)  SETB  (&PIB(2) AND &PIA(10) NE &M AND N'&SYSLIST(&M) GT 0)     31000000
         AIF   (NOT &PIB(1)).I0224                                      31200000
         IDFMSG1 100                    CONTINUATION, CONTINUE IGNORED  31400000
.I0224   AIF   (&PIB(2) AND &PIA(10) NE &M).FLD6                        31600000
&PIB(1)  SETB  (&PIB(2) AND N'&SYSLIST(&M) EQ 0)    COMPARE IS CONT'D   31800000
         AIF   (NOT &PIB(1)).I0225                                      32000000
         IDFMSG1 102     EXPECTED CONTINUATION NOT FOUND                32200000
.I0225   ANOP ,                                                         32400000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))    ACCUMULATE ERROR                32600000
&PIB(2)  SETB  (&PIB(2) AND N'&SYSLIST(&M) GT 0)    SWITCH OFF IF NOT   32800000
         AIF   (N'&SYSLIST(&M) EQ 0).CMPX                               33000000
         AIF   (NOT &B(20)).I0225A                                FSJW  33200000
&PIB(1)  SETB  1                                                  FSJW  33400000
         IDFMSG1 101                                              FSJW  33600000
         AGO   .FLD6                                              FSJW  33800000
.I0225A  ANOP  ,                                                  FSJW  34000000
&PIB(1)  SETB  (&B(10) OR &B(13))       IGNORE IF SOURCE FID, 'STRING'  34200000
         AIF   (NOT &PIB(1)).I0226                                      34400000
         IDFMSG3 537                                                    34600000
.I0226   ANOP ,                                                         34800000
         AIF  (&PIB(1)).CMPX            EXIT TO IGNORE                  35000000
&N       SETA  1                        POINT TO FIRST SUBOP            35200000
         AIF   (&PIB(2)).CMP1                                           35400000
&FA(3)   SETA  &PIA(3)+&PIA(4)+&PIA(4)/480*18                           35600000
&PIA(10) SETA  &M                                                       35800000
&TB(3)   SETB  (&DSCB(55))                                              36000000
&TB(5)   SETB  (&DSCB(65))                                              36200000
&FB(26)  SETB  0                        THIS BIT CURRENTLY UNUSED       36400000
&A(5)    SETA  1                        LOWER LIMIT                     36600000
&A(6)    SETA  127                      UPPER LIMIT                     36800000
.CMP1    ANOP  , HEAD OF PROCESSING LOOP                                37000000
&B(8)    SETB  ('&SYSLIST(&M,&N)' EQ 'FIELD')  TEST FOR NOISE WORD      37200000
&N       SETA  &N+&B(8)                 PASS NOISE WORD SUBOP IF ANY    37400000
&C(1)    SETC  '&SYSLIST(&M,&N)'        EXTRACT COMPAROPR SUBOPERAND    37600000
&FB(22)  SETB  ('&C(1)' EQ 'LT' OR '&C(1)' EQ '''<''' OR '&C(1)' EQ    *37800000
               'NL' OR '&C(1)' EQ '''�<''')                             38000000
&FB(22)  SETB  (&FB(22) OR '&C(1)' EQ 'GE' OR '&C(1)' EQ '''>=''')      38200000
&FB(23)  SETB  ('&C(1)' EQ 'EQ' OR '&C(1)' EQ '''=''' OR '&C(1)' EQ    *38400000
               'NE' OR '&C(1)' EQ '''�=''')                             38600000
&FB(24)  SETB  ('&C(1)' EQ 'GT' OR '&C(1)' EQ '''>''' OR '&C(1)' EQ    *38800000
               'NG' OR '&C(1)' EQ '''�>''')                             39000000
&FB(24)  SETB  (&FB(24) OR '&C(1)' EQ 'LE' OR '&C(1)' EQ '''<=''')      39200000
&PIB(1)  SETB  (NOT (&FB(22) OR &FB(23) OR &FB(24)))  ONLY ONE ON       39400000
         AIF   (NOT &PIB(1)).I0228                                      39600000
         IDFMSG2 200                    COMPAROPR SUBOPERAND INVALID    39800000
.I0228   ANOP ,                                                         40000000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))    ACCUMULATE ERROR                40200000
         AIF   (&PIB(1)).CMPX           EXIT ON ERROR                   40400000
&FB(25)  SETB  ('&C(1)' EQ 'NG' OR '&C(1)' EQ '''�>''' OR '&C(1)' EQ   *40600000
               'NE' OR '&C(1)' EQ '''�=''')                             40800000
&FB(25)  SETB  (&FB(25) OR '&C(1)' EQ 'NL' OR '&C(1)' EQ '''�<''' OR   *41000000
               '&C(1)' EQ 'GE' OR '&C(1)' EQ '''>=''')                  41200000
&FB(25)  SETB  (&FB(25) OR '&C(1)' EQ 'LE' OR '&C(1)' EQ '''<=''')      41400000
&N       SETA  &N+2                                                     41600000
&C(1)    SETC  '&SYSLIST(&M,&N)'        LOGICOPR LOOKAHEAD              41800000
&FB(27)  SETB  ('&C(1)' EQ 'AND' OR '&C(1)' EQ '''&&''')                42000000
&FB(28)  SETB  ('&C(1)' EQ 'OR' OR '&C(1)' EQ '''�''')                  42200000
&PIA(6)  SETA  &PIA(3)+&PIA(4)+&PIA(4)/480*18  INSURE MAXIMUM ADDRESS   42400000
&FA(4)   SETA  &PIA(6)                  SET CURRENT ADDRESS POINTER     42600000
&A(1)    SETA  64*&FB(22)+32*&FB(23)+16*&FB(24)+8*&FB(25)+0*&FB(26)+2*&-42800000
               FB(27)+&FB(28)           COMPUTE VALUE EQUIV OF BITS     43000000
         IDFASM A                       ASSEMBLE VALUE CHECK BYTE       43200000
&N       SETA  &N-1                     BACK TO COMPARAND               43400000
&PIB(1)  SETB  (T'&SYSLIST(&M,&N) EQ 'O')  COMPARAND SUBOP OMITTED      43600000
         AIF   (NOT &PIB(1)).I0229                                      43800000
         IDFMSG2 202                    COMPARAND SUBOP NOT CODED       44000000
.I0229   ANOP ,                                                         44200000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))    ACCUMULATE ERROR                44400000
         AIF   (&PIB(1)).CMPX           EXIT ON ERROR                   44600000
         AIF   (&B(11) OR &B(12) OR NOT &FB(1) AND NOT &FB(2) AND      *44800000
               &FB(3) OR &FB(15)).CMP3      6/15/2 APAR 55120     FSJW  45000000
.*             SOURCE IS RSN OR CTR, KIND IS N, SINK IS CTR, OR CTR=    45200000
         AIF   ('&SYSLIST(&M,&N)'(1,1) NE '''' AND &FA(3) EQ           *45400000
               &FA(4)).CMP3                                             45600000
.* FALL THROUGH HERE TO PROCESS PRESUMPTIVE CHARACTER COMPARAND         45800000
&PIB(1)  SETB  ('&SYSLIST(&M,&N)'(1,1) NE '''')  TEST FOR LEADING APOST 46000000
         AIF   (NOT &PIB(1)).I0230                                      46200000
         IDFMSG2 200                                                    46400000
.I0230   ANOP ,                                                         46600000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))                                    46800000
         AIF   (&PIB(1)).CMPX                                           47000000
&A(2)    SETA  K'&SYSLIST(&M,&N)-2      COMPUTE PRESUMED COUNT          47200000
&A(2)    SETA  &A(2)-&A(2)/128*128                                      47400000
&A(1)    SETA  &A(2)                                                    47600000
         IDFASM A                       ASSEMBLE PRESUMED COUNT         47800000
&I       SETA  2                                                        48000000
&TB(4)   SETB  (&DSCB(60))                                              48200000
         AIF   (&PIB(23)).CMP1A                                         48400000
         IDFTR &SYSLIST(&M,&N)                                          48600000
         AGO   .CMP1B                                                   48800000
.CMP1A   IDFTRK &SYSLIST(&M,&N)                                         49000000
.CMP1B   ANOP                                                           49200000
&PIB(1)  SETB  (&TB(4) NE &DSCB(60)) TEST FOR CHAR ERROR                49400000
         AIF   (NOT &PIB(1)).I0231                                      49600000
         IDFMSG1 103                    IDENTIFIES WHERE                49800000
.I0231   ANOP ,                                                         50000000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))    ACCUMULATE ERROR                50200000
         AIF   (&PIB(1)).CMPX           EXIT ON ERROR                   50400000
         AIF   (NOT (&TB(6) OR &TB(7) OR &TB(8))).CMP2                  50600000
&PIB(1)  SETB  (&TB(6)) NON 3735 CHAR                                   50800000
         AIF   (NOT &PIB(1)).I0232                                      51000000
         IDFMSG3 501                                                    51200000
.I0232   ANOP ,                                                         51400000
&PIB(1)  SETB  (&TB(7)) NON-ASCII                                       51600000
         AIF   (NOT &PIB(1)).I0234                                      51800000
         IDFMSG3 502                                                    52000000
.I0234   ANOP ,                                                         52200000
&PIB(1)  SETB  (&TB(8)) NON-EBCDIC                                      52400000
         AIF   (NOT &PIB(1)).I0236                                      52600000
         IDFMSG3 503                                                    52800000
.I0236   ANOP ,                                                         53000000
.CMP2    ANOP  , HERE TO BYPASS WARNING MESSAGES                        53200000
&A(1)    SETA  &FA(4)+4+(&FA(4)-&FA(4)/486*486)/476*18                  53400000
&A(1)    SETA  &PIA(6)-&A(1)-(&PIA(6)/486-&A(1)/486)*18                 53600000
&A(1)    SETA  &A(1)/2                                                  53800000
&PIB(1)  SETB  (&A(1) LT &A(5) OR &A(1) GT &A(6))                       54000000
         AIF   (NOT &PIB(1)).I0238                                      54200000
         IDFMSG3 538                                                    54400000
.I0238   ANOP ,                                                         54600000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))    ACCUMULATE ERROR                54800000
         AIF   (&PIB(1)).CMPX           EXIT ON ERROR                   55000000
         AIF   (&A(1) EQ &A(2)).CMP4                                    55200000
&PIA(6)  SETA  &FA(4)+2+(&FA(4)-&FA(4)/486*486)/478*18                  55400000
               IDFASM A                                                 55600000
         AGO   .CMP4                                                    55800000
.CMP3    ANOP , HERE TO PROCESS NUMBERIC CMPNDS                         56000000
&B(1)    SETB  ('&SYSLIST(&M,&N)'(1,1) EQ '+')  TEST FOR LEADING PLUS   56200000
&B(2)    SETB  ('&SYSLIST(&M,&N)'(1,1) EQ '-')  TEST FOR LEADING MINUS  56400000
&A(1)    SETA  K'&SYSLIST(&M,&N)-&B(1)-&B(2)                            56600000
&PIB(1)  SETB  (&A(1) LT &A(5) OR &A(1) GT &A(6))                       56800000
         AIF   (NOT &PIB(1)).I0240                                      57000000
         IDFMSG3 538                                                    57200000
.I0240   ANOP ,                                                         57400000
&PIB(15) SETB (&PIB(15) OR &PIB(1))                                     57600000
         AIF   (&PIB(1)).CMPX                                           57800000
&A(1)    SETA  K'&SYSLIST(&M,&N)-&B(1)                                  58000000
         IDFASM A                                                       58200000
&I       SETA  1+&B(1)+&B(2)                                            58400000
         AIF   (NOT &B(2)).CMP3A                                        58600000
         IDFASM (,427D)                 ASSEMBLE MINUS DIRECTLY         58800000
.CMP3A   ANOP , HEAD OF DIGIT ASSEMBLY LOOP                             59000000
&C(2)    SETC  '&SYSLIST(&M,&N)'(&I,1) GET NEXT CHARACTER               59200000
&PIB(1)  SETB  ('&C(2)' LT '0' OR '&C(2)' GT '9')                       59400000
         AIF   (NOT &PIB(1)).I0242                                      59600000
         IDFMSG1 103                    INVALID CHARACTER               59800000
.I0242   ANOP ,                                                         60000000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))    ACCUMULATE ERROR                60200000
         AIF   (&PIB(1)).CMPX           EXIT ON ERROR                   60400000
&A(1)    SETA  48+&C(2)                                                 60600000
         IDFASM A                                                       60800000
&I       SETA  &I+1                     ADVANCE POINTER                 61000000
         AIF   (&I LE K'&SYSLIST(&M,&N)).CMP3A                          61200000
         AIF   (&FA(4) GT &FA(3)).CMP4                                  61400000
         AIF   (NOT &FB(1) AND NOT &FB(2) AND &FB(3)).CMP4              61600000
&FB(1)   SETB  0                                                        61800000
&FB(2)   SETB  0                                                        62000000
&FB(3)   SETB  1                                                        62200000
&PIB(1)  SETB  1                                                        62400000
         AIF   (NOT &PIB(1)).I0243                                      62600000
         IDFMSG3 539                                                    62800000
.I0243   ANOP ,                                                         63000000
&PIA(6)  SETA  &FA(1)                                                   63200000
&A(1)    SETA  64*&FB(1)+32*&FB(2)+16*&FB(3)+8*&FB(4)+4*&FB(5)+2*&FB(6)C63400000
               +&FB(7)                                                  63600000
         IDFASM A                       REASSEMBLE DATA TYPE BYTE       63800000
.CMP4    ANOP                                                           64000000
&N       SETA  &N+1                     ADVANCE TO NEXT SUBOP           64200000
&PIB(1)  SETB  (NOT (&FB(27) OR &FB(28) OR &N GT N'&SYSLIST(&M)))       64400000
         AIF   (NOT &PIB(1)).I0244                                      64600000
         IDFMSG2 200                                                    64800000
.I0244   ANOP ,                                                         65000000
&PIB(15) SETB  (&PIB(15) OR &PIB(1))                                    65200000
         AIF   (&PIB(1)).CMPX                                           65400000
&N       SETA  &N+1                                                     65600000
&PIB(2)  SETB  (&N EQ N'&SYSLIST(&M) AND '&SYSLIST(&M,&N)' EQ 'C')      65800000
         AIF   ((&FB(27) OR &FB(28)) AND NOT &PIB(2)).CMP1              66000000
         AIF   (&PIB(2)).FLD6                                           66200000
.CMPX    AIF   (NOT &FB(14)).FLD6                                       66400000
.********************************************************************** 66600000
.*                                                                    * 66800000
.*                           SELFCHK BYTE GENERATOR                   * 67000000
.*                                                                    * 67200000
.********************************************************************** 67400000
&A(1)    SETA  64*&DSCB(40)+32*&DSCB(45)+16*&DSCB(50)                   67600000
         IDFASM A                                                       67800000
.FLD6    AIF   (NOT &PIB(48)).TX                                        68000000
         MNOTE *,'IDF100 IN TRACE MODE LEAVING IDFIN06'                 68200000
.TX      ANOP                                                           68400000
         MEND                                                           68600000
