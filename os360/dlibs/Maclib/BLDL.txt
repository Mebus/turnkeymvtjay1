         MACRO                                                          00020000
&NAME    BLDL  &DCB,&LIST,&DUMMY=                                       00040017
.* SEE CODE FLAGGED SA69847 FOR APARS XA04159 AND YA03669.              00050021
         AIF   ('&DCB' EQ '').E1                                        00060000
         AIF   ('&LIST' EQ '').E2                                       00080000
&NAME    IHBINNRA &DCB,&LIST                LOAD REG 1 AND REG 0        00100000
         LA    1,0(1)                                                   00110021
.* REGISTER 1 IS MADE POSITIVE SO THE SVC CODE WILL KNOW THIS   SA69847 00110421
.* REQUEST IS FROM BLDL AND NOT THE FIND MACRO.                 SA69847 00110821
         SVC   18                                LINK TO BLDL ROUTINE   00120000
         MEXIT                                                          00140000
.E1      IHBERMAC 6                     DCB ADDR MISSING                00150019
         AIF   ('&LIST' EQ '').E2                                       00160019
         MEXIT                                                          00180000
.E2      IHBERMAC 34                    LIST ADDR MISSING               05180019
         MEND                                                           40200017
