         MACRO                                                          00600021
&NAME    GTRACE &MF=,&ID=,&DATA=,&LNG=,&FID=00                          01200021
         GBLB  &IHLMGT                                                  01800021
         LCLC  &R                                                       02400021
.*A000000-999999                                                 S21014 02450021
.* TEST TO SEE IF THE MAPPING IS ALREADY EXPANDED                       03000021
         AIF   (&IHLMGT).XPNDED                                         03600021
         IHLMGTRC                                                       04200021
.XPNDED  ANOP                                                           05400021
         CNOP  0,4                                                      06000021
         AIF     ('&NAME' EQ '').GO                                     06600021
&NAME    EQU   *                                                        07200021
.*                                                                      07800021
.* IS THIS THE STANDARD FORM                                            08400021
.*                                                                      09000021
.GO      AIF   ('&MF' EQ '').STAND                                      09600021
.*                                                                      10200021
.* IF NOT THEN IS IT THE LIST FORM                                      10800021
.*                                                                      11400021
         AIF   ('&MF' EQ 'L').LIST                                      12000021
.*                                                                      12600021
.* IF NOT THEN IS IT THE EXECUTE FORM                                   13200021
.*                                                                      13800021
         AIF   ('&MF(1)' EQ 'E').EXEC                                   14400021
.*                                                                      15000021
.* IF NOT THEN STOP EVERYTHING                                          15600021
.*                                                                      16200021
         MNOTE 8,'IHL040  NOT A LEGAL FORM OF THE MACRO. CHECK THE MF=' 16800021
.******* NO CODE GENERATED SO THAT PGM MAY EXECUTE.                     17400021
         MEXIT                                                          18000021
.*                                                                      18600021
.*                                                                      19200021
.* THE STANDARD FORM                                                    19800021
.*                                                                      20400021
.STAND   ANOP                                                           21000021
.*                                                                      21600021
.* FIRST SET UP THE PARMLIST & HAVE REG 1 POINT TO IT                   22200021
.*                                                                      22800021
         BAL   1,*+12      PUT ADD OF PARMLIST IN REG                   23400021
         AIF   ('&LNG' NE '').SLNN                                      24000021
         MNOTE 12,'IHL041  LNG= KEYWORD MISSING.'                       24600021
         MEXIT                                                          25200021
.SLNN    AIF   ('&LNG'(1,1) EQ '(').SREGL                               25800021
         DC    AL2(&LNG)   LENGTH                                       26400021
         AGO   .SNUML                                                   27000021
.SREGL   ANOP                                                           27600021
         DC    AL2(0)     LENGTH                                        28200021
.SNUML   AIF   ('&FID'(1,1) EQ '(').SREGF                               28800021
         DC    AL2(&FID)   FID CODE                                     29400021
         AGO   .SNUMF                                                   30000021
.SREGF   ANOP                                                           30600021
         DC    AL2(0)  FID                                              31200021
.SNUMF   AIF   ('&DATA' NE '').DATAOK                                   31800021
         MNOTE 12,'IHL044  DATA= KEYWORD MISSING.'                      32400021
         MEXIT                                                          33000021
.DATAOK  ANOP                                                           33600021
         DC    AL4(0)   DATA AREA ADDRESS                               34200021
.*                                                                      34800021
.* STORE INTO PARMLIST ANY PARAMETERS IN REGS                           35400021
.*                                                                      36000021
         AIF   ('&LNG'(1,1) NE '(').SLNR                                36600021
&R       SETC  '&LNG'(2,K'&LNG-2)                                       37200021
         STH   &R,0(1)   PUT THE LENGTH IN PARMLIST                     37800021
.SLNR    AIF   ('&FID'(1,1) NE '(').SFNR                                38400021
&R       SETC  '&FID'(2,K'&FID-2)                                       39000021
         STH   &R,2(1)   PUT THE FID IN PARMLIST                        39600021
.*                                                                      40200021
.* GO TO THE EXECUTE FORM FOR THE DATA & ID OPERANDS                    40800021
.*                                                                      41400021
.SFNR    AGO   .DATASET                                                 42000021
.*                                                                      42600021
.*                                                                      43200021
.* THE LIST FORM                                                        43800021
.*                                                                      44400021
.LIST    ANOP                                                           45000021
.FIDOKS  AIF   ('&LNG' EQ '').LLNGN                                     45600021
         DC    AL2(&LNG)   LENGTH                                       46200021
         AGO   .SKIP3                                                   46800021
.LLNGN   ANOP                                                           47400021
         DC    AL2(0)  LENGTH                                           48000021
.SKIP3   ANOP                                                           48600021
         DC    AL2(&FID)   FID CODE                                     49200021
         AIF   ('&DATA' EQ '').LDATAN                                   49800021
         DC    AL4(&DATA)   DATA ADDRESS                                50400021
         MEXIT                                                          51000021
.LDATAN  ANOP                                                           51600021
         DC    AL4(0)  DATA ADDRESS                                     52200021
         MEXIT                                                          52800021
.*                                                                      53400021
.*                                                                      54000021
.* THE EXECUTE FORM                                                     54600021
.*                                                                      55200021
.* DETERMINE HOW THE PARAMETER LIST IN THE MF= OPERAND IS INDICATED     55800021
.*                                                                      56400021
.EXEC    AIF   ('&MF(2)' EQ '(1)').EREGM                                57000021
         AIF   ('&MF(2)' NE '').EMFOK                                   57600021
         MNOTE 12,'IHL045  MF=(E, PARAMETER SPECIFICATION MISSING.'     58200021
         MEXIT                                                          58800021
.EMFOK   ANOP                                                           59400021
         AIF   ('&MF(2)'(1,1) EQ '(').EMFR                              59450021
         LA    1,&MF(2)   PUT PARMLIST ADDRESS IN REG(1)                60000021
         AGO   .EREGM                                                   60050021
.EMFR    ANOP                                                           60100021
         LA    1,0&MF(2)                                                60150021
.*                                                                      60600021
.* IS THE LNG SPECIFIED                                                 61200021
.*                                                                      61800021
.EREGM   AIF   ('&LNG' EQ '').ENULL                                     62400021
         AIF   ('&LNG'(1,1) EQ '(').EREGL                               63000021
         LA    15,&LNG                                                  63600021
         STH   15,0(1)   STORE THE LENGTH IN THE PARMLIST               64200021
         AGO   .ENULL                                                   64800021
.EREGL   ANOP                                                           65400021
&R       SETC  '&LNG'(2,K'&LNG-2)                                       66000021
         STH   &R,0(1)   STORE LNG INTO PARMLIST                        66600021
.*                                                                      67200021
.* HOW IS THE FID SPECIFIED                                             67800021
.*                                                                      68400021
.ENULL   AIF   ('&FID'(1,1) EQ '(').EREGF                               69000021
         AIF   ('&FID' EQ '00').SKIP6                                   69600021
         LA    15,&FID                                                  70200021
         STH   15,2(1)   STORE THE FID INTO THE PARMLIST                70800021
         AGO   .SKIP6                                                   71400021
.EREGF   ANOP                                                           72000021
&R       SETC  '&FID'(2,K'&FID-2)                                       72600021
         STH   &R,2(1)   STORE FID INTO PARMLIST                        73200021
.*                                                                      73800021
.* IS THE DATA ADDRESS SPECIFIED                                        74400021
.*                                                                      75000021
.SKIP6   AIF   ('&DATA' EQ '').ENULD                                    75600021
.DATASET AIF   ('&DATA'(1,1) EQ '(').EREGD                              76200021
         LA    15,&DATA                                                 76800021
         ST    15,4(1)   STORE DATA ADDRESS INTO PARMLIST               77400021
         AGO   .ENULD                                                   78000021
.EREGD   ANOP                                                           78600021
&R       SETC  '&DATA'(2,K'&DATA-2)                                     79200021
         ST    &R,4(1)   STORE DATA ADDRESS INTO PARMLIST               79800021
.ENULD   ANOP                                                           80400021
         LA    15,4   PUT GTF NOT ACTIVE CODE IN REG(15)                81000021
.*                                                                      81600021
.*  IS THE ID SPECIFIED                                                 82200021
.*                                                                      82800021
         AIF   ('&ID' EQ '').NOEID                                      83400021
.*                                                                      84000021
*                        SET UP 8 BYTE HOOK                             84600021
.*                                                                      85200021
         DC    XL2'AF0E',S(&ID.(0)) MC INSTRUCTION                      85850021
         DC    XL2'470E',S(&ID.(0)) NOP FOR IDENT                       85900021
         MEXIT                                                          86400021
.NOEID   MNOTE 12,'IHL048  ID= KEYWORD MISSING'                         87000021
         MEND                                                           87600021
