         MACRO                                                          00020000
&NAME  GMLW  &SIZ             MAKE LINE WIDTH                           00040000
         GBLA  &IHBBLC,&IHBBLIM                                         00060000
         IHBGAM3                                                        00080000
&IHBBLC    SETA  &IHBBLC+2              UPDATE BUFFER-LOC COUNTER       00100000
         AIF   (&IHBBLIM GE &IHBBLC).OK                                 00120000
         IHBERMAC 182                                                   00140000
.OK      AIF   (T'&SIZ EQ 'O').NOTE     DETERMINE LINE WIDTH            00160000
         AIF   ('&SIZ'(1,1) EQ 'B').BAS                                 00180000
         AIF   ('&SIZ'(1,1) EQ 'W').WIDE                                00200000
.NOTE    IHBERMAC  180,WIDTH,BASIC                                      00220000
.BAS   ANOP                                                             00240000
&NAME  DC      H'10936'                                                 00260000
         MEXIT                                                          00280000
.WIDE    ANOP                                                           00300000
&NAME  DC      H'10937'                                                 00320000
         MEND                                                           00340000
