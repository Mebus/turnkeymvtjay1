         MACRO                                                          00020000
&NAME    GBFLM &BLC=,&BLIM=,&BLP=                                       00040000
         GBLA  &IHBBLC,&IHBBLIM,&IHBBLP                                 00060000
         GBLC  &IHBINIT                                                 00080000
         AIF   (T'&BLC EQ 'O' AND T'&BLIM EQ 'O' AND T'&BLP EQ 'O').GO  00100000
         AIF   (T'&BLP EQ 'O').TAG0                                     00120000
         AIF   (T'&BLP NE 'N').ERBLP    VALIDATE BUFFER LOAD POINT      00140000
         AIF   (&BLP GT 32767).ERBLP                                    00160000
&IHBBLP    SETA  &BLP                  INITIALIZE BUFFER LOAD POINT     00180000
.TAG0    AIF   (T'&BLC EQ 'O').TAG1                                     00200000
         AIF   (T'&BLC NE 'N').ERBLC    VALIDATE BUFFER LOC COUNTER     00220000
         AIF   (&BLC GT 32767).ERBLC                                    00240000
&IHBBLC    SETA  &BLC                  INITIALIZE BUFFER LOC COUNTER    00260000
.TAG1    AIF   (T'&BLIM EQ 'O').NOBLIM                                  00280000
         AIF   (T'&BLIM NE 'N').ERBLIM  VALIDATE BUFFER LIMIT           00300000
         AIF   (&BLIM GT 32767).ERBLIM                                  00320000
         AIF   (T'&BLC EQ 'O').TAG2                                     00340000
         AIF   (&BLIM LE &BLC).ER2                                      00360000
.TAG2    ANOP                                                           00380000
&IHBBLIM   SETA  &BLIM                 INITIALIZE BUFFER LIMIT          00400000
         AGO   .NOBLIM                                                  00420000
.ERBLC   IHBERMAC 180,BLC,0                                             00440000
&IHBBLC    SETA  0                                                      00460000
         AGO   .TAG1                                                    00480000
.ER2     IHBERMAC 180,BLIM,32767                                        00500000
         AGO   .ROND                                                    00520000
.ERBLIM  IHBERMAC 180,BLIM,32767                                        00540000
         AGO   .ROND                                                    00560000
.GO      ANOP                                                           00580000
&IHBBLP    SETA  0                     ALL OPERANDS OMITTED             00600000
&IHBBLC    SETA  0                     SET BUFFER-LOC COUNTER,LIMIT, &  00620000
&IHBBLIM   SETA  32767                    LOAD POINT TO ASSUMED VALUES  00640000
         AGO   .GO1                                                     00660000
.ERBLP   IHBERMAC 180,BLP,0                                             00680000
&IHBBLP    SETA  0                                                      00700000
         AGO   .TAG0                                                    00720000
.NOBLIM  AIF   (&IHBBLIM LE &IHBBLC).NOBLIM2                            00740000
         AGO   .BLPCHK                                                  00760000
.NOBLIM2 IHBERMAC 180,BLIM,32767                                        00780000
.ROND    ANOP                                                           00800000
&IHBBLIM   SETA  32767                                                  00820000
.BLPCHK  AIF   (T'&BLP EQ 'O').EXIT                                     00840000
.GO1     ANOP                                                           00860000
&IHBINIT    SETC  'IHBI'.'&SYSNDX'                                      00880000
&IHBINIT    DS    0H                                                    00900000
         AIF   ('&NAME' EQ '').EXIT                                     00920000
&NAME    EQU   &IHBINIT                                                 00940000
IHB&NAME   EQU   &IHBBLP                                                00960000
.EXIT    MEND                                                           00980000
