         MACRO                                                          00020000
&NAME    GIBLC &N                                                       00040000
         GBLA  &IHBBLC,&IHBBLIM                                         00060000
         AIF   (T'&N EQ 'O').ERR                                        00080000
         AIF   (T'&N NE 'N').ERR1                                       00100000
&IHBBLC    SETA  &IHBBLC+&N                                             00120000
         AIF   (&IHBBLIM GE &IHBBLC).OK                                 00140000
         IHBERMAC 182                                                   00160000
.OK      MEXIT                                                          00180000
.ERR1    IHBERMAC  36,,&N                                               00200000
         MEXIT                                                          00220000
.ERR     IHBERMAC 2                                                     00240000
         MEND                                                           00260000
