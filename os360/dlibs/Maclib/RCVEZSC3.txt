         MACRO                                                          01000017
&NAME RCVEZSC3 &F1=4E,&F5=60,&F8=6F,&F9=2A,&F11=4D,&F12=5D,&F14=6B,    -02000017
               &F15=7A,&F17=3F,&F18=61,&F19=7D,&F20=4B,&F22=7E,&F24=3F,-03000017
               &F26=3F                                                  04000017
         LCLA  &N,&X                                                    05000017
         LCLC  &ARG,&PARM                                               06000017
         LCLC  &B(15)                                                   07000017
         LCLC  &LCNAME                                                  08000017
.*                                                                      09000017
&LCNAME  SETC  'RCVEZSC3'                                               10000017
         AIF   (T'&NAME EQ 'O').NONAM                                   11000017
&LCNAME  SETC  '&NAME'                                                  12000017
.NONAM   ANOP                                                           13000017
         SPACE 1                                                        14000017
&LCNAME  CSECT                                                          15000017
*              * TRANSLATION TABLE - ZSC3 TO EBCDIC *                   16000017
         SPACE 1                                                        17000017
.*    MACRO VARIABLE DECLARATIONS.                                      18000017
.*                                                                      19000017
.*                                                                      20000017
.NONAME  ANOP                                                           21000017
&N       SETA  &N+1                                                     22000017
&ARG     SETC  '&F1.*&F5.*&F8.*&F9.*&F11.*&F12.*&F14.*&F15.*&F17.*&F18.-23000017
               *&F19.*&F20.*&F22.*&F24.*&F26.*'(3*&N,1)                 24000017
         AIF   ('&ARG' EQ '*').NEXT                                     25000017
&X       SETA  1                                                        26000017
         AGO   .ERR                                                     27000017
.NEXT    AIF   (&N LT 15).NONAME                                        28000017
.*                                                                      29000017
&N       SETA  0                                                        30000017
.LOOP    ANOP                                                           31000017
&N       SETA  &N+1                                                     32000017
&ARG   SETC '*&F1&F5&F8&F9&F11&F12&F14&F15&F17&F18&F19&F20&F22&F24&F26'-33000017
               (2*&N,2)                                                 34000017
         AIF   ('&ARG'(1,1) LT 'A' OR  '&ARG'(1,1) GT '9').ERR          35000017
         AIF   ('&ARG'(1,1) GT 'F' AND '&ARG'(1,1) LT '0').ERR          36000017
         AIF   ('&ARG'(2,1) LT 'A' OR  '&ARG'(2,1) GT '9').ERR          37000017
         AIF   ('&ARG'(2,1) GT 'F' AND '&ARG'(2,1) LT '0').ERR          38000017
&B(&N)   SETC  '&ARG'                                                   39000017
.OK      AIF   (&N LT 15).LOOP                                          40000017
*              * 0 1 2 3 4 5 6 7 8 9 A B C D E F  * *                   41000017
         DC    X'3FE315D640C8D5D425D3D9C7C9D7C3E5' 0                    42000017
         DC    X'C5E9C4C2E2E8C6E7C1E6D136E4D8D237' 1                    43000017
         DC    X'3F&B(12).15&B(8).40&B(3)&B(7).F725&B(6)&B(10).F0&B(4).-44000017
               F9F8&B(13)' 2                                            45000017
         DC    X'&B(2)&B(15).26F6&B(11).F5F4&B(14)&B(1).F3F236F1&B(9)&B-46000017
               (5).06' 3                                                47000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' 4                    48000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' 5                    49000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' 6                    50000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' 7                    51000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' 8                    52000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' 9                    53000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' A                    54000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' B                    55000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' C                    56000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' D                    57000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' E                    58000017
         DC    X'3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F' F                    59000017
         SPACE 1                                                        60000017
&SYSECT  CSECT                                                          61000017
         MEXIT                                                          62000017
.*                                                                      63000017
.ERR     ANOP                                                           64000017
&ARG     SETC '**F1 F5 F8 F9 F11F12F14F15F17F18F19F20F22F24F26'(3*&N,3) 65000017
         AIF   (&X NE 0).END                                            66000017
&B(&N)   SETC  '3F'                                                     67000017
.*                                                                      68000017
         MNOTE 4,'***   IHB086 &ARG INVALID KEYWORD, 3F ASSUMED'        69000017
         AGO   .OK                                                      70000017
.END     ANOP                                                           71000017
         MNOTE 12,'***   IHB002 INVALID &ARG OPERAND SPECIFIED'         72000017
         MEND                                                           73000017
