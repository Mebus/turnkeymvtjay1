         MACRO                                                          00400018
&NAME    RJETERM &TYPE=2780,&PUNCH=YES,&PRTSZ=120,               A29501X00800000
               &FEATURE=NONE,&PACK=NO,&ID=                       A29501 01200000
         GBLA  &IHKTMNB                                            000A 01600018
         GBLB  &IHKSWT                                             000A 02000018
         GBLB &IHKTRB                                              000C 02400018
         GBLB  &IHKUP                                              000C 02800018
         GBLB  &IHKPU                                              000C 03200018
         GBLB  &IHKENT                                             000C 03600018
         LCLB  &SW(8)                                              000A 04000018
         LCLC  &CHAR                                               000A 04400018
         LCLA  &NUMB,&NUMBR                                        000D 04600019
.*27830050-9500                                                    000A 04800018
.*1210014000,126000-140000,784000,798000-805000                    000C 05200018
.* 732000                                                        A28213 05400019
.* 188000-196000                                                 A29493 05500019
.* 008000,012000,226000,236000,248000                            A29501 05530000
.* 662000                                                        A29510 05560000
.*C371000                                                        A40162 05570021
.*A636500                                                        A40162 05580021
.*C 098000,102000,110000                                         A43947 05590021
.*C 647000-650000                                                A61477 05593021
.*                                                                      05596021
         AIF   ('&NAME' NE '').NOK                                 000A 05600018
         MNOTE 12,'NAME FIELD BLANK. NAME IS REQUIRED'             000A 06000018
&SW(1)   SETB  1                                                   000A 06400018
.NOK     ANOP                                                      000A 06800018
         AIF   ('&PUNCH' EQ 'YES' OR '&PUNCH' EQ 'NO').PCHOK       000A 07200018
         MNOTE 8,'PUNCH=&PUNCH INCORRECTLY SPECIFIED. DEFAULT ASSUMED'  07600018
.*                                                                 000A 08000018
&SW(3)   SETB  1                                                   000A 08400018
.PCHOK   ANOP                                                      000A 08800018
&SW(4)   SETB  ('&TYPE' EQ '2780')                                 000A 09200018
         AIF   ((&SW(4)  OR '&TYPE' EQ '1130' OR '&TYPE' EQ            X09500019
               '2020' OR '&TYPE' EQ '2770' OR '&TYPE' EQ 'SYS3' OR     X09800021
               '&TYPE' EQ 'INTMX') AND '&ID' NE '').MPTLN        A43947 10100021
         AIF   ('&TYPE' EQ 'CPU' OR '&TYPE' EQ '1130' OR '&TYPE'       Z10600019
               EQ '2020' OR '&TYPE' EQ 'SYS3').ACPU              A43947 11000021
         AIF   ('&TYPE' EQ '2770').CKPRT                           000D 11400019
         AIF   ('&TYPE' EQ '2780').CKPRT                           000A 11800019
         MNOTE 8,'INVALID TYPE OPERAND=&TYPE.2780 ASSUMED'         000A 12200019
         AGO   .CKPRT                                              000A 12600019
.MPTLN   ANOP                                                      000A 13000019
.CNTOK   ANOP                                                      000A 13400019
         AIF   (&SW(4)).CK2780                                     000A 13800019
&CHAR    SETC  '&ID.2D'                                            000A 14200019
         AGO   .CKPCH                                              000A 14600019
.CK2780  ANOP                                                      000A 15000019
&CHAR    SETC  '&ID.F32D'                                          000A 15400019
         AGO   .CKPCH                                              000A 15800019
.ACPU    ANOP                                                      000A 16200019
         AIF   ('&ID' EQ '').CKPCH                                 000A 16600019
         MNOTE 12,'ID SPECIFIED FOR TYPE=CPU INVALID'              000A 17000019
&SW(1)   SETB  1                                                   000A 17400019
.CKPCH   ANOP                                                      000A 17800019
.CKPRT   ANOP                                                      000A 18200019
         AIF   ('&PRTSZ' EQ '120' OR '&PRTSZ' EQ '132' OR '&PRTSZ' EQ  X18600019
               '144').PRTOK                                        000A 19000019
         MNOTE 8,'INVALID PRTSZ. DEFAULT=120 ASSUMED'              000A 19400019
&SW(2)   SETB  1                                                   000A 19800019
.PRTOK   ANOP                                                      000A 20200019
         AIF   (&SW(1)).END                                        000A 20600019
         AIF   ('&FEATURE' EQ '2780MR' OR '&FEATURE' EQ 'NONE'         X21600000
               OR '&FEATURE' EQ '2770EB' OR '&FEATURE'                 X22800019
               EQ '').SETMR                                      A29501 23200000
         MNOTE 8,'INVALID FEATURE OPERAND NONE ASSUMED'          A29501 23600019
         AGO   .TPACK                                              000C 24000018
.SETMR   ANOP                                                      000C 24400018
         AIF   ('&FEATURE' EQ 'NONE' OR '&FEATURE' EQ '').TPACK  A29501 24800019
&SW(5)   SETB  1                                                   000C 25100019
.TPACK   ANOP                                                      000C 25400019
         AIF   ('&PACK' EQ 'YES' OR '&PACK' EQ 'NO'OR                  X25700019
               '&PACK' EQ '').APAC                                 000C 26000019
         MNOTE 8,'INVALID PACK OPERAND PACK=NO ASSUMED'            000C 26300019
         AGO   .PTOK                                               000C 26600019
.APAC    ANOP                                                      000C 26900019
        AIF   ('&PACK' EQ 'NO' OR '&PACK' EQ '').PTOK              000C 27200019
         AIF   (&SW(4)).PINV                                       000C 27500019
&SW(6)   SETB  1                                                   000C 27800019
         AGO   .PTOK                                               000C 28100019
.PINV    ANOP                                                      000C 28400019
         MNOTE 8,'INVALID MACHINE SPECIFIED FOR PACK'              000C 28700019
.PTOK    ANOP                                                      000C 29000019
IHKKEN   CSECT                                                     000A 29300019
&IHKTMNB SETA  &IHKTMNB+1                                          000A 29600019
         AIF   (&IHKSWT).SKIP                                      000A 29900019
IHKCDTNB DS    0F                                                  000A 30200019
         DC    HL2'&IHKTMNB'       NO OF TERMINALS SUPPORTED. WILL BE   30500019
*                                  UPDATED BY SUCCEEDING RJETERM MACROS 30800019
.*   ABOVE TWO CARDS CHANGED                                       000A 31100019
         ENTRY IHKCDTNB                                            000A 31400019
*********************************************************************   31700019
*                                                                    *  32000019
*                                                                    *  32300019
*         THE TERMINAL DIRECTORY                                     *  32600019
*        THIS DIRECTORY IS TO MAINTAIN A LIST OF POTENTIAL           *  32900019
*   REMOTE TERMINALS AVAILABLE TO THE RJE SYSTEM.                    *  33200019
*                                                                    *  33500019
*                                                                    *  33800019
*********************************************************************   34100019
*                                                                    *  34400019
.*   ABOVE  CARDS ADDED                                            000A 34700019
         AGO   .ARND                                               000A 35000019
.SKIP    ANOP                                                      000A 35300019
         ORG   *-2                                                 000A 35600019
         DC    HL2'&IHKTMNB'                                       000A 35900019
.ARND    ANOP                                                      000A 36200019
IHKWELL  CSECT                                                     000A 36500019
         AIF   (&IHKSWT).BY1                                       000A 36800019
IHKCDTLN DC    H'80'   NO OF BYTES PER ENTRY IN TERMINAL DIRECT  A40162 37100021
.*                                                                 000A 37400019
IHKCDTDR DS    0F                                                  000A 37700019
         ENTRY IHKCDTDR,IHKCDTLN                                   000A 38000019
&IHKSWT  SETB  1                                                   000A 38300019
.BY1     ANOP                                                      000A 38600019
&NAME    DC    CL8'&NAME' .        TERMINAL NAME                   000A 38900019
         DC    3XL1'00' .          USER IDENTIFICATION             000A 39200019
         DC    XL1'00' .           STATUS BYTE                     000A 39500019
         AIF   (&SW(5)).SEMR                                       000C 39800019
         AIF   (&SW(6)).SEPAC                                      000C 40100019
         DC    XL1'00' .           TDIRRB                          000A 40400019
         AGO   .CONGO                                              000C 40700019
.SEMR    ANOP                                                      000C 41000019
         DC    X'04'               TDIRRB                          000C 41300019
         AGO   .CONGO                                              000C 41600019
.SEPAC   ANOP                                                      000C 41900019
         DC    X'08'               TDIRRB                          000C 42200019
         AGO   .CONGO                                              000C 42500019
.CONGO   ANOP                                                      000C 42800019
         DC    3XL1'00' .          TTR OF THIS ENTRY IN DISK COPY  000A 43100019
         DC    F'0' .              FIRST TERMINAL QEB              000A 43400019
         DC    F'0' .              LAST TERMINAL QEB               000A 43700019
         DC    F'0' .              LAST MESSAGE QEB                000A 44000019
         DC    CL4' ' .            PRINTER FORM NUMBER             000A 44300019
         DC    F'0' .              STOP ACK ECB                    000A 44600019
         DC    XL2'00' .           TDIR SWITCHES                   000A 44900019
         AIF   (&SW(4)).T2780                                      000A 45200019
         AIF   ('&TYPE' EQ '2770').T2780                           000D 45500019
         DC    X'40' .        LINE ANALYSIS HARDWARE CONFIGURATION 000A 45800019
         AGO   .NEXT                                               000A 46100019
.T2780   AIF   ('&PUNCH' EQ 'NO').PCHNO                            000A 46400019
         DC    X'00' .        LINE ANALYSIS HARDWARE CONFIGURATION 000A 46700019
         AGO   .NEXT                                               000A 47000019
.PCHNO   DC    X'20' .        LINE ANALYSIS HARDWARE CONFIGURATION 000A 47300019
.NEXT    AIF   ('&PRTSZ' NE '120' AND (NOT &SW(2))).T132           000A 47600019
         DC    XL1'78' .           PRINTER SIZE,120                000A 47900019
         AGO   .FIN                                                000A 48200019
.T132    AIF   ('&PRTSZ' NE '132').T144                            000A 48500019
         DC    XL1'84' .           PRINTER SIZE,132                000A 48800019
         AGO   .FIN                                                000A 49100019
.T144    DC    XL1'90' .           PRINTER SIZE,144                000A 49400019
.FIN     ANOP                                                      000A 49700019
         AIF   ('&ID' NE '').A1                                    000A 50000019
         DC    4F'0' .             RESERVE FOR DFTRMLST            000A 50300019
         AGO   .B1                                                 000A 50600019
.A1      ANOP                                                      000A 50900019
&NUMB    SETA  K'&ID                                               000D 51200019
&NUMBR   SETA  10                                                  000D 51500019
         AIF   ('&NUMB' GE '&NUMBR').MNTE                          000D 51800019
         DC    0F'0'                                               000A 52100019
         AIF   ('&TYPE' EQ '1130' OR '&TYPE' EQ '2020').E1130      000C 52400019
         AIF   ('&TYPE' EQ '2770').T2770                           000D 52700019
IHKK&SYSNDX DFTRMLST AUTOWLST,&CHAR,373737                         000C 53000019
         DC    0F'0'                                               000A 53300019
         AGO   .B1                                                 000A 53600019
.T2770   ANOP                                                      000D 53900019
IHKK&SYSNDX   DFTRMLST  AUTOWLST,&CHAR,37373737                    000D 54200019
         AGO   .B1                                                 000D 54500019
.E1130   ANOP                                                      000A 54800019
IHKK&SYSNDX DFTRMLST AUTOWLST,&CHAR,3737                           000A 55100019
         DC    F'00'                                               000C 55400019
         DC    0F'0'                                               000A 55700019
.B1      ANOP                                                      000A 56000019
         DC    AL4(0) .            ADDR OF LCB FOR THIS TERMINAL   000A 56300019
         DC    8XL1'00'                                            000A 56600019
         AIF   ('&ID' NE '').EN1                                   000C 56900019
         DC    F'00'                                               000D 57200019
         DC    H'00'                                               000D 57500019
         AGO   .SKIPA                                              000D 57800019
         DC    2F'00'                                              000C 58100019
         AGO   .TRIN                                               000C 58400019
.EN1     ANOP                                                      000C 58700019
IHKE&SYSNDX DFTRMLST OPENLST,&CHAR                                 000A 59000019
         AIF   ('&TYPE' EQ '2770').SKIPA                           000D 59300019
         DC    H'00'                                               000D 59600019
.SKIPA   ANOP                                                      000D 59900019
         AIF   ('&TYPE' NE '2770').STRIN                           000D 60200019
         DS    0H                                                  000D 60500019
         DC    X'00'                                               000D 60800019
         AIF   ('&FEATURE' EQ '2770EB').SETT                       000D 61100019
         DC    X'01'                                               000D 61400019
         AGO   .TRIN                                               000D 61700019
.SETT    ANOP                                                      000D 62000019
         DC    X'03'                                               000D 62300019
         AGO   .TRIN                                               000D 62600019
.STRIN   ANOP                                                      000D 62900019
         DC    H'00'                                               000D 63200019
.TRIN    ANOP                                                      000C 63500019
         DC    X'00' .             SAVE AREA FOR VOL SEQ NUM     A40162 63600021
         DC    3X'00' .            NOT USED                      A40162 63700021
         AIF   (&SW(4)).TRAN                                       000C 63800019
         AIF   ('&TYPE' EQ '2780' OR '&TYPE' EQ '2770').TRAN     A32856 63900000
         AGO   .IHKPK                                              000C 64100019
.TRAN    ANOP                                                      000C 64400019
         AIF   (&IHKTRB).IHKPK                                   A61477 64700021
         ENTRY IHKTRTAB                                          A61477 65000021
&IHKTRB  SETB  1                                                   000C 65300019
IHKTRTAB CSECT                                                     000C 65600019
*IHKTRTAB                     ***DATA LINK ZAP TRANSLATE TABLE     000C 65900019
         DC    X'004040400440060708090A0B0C0D0E0F'               A29510 66200000
         DC    X'401112131415161718191A1B1C1D1E40'                 000C 66500019
         DC    X'202122232425404028292A2B2C402E2F'                 000C 66800019
         DC    X'303140333435364038393A3B3C403E3F'                 000C 67100019
         DC    X'404142434445464748494A4B4C4D4E4F'                 000C 67400019
         DC    X'505152535455565758595A5B5C5D5E5F'                 000C 67700019
         DC    X'606162636465666768696A6B6C6D6E6F'                 000C 68000019
         DC    X'407172737475767778797A7B7C7D7E7F'               A28213 68300019
         DC    X'808182838485868788898A8B8C8D8E8F'                 000C 68600019
         DC    X'909192939495969798999A9B9C9D9E9F'                 000C 68900019
         DC    X'A0A1A2A3A4A5A6A7A8A9AAABACADAEAF'                 000C 69200019
         DC    X'B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF'                 000C 69500019
         DC    X'C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF'                 000C 69800019
         DC    X'D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF'                 000C 70100019
         DC    X'E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF'                 000C 70400019
         DC    X'F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF'                 000C 70700019
.IHKPK   ANOP                                                      000C 71000019
IHKTRTAB CSECT                                                     000C 71300019
IHKAPACK CSECT                                                     000C 71600019
*******************************************************************000C 71900019
*        THIS ROUTINE GENERATES THE V-CONS NECESSARY TO MAKE THE   000C 72200019
*        PACK ROUTINES RESIDENT IF SPECIFIED.  IF NOT IT GENERATES 000C 72500019
*        AN ENTRY POINT TO RESOLVE THE EXTERNAL REFERENCES.        000C 72800019
*******************************************************************000C 73100019
         AIF   (&IHKENT).IHKNT                                     000C 73400019
&IHKENT  SETB  1                                                   000C 73700019
         ENTRY IHKADPCK                                            000C 74000019
         ENTRY IHKADUPK                                            000C 74300019
.IHKNT   ANOP                                                      000C 74600019
         AIF   (&SW(6)).SPAC                                       000C 74900019
         AIF   (&IHKPU).END                                        000C 75200019
&IHKPU   SETB  1                                                   000C 75500019
IHKADPCK EQU   *                                                   000C 75800019
IHKADUPK EQU   *+4                                                 000C 76100019
         AGO   .END                                                000C 76400019
.SPAC    ANOP                                                      000C 76700019
         AIF   (&IHKPU).IHKP                                       000C 77000019
&IHKPU   SETB  1                                                   000C 77300019
IHKADPCK EQU   *                                                   000C 77600019
IHKADUPK EQU   *+4                                                 000C 77900019
.IHKP    ANOP                                                      000C 78200019
         AIF   (&IHKUP).END                                        000C 78500019
&IHKUP   SETB  1                                                   000C 78800019
         DC    V(IHKCHPCK)                                         000C 79100019
         DC    V(IHKCHUPK)                                         000C 79400019
         AGO   .END                                                000D 79700019
.MNTE    ANOP                                                      000D 80000019
         MNOTE  8,'ADDRESSING CHARACTERS EXCEED K ASSEMBLY             X80300019
               TERMINATED'                                         000D 80600019
.END     MEND                                                           88400018
