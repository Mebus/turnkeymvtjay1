         MACRO                                                          04000013
&NAME    RCVSEG                                                         08000013
         GBLB  &SW,&RCV,&SEN,&RHDR,&SHDR                                12000013
         GBLC  &SEND,&TEXT,&MGTP                                        16000013
.* 440000,560000                                                 A26022 18000019
         AIF   ('&NAME' EQ '').NONAME                                   20000013
&NAME    DS    0H                                                       24000013
.NONAME  AIF   (&RCV AND NOT &SEN).OK                                   28000013
         MNOTE 12,'***  IHB071  SEQUENCE ERROR IN LPS DELIMITER MACRO'  32000013
.OK      AIF   ('&TEXT' EQ '').NOTX                                     36000013
         AIF   ('&MGTP' EQ '').NOTP                                     40000013
&MGTP    DS    0H                                                A26022 44000019
&MGTP    SETC  ''                                                       48000013
.NOTP    ANOP                                                           52000013
&TEXT    DS    0H                                                       56000019
&TEXT    SETC  ''                                                       60000013
.NOTX    ANOP                                                           64000013
&RHDR    SETB  0                                                        68000013
&SHDR    SETB  0                                                        72000013
.GN      MEND                                                           76000013
