         MACRO                                                          02000013
&NAME    STOPLN  &DCB,&RLN                                              04000013
         GBLB  &IEDQE7                                                  04400020
.* 044000,052000-056000,744000-960000                             20002 04800020
&NAME    DS    0H                                                       05200020
         AIF   (&IEDQE7 EQ 1).TCAM                                20002 05600020
         AIF   ('&DCB' NE '' AND '&RLN' NE '').OK                       06000013
         MNOTE 12,'***  IHB004  REQUIRED OPERAND(S) NOT SPECIFIED'      08000013
         MEXIT                                                          10000013
.OK      AIF   ('&DCB'(1,1) EQ '(' OR K'&DCB LT 9).OK2                  12000013
         MNOTE 12,'***  IHB002  INVALID TERMNAME OPERAND SPECIFIED'     14000013
         MEXIT                                                          16000013
.OK2     ANOP                                                           18000013
         AIF   ('&RLN' EQ 'ALL').ALL                                    22000013
         AIF   ('&RLN' EQ '(0)').SHIFT                                  24000013
         AIF   ('&RLN'(1,1) NE '(').LA                                  26000013
         LR    0,&RLN(1)               REL. LINE NUMBER TO REGISTER 0   28000013
         AGO   .SHIFT                                                   30000013
.LA      LA    0,&RLN                  REL. LINE NUMBER TO REGISTER 0   32000013
.SHIFT   SLL   0,24                    POSITION RELATIVE LINE NUMBER    34000013
.ALL     AIF   ('&DCB'(1,1) EQ '(').NOLOC                               36000013
         AIF   ('&RLN' NE 'ALL').NOSR                                   38000013
         SR    0,0                     ZERO RELATIVE LINE NUMBER        40000013
.NOSR    BAL   1,*+12                  SET TERMNAME ADDRESS IN REG. ONE 42000013
         DC    CL8'&DCB'                                                44000013
         L     15,=V(IECKDCBL)         ADDRESS OF THE DCB LOCATE RT.    46000013
         BALR  14,15                   LINK TO THE DCB LOCATE ROUTINE   48000013
         LTR   15,15                   TEST THE RETURN CODE             50000013
         BC    7,*+12                  BRANCH ON ERROR-INVALID TERMNAME 52000013
         AGO   .AFT                                                     54000013
.NOLOC   AIF   ('&RLN' EQ 'ALL').NORLN                                  56000013
         LA    1,0(&DCB(1))            DCB ADDRESS TO REGISTER ONE      58000013
         ALR   0,1                     COMBINE DCB AND REL. LINE NUMBR. 60000013
         AGO   .AFT                                                     62000013
.NORLN   LA    0,0(&DCB(1))            DCB ADDRESS TO REGISTER ZERO     64000013
.AFT     L     15,=V(IECKLNCH)         ADDRESS OF THE CHANGE LINE RT.   66000013
         AIF   ('&RLN' EQ 'ALL').EX2                                    68000013
         BAL   14,0(15)                TO STOP 1 LINE ENTRY IN IECKLNCH 70000013
         AGO   .END                                                     72000013
.EX2     BAL   14,4(15)                TO STOP ALL ENTRY IN IECKLNCH    74000013
.END     MEXIT                                                          74400020
.TCAM    ANOP                                                           74800020
         AIF   ('&DCB' NE '' AND '&RLN' NE '').OK1                20002 75200020
         MNOTE 12,'***  IHB004  REQUIRED OPERAND(S) NOT SPECIFIED'      75600020
         MEXIT                                                          76000020
.OK1     ANOP                                                           76400020
         AIF   ('&DCB'(1,1) EQ '(' OR K'&DCB LT 9).OK3            20002 76800020
         MNOTE 12,'***  IHB002  INVALID TERMNAME OPERAND SPECIFIED'     77200020
         MEXIT                                                          77600020
.OK3     ANOP                                                           78000020
         AIF   ('&RLN'(1,1) EQ '(').STC                           20002 78400020
         AIF   ('&RLN' NE '0').ALLX                               20002 78800020
         MNOTE 12,'*** IHB002 INVALID RLN OPERAND SPECIFIED'            79200020
         MEXIT                                                          79600020
.STC     LTR   &RLN(1),&RLN(1) .        INVALID LINE NUMBER       20002 80000020
         BZ    IEDQ&SYSNDX+40 .         BRANCH IF YES             20002 80400020
         STC   &RLN(1),IEDQ&SYSNDX+10 . RELATIVE LINE NUMBER      20002 80800020
         AGO   .TEST                                              20002 81200020
.ALLX    AIF   ('&RLN' NE 'ALL').NEXT                             20002 81600020
         MVI   IEDQ&SYSNDX+10,0 .       'ALL' INDICATOR           20002 82000020
         AGO   .TEST                                              20002 82400020
.NEXT    ANOP                                                           82800020
         LA    15,&RLN .                RELATIVE LINE NUMBER      20002 83200020
         STC   15,IEDQ&SYSNDX+10 .      RELATIVE LINE NUMBER      20002 83600020
.TEST    ANOP                                                           84000020
         AIF   ('&DCB'(1,1) EQ '(').DCB                           20002 84400020
         BAL   15,*+12 .                TERMNAME ADDRESS          20002 84800020
         DC    CL8'&DCB' .              TERMINAL NAME             20002 85200020
         MVC   IEDQ&SYSNDX+16(8),0(15) MOVE TERMNAME INTO CIB     20002 85600020
         AGO   .LINK                                              20002 86000020
.DCB     ANOP                                                           86400020
         ST    &DCB(1),IEDQ&SYSNDX+16 . ADDRESS OF DCB            20002 86800020
         MVI   IEDQ&SYSNDX+16,0 .       SET DCB ADDR FLAG         20002 87200020
.LINK    ANOP                                                           87600020
         SR    0,0 .                    INDICATE NO PASSWORD      20002 88000020
         MVI   IEDQ&SYSNDX+11,X'FF' .   FLAG AS CQTAM MACRO       20002 88400020
         CNOP  0,4 .                                              20002 88800020
         BAL   1,*+46 .                 SET CIB ADDR IN PARM REG  20002 89200020
IEDQ&SYSNDX DS 0F .                     COMMAND INPUT BUFFER      20002 89600020
         DC    F'0' .                   OPERATOR CONTROL QCB ADDR 20002 90000020
         DC    XL1'E0' .                CIB QUEUING PRIORITY      20002 90400020
         DC    XL3'00' .                LINK FIELD                20002 90800020
         DC    XL1'41' .                VERB CODE FOR STOPLN      20002 91200020
         DC    XL1'1C' .                CIB LENGTH                20002 91600020
         DC    XL1'00' .                RELATIVE LINE NUMBER      20002 92000020
         DC    XL1'FF' .                COMPLETION CODE(CQTAM)    20002 92400020
         DC    A(IEDQ&SYSNDX+32) .      ECB ADDRESS               20002 92800020
         DC    XL8'00' .                TERMINAL NAME             20002 93200020
         DC    XL4'00' .                WORKAREA ADDRESS          20002 93600020
         DC    A(IEDQ&SYSNDX) .         ADDRESS OF CIB            20002 94000020
         DC    F'0' .                   PCB WORKAREA/ECB          20002 94400020
         DC    F'0' .                   ADDRESS OF CIB LENGTH     20002 94800020
         SR    1,1 .                                              20002 95200020
         LINK  EP=IEDQET .                                        20002 95600020
         MEND                                                           96000020
