         MACRO                                                          00020000
&NAME    GMVD  &ADDR,&GROUP,&BDATA=0    MOVE IMMEDIATE DATA 2AEC        00040000
         GBLA  &IHBBLC,&IHBBLIM                                         00060000
&IHBBLC  SETA  &IHBBLC+2                                                00080000
&NAME    IHBGAM1 10988,&ADDR,&GROUP                                     00100000
         DC    XL2'&BDATA'                                              00120000
         MEND                                                           00140000
