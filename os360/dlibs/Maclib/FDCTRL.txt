         MACRO                                                          01000000
&NAME    FDCTRL &IF=,&IND=,&COMMAND=,&TOTAL=,&GOTO=,&SAVELOC=,&CYCLE=, X02000000
               &CTR=                                                    03000000
         COPY  IDFGBL                                                   04000000
         AIF   (&PIB(46)).TX  SYNTAX CHECK                              05000000
         AIF   (NOT &PIB(13)).X1        NOT LOAD MODE             FSDB  06000000
.********************************************************************** 07000052
.*                                                                    * 07050052
.*             PURGE FILE IN LOAD MODE                                * 07100052
.*                         REFERENCE APAR IR00271                     * 07150052
.*                                                                    * 07200052
.********************************************************************** 07250052
&PIB(1)  SETB  (T'&SYSLIST(1) NE 'O' OR T'&IF NE 'O' OR T'&GOTO NE 'O' X07300052
               OR T'&TOTAL NE 'O' OR T'&SAVELOC NE 'O')                 07350052
&PIB(1)  SETB  (&PIB(1) OR T'&CYCLE NE 'O' OR T'&CTR NE 'O')            07400052
         AIF   (&PIB(1) OR &PIB(27) OR &PIB(2)).JC01                    07450052
         AIF   ('&COMMAND' NE 'PURGE' AND '&COMMAND' NE 'PURGE(FILE)').X07500052
               JC01                                                     07550052
         IDFASM (,4776)                                                 07600052
         IDFASM (,4378)                                                 07650052
         MNOTE *,'IDF158 FILE PURGED IN LOAD MODE.'                     07700052
         AGO   .TX                                                      07750052
.JC01    MNOTE 8,'IDF791 INVALID USE OF FDCTRL IN LOAD MODE.'           07800052
&PIB(15) SETB  1                                                        07850052
         AGO   .TX                                                FSJM  08000000
.X1      AIF   (NOT&PIB(48)).TR                                         09000000
         MNOTE *,'IDF100 IN TRACE MODE ENTERING FDCTRL'                 10000000
.TR      ANOP                                                           11000000
.********************************************************************** 12000000
.*                                                                    * 13000000
.*                           CONTINUATION CHECK                       * 14000000
.*                                                                    * 15000000
.********************************************************************** 16000000
&PIB(1)  SETB  (&PIB(2) AND NOT &PIB(3))                                17000000
         AIF   (NOT &PIB(1)).I0033                                      18000000
         IDFMSG 018                    INVLAID CONTINUATION             19000000
         IDFIN01                                                        20000000
         IDFIN02                                                        21000000
         IDFIN03                                                        22000000
         AIF   (&PIB(3)).CTC                                            23000000
         IDFIN04                                                        24000000
         IDFIN05                                                        25000000
         AIF   (&PIA(1) NE 4).ENDC                                      26000000
         IDFIN06                                                        27000000
         IDFIN07                                                        28000000
         IDFIN08                                                        29000000
         AGO   .ENDC                                                    30000000
.CTC     IDFIN09                                                        31000000
         IDFIN10                                                        32000000
.ENDC    IDFIN11                                                        33000000
.I0033   ANOP                                                           34000000
.********************************************************************** 35000000
.*                                                                    * 36000000
.*                           ORDER CHECK                              * 37000000
.*                                                                    * 38000000
.********************************************************************** 39000000
&PIB(1)  SETB  (&PIA(2) EQ 0)                                           40000000
         AIF   (NOT &PIB(1)).I0034                                      41000000
         IDFMSG 008  FDFORM MUST START FORM                             42000000
.I0034   ANOP ,                                                         43000000
         AIF   (&PIB(1)).C2                                             44000000
         AIF   (&PIB(2)).C0                                             45000000
.********************************************************************** 46000000
.*                                                                    * 47000000
.*                           SETUP                                    * 48000000
.*                                                                    * 49000000
.********************************************************************** 50000000
&PIB(25) SETB  (&PIB(7) OR &PIB(9)) INVOKE END CYCLE          A01301    51000051
&PIB(42) SETB  1 INVOKE IMPLICIT GOTO                                   52000000
&PIB(40) SETB  1 ALLOW GOTO                                             53000000
&PIB(35) SETB  1 INVOKE END CTRL                                        54000000
.C0      ANOP                                                           55000000
&PIB(3)  SETB  1                                                        56000000
&PIB(1)  SETB  1                                                        57000000
         IDFIN01 &IF,&IND,&CTR,&COMMAND,&TOTAL,                        X58000000
               &GOTO,,,,&SAVELOC,                                      X59000000
               &NAME,,&CYCLE                                            60000000
         IDFIN02 &IF,&IND,&CTR,&COMMAND,&TOTAL,                        X61000000
               &GOTO,,,,&SAVELOC,                                      X62000000
               &NAME,&SYSLIST(1),&CYCLE                                 63000000
         IDFIN03 &IF,&IND,&CTR,&COMMAND,&TOTAL,                        X64000000
               &GOTO,,,,&SAVELOC,                                      X65000000
               &NAME,,&CYCLE                                            66000000
         IDFIN09 &IF,&IND,&CTR,&COMMAND,&TOTAL,                        X67000000
               &GOTO,,,,&SAVELOC,                                      X68000000
               &NAME,,&CYCLE                                            69000000
         IDFIN10 &IF,&IND,&CTR,&COMMAND,&TOTAL,                        X70000000
               &GOTO,,,,&SAVELOC,                                      X71000000
               &NAME,,&CYCLE                                            72000000
         IDFIN11 &IF,&IND,&CTR,&COMMAND,&TOTAL,                        073000000
               &GOTO,,,,&SAVELOC,                                      X74000000
               &NAME,,&CYCLE                                            75000000
&PIB(3)  SETB  (&PIB(2))                FDCTRL MODE OFF UNLESS CONTIN   76000000
.C2      AIF   (NOT &PIB(48)).TX                                        77000000
         MNOTE *,'IDF100 IN TRACE MODE LEAVING FDCTRL'                  78000000
.TX      ANOP                                                           79000000
         MEND                                                           80000000
