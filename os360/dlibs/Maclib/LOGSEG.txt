         MACRO                                                          04000013
&NAME    LOGSEG &PAR1                                                   08000013
         AIF   ('&PAR1' NE '').GO                                       12000013
         MNOTE 12,'***  IHB001  DCB OPERAND REQ''D-NOT SPECIFIED'       16000013
         AGO   .END                                                     20000013
.GO      ANOP                                                           24000013
&NAME    LA    14,4(6)             ADDRESS FIRST BYTE TO LOG            28000013
         LH    15,4(14)            LOAD MESSAGE SIZE                    32000013
         LA    15,4(15)            ADD FOUR                             36000013
         STH   15,0(14)            STORE IN FIRST HALFWORD TO LOG       40000013
         LR    0,14 LOAD ADDRESS OF FIRST BYTE TO LOG IN REGISTER 0     44000013
         AIF   ('&PAR1'(1,1) EQ '(').REG                                48000013
         L     1,=A(&PAR1)             LOAD THE DCB ADDRESS             52000013
         AGO   .ADR                                                     56000013
.REG     AIF   ('&PAR1' EQ '(1)').ADR                                   60000013
         LR    1,&PAR1(1)              DCB ADDRESS TO REGISTER 1        64000013
.ADR     L     15,48(1)                LOAD ADDRESS OF OUTPUT ROUTINE   68000013
         BALR  14,15                   TO OUTPUT ROUTINE                72000013
.END     MEND                                                           76000013
