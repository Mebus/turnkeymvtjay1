         MACRO                                                          03000013
&NAME    SENDSEG                                                        06000013
         GBLB  &SW,&RCV,&SEN,&RHDR,&SHDR                                09000013
         GBLC  &SEND,&TEXT,&MGTP                                        12000013
.* 540000,630000                                                 A26022 13000019
         AIF   (&SEN AND NOT &RCV).OK                                   15000013
         MNOTE 12,'***  IHB071  SEQUENCE ERROR IN LPS DELIMITER MACRO'  18000013
.OK      AIF   ('&NAME' EQ '').NONAME                                   21000013
&NAME    DS    0H                                                       24000013
.NONAME  AIF   (NOT &SW).NOFS                                           27000013
&SEND    TM    12(6),X'10'             IS THE MESSAGE COMPLETE          30000013
&SEND    SETC  'IHB'.'&SYSNDX'                                          33000013
         BC    1,&SEND                 BRANCH IF MESSAGE COMPLETE       36000013
&SW      SETB  0                                                        39000013
&TEXT    SETC  ''                                                       42000013
&MGTP    SETC  ''                                                       45000013
.NOFS    AIF   ('&TEXT' EQ '').NOTX                                     48000013
         AIF   ('&MGTP' EQ '').NOTP                                     51000013
&MGTP    DS    0H                                                A26022 54000019
&MGTP    SETC  ''                                                       57000013
.NOTP    ANOP                                                           60000013
&TEXT    DS    0H                                                A26022 63000019
&TEXT    SETC  ''                                                       66000013
.NOTX    ANOP                                                           69000013
&RHDR    SETB  0                                                        72000013
&SHDR    SETB  0                                                        75000013
         MEND                                                           78000013
