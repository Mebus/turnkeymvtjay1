         MACRO                                                          01000000
&NAME    TRANSLAT &TRANSCH=,&TRANTXT=                                   02000000
         GBLA  &IECNDGP(100)                                            03000000
.*             IECNDGP(11)  NUMBER OF TRANSLAT MACROS                   04000000
         GBLB  &IECGDSW,&IECSEQ(10),&IECSQ(7)                           05000000
.*             IECGDSW   INDICATES GDU=YES CODED                        06000000
.*             IECSQ(6)  SEQUENCE INDICATOR                             07000000
         LCLA  &A,&B,&C                                                 08000000
         LCLB  &NOCODE                                                  09000000
         ACTR  1000                                                     10000000
         AIF   (&IECSEQ(1) AND &IECSEQ(2) AND &IECSQ(1) AND &IECSQ(2)  X11000000
               AND &IECSQ(3) AND &IECSQ(4) AND NOT &IECSEQ(4)).SEQ1     12000000
.SEQ2    ANOP                                                           13000000
&NOCODE  SETB  1                                                        14000000
         MNOTE 12,'MACRO SEQUENCE ERROR'                                15000000
         AGO   .SEQ3                                                    16000000
.SEQ1    ANOP                                                           17000000
         AIF   (&IECSEQ(5) OR &IECSEQ(10) OR &IECSEQ(6)).SEQ2           18000000
.SEQ3    ANOP                                                           19000000
         AIF   (&IECGDSW).TRAN1                                         20000000
&NOCODE  SETB  1                                                        21000000
         MNOTE 12,'TRANSLAT MACRO NOT VALID UNLESS GDU=YES IN CONFIGUR X22000000
               MACRO'                                                   23000000
.TRAN1   ANOP                                                           24000000
         AIF   (T'&TRANSCH NE 'O').TRAN2                                25000000
&NOCODE  SETB  1                                                        26000000
         MNOTE 12,'TRANCH OPERAND CAN NOT BE OMITTED'                   27000000
         AGO   .LEN1                                                    28000000
.TRAN2   ANOP                                                           29000000
         AIF   (K'&TRANSCH EQ 2 ).TRAN4                                 30000000
&NOCODE  SETB  1                                                        31000000
         MNOTE 12,'TRANSCH OPERAND MUST HAVE TWO HEX CHARACTERS ONLY'   32000000
         AGO   .LEN1                                                    33000000
.TRAN4   ANOP                                                           34000000
.TRAN6   ANOP                                                           35000000
&B       SETA  &B+1                                                     36000000
         AIF   ('&TRANSCH' EQ '*051525404B4E505A5B5C'(2*&B,2)).LEN1     37000000
         AIF   ('&TRANSCH' EQ '*5E60616B6F7A7B7C7E7F'(2*&B,2)).LEN1     38000000
         AIF   ('&TRANSCH' EQ '*F0F1F2F3F4F5F6F7F8F9'(2*&B,2)).LEN1     39000000
         AIF   ('&TRANSCH' EQ '*C1C2C3C4C5C6C7C8C9D1'(2*&B,2)).LEN1     40000000
         AIF   ('&TRANSCH' EQ '*D2D3D4D5D6D7D8D9E2E3'(2*&B,2)).LEN1     41000000
         AIF   ('&TRANSCH' EQ '*E4E5E6E7E8E9E9E9E9E9'(2*&B,2)).LEN1     42000000
         AIF   (&B LT 10).TRAN6                                         43000000
&NOCODE  SETB  1                                                        44000000
         MNOTE 12,'TRANSCH OPERAND IS INVALID'                          45000000
.LEN1    ANOP                                                           46000000
.TEXT1   ANOP                                                           47000000
         AIF   (T'&TRANTXT NE 'O').TEXT2                                48000000
&NOCODE  SETB  1                                                        49000000
         MNOTE 12,'TRANTXT OPERAND CAN NOT BE OMITTED'                  50000000
         AGO   .CKGEN                                                   51000000
.TEXT2   ANOP                                                           52000000
         AIF   ('&TRANTXT'(1,1) EQ '''' AND '&TRANTXT'(K'&TRANTXT,1)   X53000000
               EQ '''').TEXT3                                           54000000
&NOCODE  SETB  1                                                        55000000
         MNOTE 12,'TRANTXT OPERAND IN INCORRECT FORMAT'                 56000000
         AGO   .CKGEN                                                   57000000
.TEXT3   ANOP                                                           58000000
&B       SETA  K'&TRANTXT-2                                             59000000
         AIF   (&B GE 1 AND &B LE 14).CKGEN                             60000000
&NOCODE  SETB  1                                                        61000000
         MNOTE 12,'TRANTXT OPERAND DOES NOT HAVE BETWEEN 1 AND 14      X62000000
               CHARACTERS'                                              63000000
.CKGEN   ANOP                                                           64000000
         AIF   (NOT &NOCODE).GEN                                        65000000
         MNOTE 12,'TEXT GENERATION SUPPRESSED'                          66000000
         AGO   .MEND                                                    67000000
.GEN     ANOP                                                           68000000
         AIF   (&IECSQ(6)).GEN1                                         69000000
&IECNDGP(11) SETA 0                                                     70000000
.GEN1    ANOP                                                           71000000
         AIF   (&IECNDGP(11) LT 8).GEN2                                 72000000
&NOCODE  SETB  1                                                        73000000
         MNOTE 12,'NO MORE THAN 8 TRANSLAT MACROS CAN BE CODED'         74000000
         AGO   .CKGEN                                                   75000000
.GEN2    ANOP                                                           76000000
&IECNDGP(11) SETA &IECNDGP(11)+1                                        77000000
         AIF   (&IECSQ(6)).GEN3                                         78000000
*                                                                       79000000
         DC    0H'0'                                                    80000000
IECTRANS EQU   *                                                        81000000
         SPACE 1                                                        82000000
*                      GUIDANCE DISPLAY UNIT TRANSLATE TABLE            83000000
          SPACE 1                                                       84000000
.GEN3    ANOP                                                           85000000
         DC    XL1'&TRANSCH'       TRANSLATE CHARACTER                  86000000
         DC    YL1(&B)             LENGTH BYTE                          87000000
         DC    CL14&TRANTXT        TRANSLATE TEXT                       88000000
&B       SETA  &B+2                                                     89000000
&IECNDGP(1) SETA &IECNDGP(1)+16                                         90000000
&IECSQ(6)  SETB 1                                                       91000000
.MEND    MEND                                                           92000000
