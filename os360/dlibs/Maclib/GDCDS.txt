         MACRO                                                          00020000
&NAME    GDCDS &DCDS,&N,&GROUP                                          00040000
         GBLA  &IHBBLC,&IHBBLIM,&IHBBLP                                 00060000
         GBLC  &IHBINIT                                                 00080000
         LCLA  &WN                                                      00100000
         AIF   (T'&DCDS EQ 'O').ERR1    VALIDATE DCDS PARAMETER         00120000
         AIF   ('&DCDS'(1,1) EQ 'C').CONST                              00140000
         AIF   ('&DCDS'(1,1) EQ 'S').SPACE                              00160000
.ERR1    IHBERMAC 2                                                     00180000
         MEXIT                                                          00200000
.SPACE   AIF   (T'&N EQ 'O').ERR2A VALIDATE NUMBER OF BYTES             00220000
         AIF   (T'&N NE 'N').ERR2                                       00240000
         AIF   (&N GT 32767).ERR2                                       00260000
&NAME    DS    CL&N                                                     00280000
&IHBBLC    SETA  &IHBBLC+&N                 UPDATE BUFFER LOC COUNTER   00300000
         AGO   .TEST                                                    00320000
.ERR2A   IHBERMAC  3                                                    00340000
         MEXIT                                                          00360000
.ERR2    IHBERMAC  37,,&N                                               00380000
         MEXIT                                                          00400000
.CONST   AIF   (T'&N EQ 'O').ERR2A CHECK OMITTED CONSTANT               00420000
&IHBBLC    SETA  &IHBBLC+2                  UPDATE BUFFER LOC COUNTER   00440000
         AIF   ('&N'(1,1) EQ '+' OR '&N'(1,1) EQ '-').SIGNCON           00460000
         AIF   (T'&N NE 'N').SYMAD                                      00480000
.SIGNCON ANOP                                                           00500000
&NAME    DC    H'&N'                                                    00520000
         AGO   .TEST                                                    00540000
.SYMAD   AIF   (T'&GROUP NE 'O').GRSYM                                  00560000
&NAME    DC    AL2(&N-&IHBINIT+&IHBBLP)                                 00580000
         AGO   .TEST                                                    00600000
.GRSYM   ANOP                                                           00620000
&NAME    DC    AL2(&N-&GROUP+IHB&GROUP)                                 00640000
.TEST    AIF   (&IHBBLIM GE &IHBBLC).OK                                 00660000
         IHBERMAC 182                                                   00680000
.OK      MEND                                                           00700000
