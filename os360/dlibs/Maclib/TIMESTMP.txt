         MACRO                                                          04000013
&NAME    TIMESTMP &PAR1                                                 08000013
         GBLB  &RHDR,&SHDR                                              12000013
         LCLA  &CNT                                                     16000013
         LCLB  &NOGO                                                    20000013
         LCLC  &V                                                       24000013
         AIF   (&RHDR OR &SHDR).OK                                      28000013
         MNOTE 12,'***  IHB070  SEQUENCE ERROR-MUST FOLLOW SENDHDR OR RX32000013
               CVHDR MACRO'                                             36000013
.OK      AIF   ('&PAR1' NE '').OK1                                      40000013
         MNOTE 12,'***  IHB001  VALUE OPERAND REQ''D-NOT SPECIFIED'     44000013
         MEXIT                                                          48000013
.OK1     ANOP                                                           52000013
&V       SETC  '&PAR1'                                                  56000013
&CNT     SETA  &V                                                       60000013
         AIF   (&CNT GT 0 AND &CNT LT 13).VAL                           64000013
         MNOTE 12,'***  IHB002  INVALID VALUE OPERAND SPECIFIED'        68000013
         MEXIT                                                          72000013
.VAL     AIF   (&NOGO).END                                              76000013
&NAME    L     15,=V(IECKTIME)         ADDRESS OF TIME INSERT ROUTINE   80000013
         BALR  14,15                   LINK TO THE TIME ROUTINE         84000013
         DC    FL2'&CNT'                                                88000013
.END     MEND                                                           92000013
