         MACRO                                                          03000013
&NAME    PROCESS &PAR                                                   06000013
         GBLA  &QNAME,&TSIZE                                            09000013
         GBLC  &SECT                                                    12000013
         AIF   ('&NAME' NE '').OK                                       15000013
         MNOTE 12,'***  IHB001  ENTRY OPERAND REQ''D-NOT SPECIFIED'     18000013
         MEXIT                                                          21000013
.OK      AIF   (K'&NAME LE &TSIZE).OK2                                  24000013
         MNOTE 12,'***  IHB002  INVALID ENTRY OPERAND SPECIFIED'        27000013
         MEXIT                                                          30000013
.OK2     ANOP                                                           33000013
IECKQUE  CSECT                                                          36000013
&QNAME   SETA  &QNAME+1                                                 39000013
IHB&SYSNDX DC  AL1(3)                  QKEY                             42000013
         DC    AL3(&QNAME)         QUEUE NUMBER AND QSTART              45000013
         DC    A(0)               QLINK                                 48000013
         AIF   ('&PAR' EQ 'EXPEDITE').EX                                51000013
         DC    3A(0)              FILLER                                54000013
         AGO   .EXCOM                                                   57000013
.EX      DC    2A(0)              FILLER                                60000013
         DC    A(1)                EXPEDITE FLAG                        63000013
.EXCOM   DC    H'0'                QSIZE                                66000013
         DC    AL3(&QNAME)         QNASEG                               69000013
         DC    AL3(IHB&SYSNDX.+1)      QSOURCE                          72000013
         DC    A(0)                QBACK                                75000013
&SECT    CSECT                                                          78000013
&NAME    DC    AL1(IHB&SYSNDX.A-&NAME) TNTRYSZE                         81000013
         DC    AL3(IHB&SYSNDX)         TQCBADDR FOR PROCESS PROGRAM     84000013
         DC    X'0001000100'      SEQIN , SEQOUT , STATUS               87000013
         DC    CL&TSIZE'&NAME'         PROCESS ENTRY IDENTIFICATION     90000013
IHB&SYSNDX.A DS 0F                                                      93000013
         MEND                                                           96000013
