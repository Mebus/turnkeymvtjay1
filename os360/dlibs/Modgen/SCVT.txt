         MACRO                                                          00020000
&NAME    SCVT  &TYPE=TRN                                                00040000
.*0018012000                                                       RORI 00050015
&NAME    DS    0F                                                       00060000
         AIF   ('&TYPE' EQ 'RES').NEXT1                                 00080000
SCVTPGTM DC    V(IEAQPGTM)              ADDR OF EOT TIMER PURGE ROUTINE 00100000
         AGO   .NEXT2                                                   00120000
.NEXT1   ANOP                                                           00140000
SCVTPGTM DC    A(IEAQPGTM)              ADDR OF EOT TIMER PURGE ROUTINE 00160000
.NEXT2   ANOP                                                           00180000
SCVTPGWR DC    V(IEECVPRG)              ADDR OF WTOR PURGE ROUTINE      00200000
         AIF   ('&TYPE' EQ 'RES').NEXT3                                 00220000
SCVTSPET DC    V(IEAQSPET)              ADDR OF EOT SUBPOOL RELEASE     00240000
         AGO   .NEXT4                                                   00260000
.NEXT3   ANOP                                                           00280000
SCVTSPET DC    A(IEAQSPET)              ADDR OF EOT SUBPOOL RELEASE     00300000
.NEXT4   AIF   ('&TYPE' EQ 'RES').NEXT41                                00320000
SCVTTACT DC    V(IEAQTAQ)               ADDR OF T.A. CONTROL TABLE      00340000
         AGO   .NEXT42                                                  00360000
.NEXT41  ANOP                                                           00380000
         EXTRN IEAQTAQ                                                  00400000
SCVTTACT DC    A(IEAQTAQ)               ADDR OF T.A. CONTROL TABLE      00420000
.NEXT42  AIF   ('&TYPE' EQ 'RES').NEXT5                                 00440000
SCVTERAS DC    V(IEAQERA)               ADDR OF EOT ERASE PHASE         00460000
         AGO   .NEXT6                                                   00480000
.NEXT5   ANOP                                                           00500000
SCVTERAS DC    A(IEAQERA)               ADDR OF EOT ERASE PHASE         00520000
.NEXT6   AIF   ('&TYPE' EQ 'RES').NEXT61                                00540000
SCVTQCBO DC    V(IEAQQCB0)              ADDR OF QCB ORIGIN              00560000
         AGO   .NEXT62                                                  00580000
.NEXT61  ANOP                                                           00600000
         EXTRN IEAQQCB0                                                 00620000
SCVTQCBO DC    A(IEAQQCB0)              ADDR OF QCB ORIGIN              00640000
.NEXT62  ANOP                                                           00660000
SCVTPGEQ DC    V(IEA0EQ01)              ADDR OF ENQ/DEQ PURGE ROUTINE   00680000
SCVTRMBR DC    V(RMBRANCH)              ADDR OF REGMAIN BRANCH ENTRY    00700000
SCVTPGIO DC    V(IGC016)                ADDR OF I/O PURGE ROUTINE       00720000
SCVTRACE DC    V(IECXTRA)               ADDR OF TRACE ROUTINE SWITCH    00754020
SCVTTASW DC    V(IEA0DS02)              ADDR OF TASK SWITCHING ROUTINE  00760000
SCVTCDCL DC    V(IEAQCS02)              ADDR OF CDCONTRL IN LINK        00780000
SCVTLFRM DC    V(FMBRANCH)              LIST FORMAT FREEMAIN BRANCH EP  00800000
         AIF   ('&TYPE' EQ 'RES').NEXT7                                 00820000
SCVTPABL DC    V(IEAQABL)               ADDR OF CDABDEL IN EOT          00840000
SCVTDQTC DC    V(IEADQTCB)              ADDR OF TCB DEQ RTN IN EOT      00860000
SCVTHSKP DC    V(CDHKEEP)               ADDR OF CDHKEEP IN EOT          00880000
         AGO   .NEXT8                                                   00900000
.NEXT7   ANOP                                                           00920000
SCVTPABL DC    A(IEAQABL)               ADDR OF CDABDEL IN EOT          00940000
SCVTDQTC DC    A(IEADQTCB)              ADDR OF TCB DEQ RTN IN EOT      00960000
SCVTHSKP DC    A(CDHKEEP)               ADDR OF CDHKEEP IN EOT          00980000
.NEXT8   AIF   ('&TYPE' EQ 'RES').NEXT9                                 01000000
SCVTRPTR DC    V(TRPTR)                 ADDR OF TRACE TABLE POINTERS    01020000
         AGO   .NEXT10                                                  01040000
.NEXT9   ANOP                                                           01060000
         EXTRN TRPTR                                                    01080000
SCVTRPTR DC    A(TRPTR)                 ADDR OF TRACE TABLE POINTERS    01100000
.NEXT10  ANOP                                                           01120000
SCVTGMBR DC    V(GMBRANCH)              LIST FORMAT GETMAIN BRANCH EP   01140000
         AIF   ('&TYPE' EQ 'RES').NEXT11                                01160000
SCVTAUCT DC    V(TAUSERCT)              TRANSIENT AREA USER COUNT       01180000
         AGO   .NEXT12                                             RORI 01200015
.NEXT11  ANOP                                                           01220000
         EXTRN TAUSERCT                                                 01240000
SCVTAUCT DC    A(TAUSERCT)              TRANSIENT AREA USER COUNT       01260000
.NEXT12  ANOP                                                      RORI 01261015
         AIF   ('&TYPE' EQ 'RES').NEXT13                           RORI 01262015
SCVTROCT DC    V(IEARCTRS)           ADDRESS OF ROLLOUT COUNTERS   RORI 01263015
SCVTROQ  DC    V(IEAROQUE)              ADDRESS OF ROLLOUT QUEUE   RORI 01264015
SCVTRIRB DC    V(IEAROIRB)              ADDRESS OF ROLLOUT IRB     RORI 01265015
SCVTRTCB DC    V(IEAROTCB)              ADDRESS OF ROLLOUT TCB     RORI 01266015
         AGO   .SKPEND                                             RORI 01267015
.NEXT13  ANOP                                                      RORI 01268015
         EXTRN IEARCTRS                                            RORI 01269015
SCVTROCT DC    A(IEARCTRS)           ADDRESS OF ROLLOUT COUNTERS   RORI 01270015
         EXTRN IEAROQUE                                            RORI 01271015
SCVTROQ  DC    A(IEAROQUE)              ADDRESS OF ROLLOUT QUEUE   RORI 01272015
         EXTRN IEAROIRB                                            RORI 01273015
SCVTRIRB DC    A(IEAROIRB)              ADDRESS OF ROLLOUT IRB     RORI 01274015
         EXTRN IEAROTCB                                            RORI 01275015
SCVTRTCB DC    A(IEAROTCB)              ADDRESS OF ROLLOUT TCB     RORI 01276015
.SKPEND  ANOP                                                           01280000
SCVTCOMM DC    V(IEECVCTW)             ENTRY IN COMM TASK          ABND 01282018
SCVTABLK DC    V(SCEDWAIT)             ADDR IN ABTERM              ABND 01284018
SCVTNFND DC    V(TBNOTFND)             ADDR IN IEQRT33             ABND 01286018
SCVTRMTC DC    V(IGFRMTCB)             RMS TCB                    19022 01288019
SCVTMSSQ DC    V(GOVRFLB)              ORIGIN OF MAIN STOR QUEUES  ABND 01290018
SCVTCTCB DC    V(IEECVTCB)             COMM TASK TCB               ABND 01292018
SCVTETCB DC    V(IEAERTCB)             SYSTEM ERROR TCB            ABND 01294018
SCVTRXLQ DC    A(0) .                  RECOVERY EXTNT LIST        20021 01294420
SCVTRQND DC    V(IECITSAR) .           END OF I/O RQE TABLE       20021 01294820
SCVTTAR  DC    V(IEAQTR02) .           TRAN AREA REFRESH RTN      20021 01295220
SCVTSVCT DC    V(IBMORG) .             ORIGIN OF SVC TABLE        20021 01295620
SCVTSTXP DC    V(IEAKJXP) .            STAX PURGE RTN             20021 01296020
SCVTTQE  DC    V(IEATSELM) .           TSO SUBSYSTEM'S TQE        20021 01296420
SCVTRMSV DC    V(IORMSSVC)             RMS SVC                    19022 01297019
SCVTMSGS DC    V(IEAQMSGS) .           INFORMATION LIST MODULE    21016 01299021
SCVTFMSA DC    V(IEA10FS) .             ADDRESS OF FREE SAVE AREA M3806 01299421
SCVTSW1  EQU   X'80' .                  SW SET BEFORE ENTERING    M3806 01299821
*                                        FREEMAIN                 M3806 01299921
SCVTSW2  EQU   X'40' .                  SW SET BEFORE ENTERING    M3806 01349921
*                                        EXIT                     M3806 01359921
SCVTSW3  EQU   X'20' .                  SW SET BEFORE FMPAR       21472 01369921
*                                        ENTRY IN EXIT FROM ABEND 21472 01379921
         MEND                                                           01399921
