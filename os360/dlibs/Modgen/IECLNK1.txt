         MACRO                                                          01000017
         IECLNK1 &LABEL,&OPRND,&OP1,&OP2,&OP3,&OP4,&EP,&TBASE           02000017
         LCLC  &VECTOR                                                  03000017
         AIF   ('&TBASE' EQ '0').FORMAT                                 04000017
         AIF   ('&TBASE' EQ '1' AND '&OPRND' EQ 'BAL').CONVBAL          05000017
         AIF   ('&TBASE' EQ '1' AND '&OPRND' EQ 'BALR').CONVBLR         06000017
         AIF   ('&TBASE' EQ '1' AND '&OPRND' EQ 'BC').CONVBC            07000017
         AIF   ('&TBASE' EQ '1' AND '&OPRND' EQ 'BCR').CONVBCR          08000017
         AIF   ('&TBASE' EQ '1' AND '&OPRND' EQ 'LA').CONVLA            09000017
         AIF   ('&TBASE' EQ '1' AND '&OPRND' EQ 'LR').CONVLR            10000017
         MNOTE 7,'* * * &OPRND - OPERAND SPECIFIED NOT VALID'           11000017
         MEXIT                                                          12000017
.CONVBAL ANOP                                                           13000017
&LABEL   LA    WKREG1,&EP                                          TBSE 14000017
         ST    WKREG1,LNKRG1S                                      TBSE 15000017
         BC    15,&OP2                                             TBSE 16000017
         MEXIT                                                          17000017
.CONVBLR ANOP                                                           18000017
&LABEL   LA    WKREG1,&EP                                          TBSE 19000017
         ST    WKREG1,LNKRG1S                                      TBSE 20000017
         BCR   15,&OP2                                             TBSE 21000017
         MEXIT                                                          22000017
.CONVBC  ANOP                                                           23000017
         AIF   ('&OP2' EQ 'SELBSY').BSYV                                24000017
         AIF   ('&OP2' EQ 'SELTAG').TAGV                                25000017
.BSYV    ANOP                                                           26000017
&VECTOR  SETC  'SELBSYV'                                                27000017
         AGO   .CONV                                                    28000017
.TAGV    ANOP                                                           29000017
&VECTOR  SETC  'SELTAGV'                                                30000017
.CONV    ANOP                                                           31000017
&LABEL   BC    &OP1,&VECTOR                                        TBSE 32000017
         MEXIT                                                          33000017
.CONVBCR ANOP                                                           34000017
&LABEL   BC    &OP1,SELECTV                                        TBSE 35000017
         MEXIT                                                          36000017
.CONVLA  ANOP                                                           37000017
         AIF   ('&OP4' EQ 'LNKRG1').LA2                                 38000017
&LABEL   LA    WKREG1,&OP2                                         TBSE 39000017
         ST    WKREG1,LNKRG1S                                      TBSE 40000017
         MEXIT                                                          41000017
.LA2     ANOP                                                           42000017
&LABEL   L     WKREG1,LNKRG1S                                      TBSE 43000017
         LA    WKREG1,SELTAG(WKREG1)                               TBSE 44000017
         ST    WKREG1,LNKRG1S                                      TBSE 45000017
         MEXIT                                                          46000017
.CONVLR  ANOP                                                           47000017
&LABEL   L     &OP1,LNKRG1S                                        TBSE 48000017
         MEXIT                                                          49000017
.FORMAT  AIF   (T'&OP3 EQ 'O').FORM2                                    50000017
.FORM1   ANOP                                                           51000017
&LABEL   &OPRND &OP1,&OP2.(&OP3,&OP4)                                   52000017
         MEXIT                                                          53000017
.FORM2   ANOP                                                           54000017
&LABEL   &OPRND &OP1,&OP2                                               55000017
         MEXIT                                                          56000017
         MEND                                                           57000017
