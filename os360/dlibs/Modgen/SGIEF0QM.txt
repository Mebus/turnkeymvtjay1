         MACRO                                                          00020000
         SGIEF0QM &N,&T,&K,&P,&L                                  20035 00030020
.* C 001200,001350                                                M6526 00040020
IEFQMSGV CSECT                                                          00060000
         DC    AL2(&N)             NO. RECORDS PER LOGICAL TRACK        00080000
         DC    AL2(&T)             THRESHOLD VALUE FOR INIT PROCESSING  00100000
         DC    AL2(&K)      NO. RECORDS RESERVED FOR TERMINATION  M6526 00120020
         DC    AL2(&P)                 JOBQWTP LIMIT VALUE         I254 00130019
         DC    AL2(&L)       LIMIT OF RECORDS FOR SUBMIT          M6526 00135020
         MEND                                                           00140000
