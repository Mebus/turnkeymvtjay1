         MACRO                                                          00020000
         SGIEA2TC                                                       00040000
         COPY  SGGBLPAK                                                 00060000
         LCLC  &IDENT,&TRSVCTB,&TESTRAN,&STORAGE,&SCHED            MOG1 00080016
         LCLC  &PROTECT,&RAM,&RESVC,&HIARCHY                       LC0A 00110017
         LCLC  &LPAQ                                               LPA  00130018
         LCLC  &ERP                                               19017 00135019
         LCLC  &TMSLC                                              I275 00137019
         LCLC  &ATTACH                                             I250 00138019
         LCLC  &DDRSR                                             19022 00139019
&IDENT   SETC  'N'                                                      00140000
&TRSVCTB SETC  'N'                                                      00160000
&TESTRAN SETC  'N'                                                      00180000
&STORAGE SETC  'PRIM'                                              MOH1 00200017
&SCHED   SETC  'SSS'                                               MOH1 00220017
&PROTECT SETC  'N'                                                      00240000
&RAM     SETC  'N'                                                      00260000
&RESVC   SETC  'N'                                                      00280000
&HIARCHY SETC  'N'                                                 LC0A 00285017
&LPAQ    SETC  'N'                                                 LPA  00288018
&ERP     SETC  'N'                                                19017 00289019
&TMSLC   SETC  'N'                                                 I275 00289519
&ATTACH  SETC  'N'                                                 I250 00289719
&DDRSR   SETC  'N'                                                19022 00289819
.*0000001200,003000-003600                                         MOG1 00290016
.*0821001000,002000-002200,011000,011800                           LC0A 00330017
.*011150,011800                                                   19017 00340019
.*001200,002870,009050-009150,011800                              19018 00350019
.*001200,002870,007200-008600,011800                               I275 00360019
.*011800                                                           I250 00370019
.*011800                                                          19022 00375019
.****** TEST FOR IDENTIFY OPTION ************************************** 00380000
         AIF   (&SGSUPRB(1) NE 1).NEXT2                                 00400000
&IDENT   SETC  'Y'                                                      00420000
.NEXT2   ANOP                                                           00440000
.****** TEST FOR TRANSIENT SVC TABLE OPTION *************************** 00460000
         AIF   (&SGSUPRB(3) NE 1).NEXT3                                 00480000
&TRSVCTB SETC  'Y'                                                      00500000
.NEXT3   ANOP                                                           00520000
.****** TEST FOR TESTRAN OPTION *************************************** 00540000
         AIF   (&SGTESTB(3) NE 1).NEXT5                                 00560000
&TESTRAN SETC  'Y'                                                      00580000
.NEXT5   ANOP                                                           00600000
.****** TEST FOR STORAGE CONFIGURATION ******************************** 00700000
         AIF   (&SGCPRGB(4) NE 1).NEXT9                                 00880000
&STORAGE SETC  'PART'                                                   00900000
.****** TEST FOR PROTECTION FEATURE *********************************** 00920000
.NEXT9   AIF   (&SGSUPRB(23) NE 1).NEXT10                               00940000
&PROTECT SETC  'Y'                                                      00960000
.******* TEST FOR RESIDENT ACCESS METHODS****************************** 00980000
.NEXT10  AIF  (&SGSUPRB(21) NE 1).NEXT11                                01000000
&RAM    SETC  'Y'                                                       01020000
.******  TEST FOR RESIDENT SVC ROUTINES ******************************* 01040000
.NEXT11  AIF   (&SGSUPRB(25) NE 1).NEXT12                               01060000
&RESVC   SETC  'Y'                                                      01080000
.****** TEST FOR STORAGE HIERARCHIES OPTION ********************** LC0A 01090017
.NEXT12  AIF   (&SGCPRGB(11) NE 1).NEXT13                          LC0A 01097017
&HIARCHY SETC  'Y'                                                 LC0A 01098019
.NEXT13  ANOP                                                      LC0A 01099019
.******** TEST FOR LINK PACK AREA OPTION ***********************        01100019
         AIF   (&SGSUPRB(38) NE 1 OR &SGCPRGB(4) NE 1).NEXT14 BD A51853 01101000
&LPAQ    SETC  'Y'                                                 LPA  01102019
.******** TEST FOR RESIDENT ERPS OPTION ***********************   19017 01103019
.NEXT14  AIF   (&SGSUPRB(40) NE 1).NEXT15                         19017 01104019
&ERP     SETC  'Y'                                                19017 01105019
.NEXT15  ANOP                                                     19017 01106019
.*********     TEST FOR TIME SLICING IN SYSTEM *****************   I275 01107019
         AIF   (&SGCPRGB(12) NE 1).NEXT16                          I275 01108019
&TMSLC   SETC  'Y'                                                 I275 01109019
.NEXT16  ANOP                                                      I275 01110019
.******** TEST FOR ASYNCHRONOUS ATTACH OPTION ******************   I250 01111019
         AIF   (&SGSUPRB(37) NE 1).NEXT17                          I250 01112019
&ATTACH  SETC  'Y'                                                 I250 01113019
.NEXT17  ANOP                                                      I250 01114019
.******** TEST FOR I/O RECOVERY OPTION *************************  19022 01115019
         AIF   (&SGSUPRB(34) NE 1).NEXT18                         19022 01116019
&DDRSR   SETC  'Y'                                                19022 01117019
.NEXT18  ANOP                                                     19022 01118019
&SGCTRLC(6) SETC 'IEAATC00'                                             01128019
         COPY  SGASMPAK                                                 01140000
         PUNCH '         IEAATC &IDENT,&TRSVCTB,&TESTRAN,&STORAGE,&SCHE*01160016
               D,&PROTECT,&RAM,&RESVC,&HIARCHY,&LPAQ,&ERP,&TMSLC,&ATTAC*01170019
               H,&DDRSR'                                                01180019
         PUNCH '         END'                                           01200000
         PUNCH '/*'                                                     01220000
         MEND                                                           01240000
