         MACRO                                                          02000019
         SGIKM400                                                       04000019
.*                 REL 20 DELETIONS.                                    04500020
.*  300000-420000,540000,680000-700000                           S20046 05000020
.*  536000,*  668000,*  704000,*                 DCR526           M2254 05300056
.*   680000,*                                                     M0790 05700000
.* R20.1  500000  640000  692000                                  M5790 05800020
.* R21.6 C 528000,664000,700000                               MCB 49385 05900056
         COPY  SGGBLPAK                                                 06000019
         AIF   (NOT &SGCKPLB(1)).END                                    08000019
.* TYPE=PL1 SPECIFIED IN CHECKER MACRO                                  10000019
.*  GENERATE JCL TO LINK PL1 SYNTAX CHECKER MODULES TO LINKLIB          12000019
&SGCTRLC(7)  SETC  '&SGCTRLC(32)'    VOLUME NUMBER FOR LINKLIB          14000019
&SGCTRLC(8)  SETC  '&SGCTRLC(31)'    UNIT NAME FOR LINKLIB              16000019
&SGCTRLC(9)  SETC  'LINKLIB'         OUTPUT LIBRARY                     18000019
&SGCTRLC(6)  SETC  ''                MEMBER NAMES WITH NAME CARDS       20000019
&SGCTRLC(11) SETC  ',RENT'            MODULES ARE REENTRANT             22000019
         COPY  SGLEDPK1                                                 24000019
         PUNCH '//PL552 DD DISP=SHR,VOL=(,RETAIN),DSN=SYS1.PL552'       26000019
         COPY  SGLEDPK2                                                 28000019
         AIF   (NOT &SGCKPLB(3)).NO20K                                  36000020
.* SIZE=20K SPECIFIED IN CHECKER MACRO OR SIZE DEFAULTED                44000019
         PUNCH ' INCLUDE  PL552(IKM001,IKM02,IKM03,IKM11,IKM12)'        46000019
         PUNCH ' INCLUDE  PL552(IKM131,IKM132,IKM133,IKM134)'           48000019
         PUNCH ' INCLUDE  PL552(IKM135,IKM136)'                         50000019
         PUNCH ' ENTRY IKM001 '                                   M5790 51000020
         PUNCH ' ALIAS  PLICHK20 '                                      52000019
         AIF   (NOT &SGSCHDB(44)).NOTS20                          49385 52600056
         PUNCH ' ALIAS PLIFSCAN'                                        53300020
         PUNCH ' ALIAS PLISCAN '                                        53800020
         MNOTE *,' TSO WILL USE THE 20K VERSION OF PL/I SYNTAX CHECKER' 54400020
.NOTS20  PUNCH ' NAME  IKM001(R)'                                       55200020
.NO20K   AIF   (NOT &SGCKPLB(4)).NO27K                                  56000019
.* SIZE=27K SPECIFIED IN CHECKER MACRO                                  58000019
         PUNCH ' INCLUDE  PL552(IKM002,IKM02,IKM03,IKM11,IKM12)'        60000019
         PUNCH ' INCLUDE  PL552(IKM131,IKM132,IKM133,IKM134)'           62000019
         PUNCH ' INCLUDE PL552(IKM135,IKM136,IKM23,IKM22,IKM21)'        64000019
         PUNCH ' ENTRY IKM002 '                                   M5790 65000020
         PUNCH ' ALIAS  PLICHK27 '                                      66000019
         AIF   (NOT &SGSCHDB(44) OR &SGCKPLB(3)).NOTS27           49385 66300056
         PUNCH ' ALIAS PLIFSCAN'                                        66600020
         PUNCH ' ALIAS PLISCAN '                                        66800020
         MNOTE *,' TSO WILL USE THE 27K VERSION OF PL/I SYNTAX CHECKER' 67200020
.NOTS27  PUNCH ' NAME  IKM002(R)'                                       67600020
.NO27K   AIF   (NOT &SGCKPLB(2)).NO16K                            M0790 68000000
.* SIZE=16K SPECIFIED IN CHECKER MACRO                                  68400020
         PUNCH ' INCLUDE PL552(IKM003,IKM02,IKM03,IKM11,IKM12)'         68800020
         PUNCH ' INCLUDE PL552(IKM132,IKM133,IKM134,IKM135)'            69200020
         PUNCH ' ENTRY IKM003 '                                   M5790 69400020
         PUNCH ' ALIAS PLICHK16'                                        69600020
         AIF   (NOT &SGSCHDB(44)).NOTS16                          49385 69800056
         AIF   (&SGCKPLB(3) OR &SGCKPLB(4)).NOTS16                49385 70000056
         PUNCH ' ALIAS PLIFSCAN'                                        70200020
         PUNCH ' ALIAS PLISCAN '                                        70400020
         MNOTE *,' TSO WILL USE THE 16K VERSION OF PL/I SYNTAX CHECKER' 70800020
.NOTS16  PUNCH ' NAME  IKM003(R)'                                       71200020
.NO16K   ANOP                                                           71600020
         PUNCH '/*'                                                     72000019
.END     MEND                                                           74000019
