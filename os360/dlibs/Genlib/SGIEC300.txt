         MACRO                                                          00020000
         SGIEC300                                                       00040000
         COPY  SGGBLPAK                                                 00060000
.* 000800-003400                                                   I266 00120019
         PUNCH '   INCLUDE SYSPUNCH(IEC23XXF)'                     I266 00180019
         PUNCH '   INCLUDE CI505(SCXCPMON) INCLUDE ITEL SCXCPMON' ITH01 00190021
         AIF   (&GETB(3)).NOTO4         TEST FOR IODEVICE SYSGEN   I266 00240019
         PUNCH '   INCLUDE CI505(IECINTRP)'                        I266 00300019
.SG30A   AIF   (&SGSUPRA(1) EQ 0).SG300                                 00360000
 PUNCH '  INCLUDE SYSPUNCH(IEAATR00) '                                  00380000
.SG300   AIF   (NOT &SGSCHDB(44)).SG301 TEST FOR TSO              20001 00384020
         AIF   (NOT &SGCPRGB(13)).ONYTSO                                00386021
         PUNCH '   INCLUDE CI535(IECIPRMT)'                             00386421
         AGO   .NOTO4                                                   00386821
.ONYTSO  ANOP                                                           00387221
         PUNCH '    INCLUDE CI555(IECIPRTS)'                      20001 00388020
         AGO   .NOTO4                   BRANCH AROUND             20001 00392020
.SG301   AIF   (NOT &SGCPRGB(8)).NOTO4  TEST FOR MVT              20001 00396020
         AIF   (NOT &SGCPRGB(13)).NOTMP TEST FOR MULTI-PROCESSOR   MPS1 00404017
         PUNCH '      INCLUDE CI535(IECIPRMP) '                    MPS1 00408017
         AGO   .NOTO4                                              MPS1 00412017
.NOTMP   ANOP                                                      MPS1 00416017
         PUNCH '     INCLUDE CI535(IECIPR16) '                     MLIB 00420017
.NOTO4   ANOP                                                           00440000
         AIF   (NOT &SGDCLSB(73) AND NOT &SGDCLSB(77)).NO34       21048 00442021
         PUNCH '      INCLUDE  CI505(IECTATTN)'  INCLUDE 34XX ATTN      00444021
.NO34    AIF   (NOT &SGDCLSB(71)).NO3211                                00445021
         PUNCH '      INCLUDE CI505(IECURATN)' INCLUDE 3211 ATTN A44146 00446021
.NO3211  ANOP                                                           00447021
.*1467001600,002200,002800,003400,004200                           MLIB 00450017
.*1385004000                                                       MPS1 00455017
         MEND                                                           00460000
