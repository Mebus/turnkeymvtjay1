         MACRO                                                          00020000
         SGIEA2SU                                                       00040000
         COPY  SGGBLPAK                                                 00060000
   LCLC  &STORAGE,&TIMER,&FLOATPT,&IQE,&SERR,&EXT(8),&TRACE,&TRSVCTB    00080000
         LCLC  &JST                                                J/ST 00090017
         LCLC  &PROTECT,&SLICE                                     MTS0 00100016
         LCLA  &MOD,&MACH,&SW1,&SW2,&SIZER                         MO17 00106017
         LCLC  &HIARCHY                                            MO17 00112017
         LCLC  &SMF                                               19018 00113019
         LCLC  &ATTACH                                             I250 00114019
         LCLC  &EMU                                                EMUL 00115018
.*1306009720,009850-010800,017080,017240-018000                    ISER 00118017
.*1306001200-001300,009660,016600-016800,018400                    MO17 00124017
.*1306019000                                                       MP65 00130017
.*  007000-007400                                                 19018 00133019
.*007900                                                         A26704 00136019
.*011800-012400,012800-013000,013400-013800                   BD A56865 00138021
&STORAGE SETC  'PRIM'                                                   00140000
&TIMER   SETC  'NOTIM'                                                  00160000
&FLOATPT SETC  'N'                                                      00180000
&IQE     SETC  'OTHR12'                                       BD A56865 00200021
&SERR    SETC  'SERP'                                                   00220000
&EXT(1)  SETC  'N'                                                      00240000
&EXT(2)  SETC  'N'                                                      00260000
&EXT(3)  SETC  'N'                                                      00280000
&EXT(4)  SETC  'N'                                                      00300000
&EXT(5)  SETC  'N'                                                      00320000
&EXT(6)  SETC  'N'                                                      00340000
&EXT(7)  SETC  'N'                                                      00360000
&EXT(8)  SETC  'N'                                                      00380000
&TRACE   SETC  'N'                                                      00400000
&TRSVCTB  SETC 'N'                                                      00420000
&PROTECT  SETC  'N'                                                     00440000
&SLICE   SETC  'NOSLICE'                                           MTS0 00450016
&JST     SETC  'N'                                                 J/ST 00452017
&SMF     SETC  'NSF'                                              19018 00452319
&ATTACH  SETC  'NOATH'                                             I250 00452619
&EMU     SETC  'EMUN'                                              EMUL 00453018
.****** SET MODEL NUMBER ********************************************** 00454017
&MOD   SETA  40+10*&SGCPUB(3)+25*&SGCPUB(4)+35*&SGCPUB(5)+51*&SGCPUB(6) 00456017
&MOD     SETA  &MOD+45*&SGCPUB(7)                                  R85  00458018
&MOD     SETA  &MOD+155*&SGCPUB(9)                                M4600 00459019
&MOD     SETA  &MOD+755*&SGCPUB(13)                           BD  21108 00459121
&MOD     SETA  &MOD+115*&SGCPUB(8)                                19E07 00459220
&MOD     SETA  &MOD+125*&SGCPUB(11)                               19E08 00459420
&MOD     SETA  &MOD+118*&SGCPUB(14)                            BD 21122 00462821
&MOD     SETA  &MOD+128*&SGCPUB(15)                            BD 21122 00464821
.* MODEL NUMBER HAS BEEN CHANGED FROM 297 TO 145                  M0132 00466421
&MOD     SETA  &MOD+105*&SGCPUB(10)                               20301 00469821
&MOD     SETA  &MOD+95*&SGCPUB(12)                                21054 00473221
.****** TEST FOR STORAGE CONFIGURATION ******************************** 00476621
         AIF   (&SGCPRGB(8) EQ 1).NEXT20                                00480000
         AIF   (&SGCPRGB(1) EQ 1).NEXT3                                 00500000
         AIF   (&SGCPRGB(2) NE 1).NEXT1                                 00520000
&STORAGE SETC  'SING'                                                   00540000
         AGO   .NEXT3                                                   00560000
.NEXT1   AIF   (&SGCPRGB(3) NE 1).NEXT2                                 00580000
&STORAGE SETC  'SHAR'                                                   00600000
         AGO   .NEXT3                                                   00620000
.NEXT2   ANOP                                                           00640000
&STORAGE SETC  'PART'                                                   00660000
.****** TEST FOR TIMER OPTIONS **************************************** 00680000
.NEXT3   AIF   (&SGSUPRB(19) NE 1).NEXT3A    JOBSTEP TIMING       19018 00687019
         AIF   (&SGCPRGB(4) NE 1).PASS       MFT                  19018 00694019
&JST     SETC  'Y'                           SET JOBSTEP PARM     19018 00701019
         AIF   (&SGSCHDB(7) NE 1).PASS       SMF                  19018 00708019
&SMF     SETC  'SMF'                         SET SMF PARM         19018 00715019
         AGO   .PASS                                              M4959 00722019
.NEXT3A  AIF   (&SGSUPRB(14) NE 1).NEXT4                          19018 00729019
.PASS    ANOP                                                     19018 00736019
&TIMER   SETC  'TTIME'                                            19018 00743019
         AGO   .NEXT4A                                            19018 00750019
.NEXT4   AIF   (&SGSUPRB(13) NE 1).NEXT4B                               00760000
&TIMER   SETC  'DTIME'                                                  00780000
.NEXT4A  ANOP                                                           00800000
&EXT(1)  SETC  'Y'                                                      00820000
.****** TEST FOR TRANSIENT SVC TABLE OPTION *************************** 00840000
.NEXT4B  AIF   (&SGSUPRB(3) NE 1).NEXT5                                 00860000
&TRSVCTB  SETC 'Y'                                                      00880000
.****** TEST FOR FLOATING POINT REGISTERS ***************************** 00900000
.NEXT5   AIF   (NOT(&SGCPUB(32) OR &SGCPUB(33))).NEXT6                  00920000
&FLOATPT SETC  'Y'                                                      00940000
.****** TEST FOR MACHINE SIZE ****************************************  00941017
.NEXT6   ANOP                                                      MO17 00942017
&SW1     SETA  20                                                  MO17 00943017
&SW2     SETA  32768                                               MO17 00944017
.MACHSZ  AIF   (&SGCPUB(&SW1) NE 1).INCRMT                         MO17 00945017
&MACH    SETA  (&SW2)                                              MO17 00946017
         AGO   .HIARY1                                             MO17 00947017
.INCRMT  AIF   ((&SW1) EQ 29).HIARY1                               MO17 00948017
&SW1     SETA  ((&SW1)+1)                                          MO17 00949017
&SW2     SETA  ((&SW2)+(&SW2))                                     MO17 00950017
         AGO   .MACHSZ                                             MO17 00951017
.****** TEST FOR HIERARCHY *******************************************  00952017
.HIARY1  AIF   (&SGCPRGB(11) NE 1).HIARY2                          MO17 00953017
&HIARCHY SETC  'Y'                                                 MO17 00954017
         AGO   .NEXT6A                                             MO17 00955017
.HIARY2  ANOP                                                      MO17 00956017
&HIARCHY SETC  'N'                                                 MO17 00957017
.****** TEST FOR SER/R LEVEL ****************************************** 00960000
.NEXT6A  ANOP                                                      MO17 00964017
&SIZER   SETA  &SGCPRGA(2)                                         MO17 00968017
         AIF   (&SGSUPRB(28) NE 1).NEXT6B                          ISER 00973017
&SERR    SETC  'MCH'                                                    00978015
.NEXT6B  ANOP                                                      MTS0 01081016
.*                                                                 MTS0 01082016
.*         TIME SLICING IS DEPENDENT ON THE PRESENCE OF THE        MTS0 01084016
.*   INTERVAL TIMER.  'GENERATE' WILL VALIDITY CHECK THIS          MTS0 01086016
.*   CONDITION.  GLOBAL SWITCH SGCPRGB(12) INDICATES THE           MTS0 01088016
.*   TIME-SLICING OPTION.                                          MTS0 01090016
.*                                                                 MTS0 01092016
         AIF   (&SGCPRGB(12) EQ 0).GOBY                            MTS0 01094016
&SLICE   SETC  'TIMESL'                                            MTS0 01096016
.GOBY    ANOP                                                      MTS0 01098016
.****** TEST FOR CONSOLE SWITCHING ************************************ 01100000
.NEXT7   AIF   ('&SGSCHDC(1)'(1,3) EQ '&SGSCHDC(2)'(1,3)).NEXT8         01120000
&EXT(2)  SETC  'Y'                                                      01140000
.****** TEST FOR ASYNCHRONOUS EXIT INFO ******************************* 01160000
.NEXT8   ANOP                                                 BD A56865 01180021
         AIF   (&SGSUPRB(15) EQ 1 OR &SGSUPRB(16) EQ 1).NEXT8B    65974 01260021
         AGO   .NEXT9                                         BD A56865 01320021
.NEXT8B  ANOP                                                 BD A56865 01400021
&IQE     SETC  'BOTH12'                                       BD A56865 01420021
.****** TEST FOR TRACE OPTION ***************************************** 01440000
.NEXT9   AIF   (&SGSUPRA(1) EQ 0).NEXT10                                01460000
&TRACE   SETC  'Y'                                                      01480000
.******* TEST FOR PROTECTION FEATURE ********************************** 01500000
.NEXT10  AIF  (&SGSUPRB(23) NE 1).NEXT11                                01520000
&PROTECT  SETC  'Y'                                                     01540000
.NEXT11  ANOP                                                           01541019
.******************* TEST FOR ASYNCHRONOUS ATTACH ********************* 01542019
         AIF   (NOT &SGSUPRB(37)).NEXT12B                          I250 01543019
&ATTACH  SETC  'ATTACH'                                            I250 01544019
.****** TEST FOR EMULATOR ********************************************* 01546018
.NEXT12B AIF  (NOT &SGMENTB(58)).NEXT12                            EMUL 01552019
         AIF   (NOT &SGCPUB(7)).NEXT12                             EMUL 01558018
&EMU     SETC  'EMUY'                                              EMUL 01564018
.NEXT12  ANOP                                                           01570018
&SGCTRLC(6) SETC 'IEAASU00'                                             01580000
         COPY  SGASMPAK                                                 01600000
         PUNCH '     IEAAIH &STORAGE,&IEAHIA,&IEALOA,&IEALMTA,&SERR,(&E*01620000
               XT(1),&EXT(2),&EXT(3),&EXT(4),&EXT(5),&EXT(6),&EXT(7),&E*01640000
               XT(8)),                   XXXXXXXX'               A73495 01650021
         PUNCH '               &TRACE,&PROTECT,&FLOATPT,&TRSVCTB,&MOD,&*01660017
               SIZER,&MACH,&HIARCHY,&TIMER,&SMF,&SGMENTB(68)'     19E01 01670020
         PUNCH '     IEAAPS &STORAGE,&TIMER,&FLOATPT,&IQE,&TRACE,&SLICEG01680017
               ,&JST,&EMU,&ATTACH,&SMF,&SERR'                     19E02 01690020
         MEXIT                                                          01700000
.NEXT20  AIF   (&SGSUPRB(28) NE 1).NEXT24                          ISER 01708017
&SERR    SETC  'MCH'                                                    01716015
.NEXT24  ANOP                                                           01820000
&SGCTRLC(6) SETC  'IEAASU00'                                            01860000
         COPY  SGASMPAK                                                 01880000
 PUNCH ' IEAQFX &MOD,&SERR,&SGCPRGB(13),&SGMENTB(68),GENCODE=YES' 19E01 01900020
         MEND                                                           01920000
