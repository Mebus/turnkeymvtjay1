         MACRO                                                          00020000
         SVCTABLE                                                       00040000
         COPY  SGGBLPAK                                                 00060000
         LCLA  &I,&J,&K,&L,&M,&N,&P,&Q                                  00080000
         LCLC  &C,&D,&F                                                 00100000
.*                                     LAST UPDATE 20 MARCH 1972        00120000
.L1      AIF   (N'&SYSLIST EQ 0).M1  ERROR EXIT FOR NO OPERANDS.        00140000
&I       SETA  &I+1  SET INDEX TO OPERAND.                              00160000
         AIF  (K'&SYSLIST(&I) GT 13 OR K'&SYSLIST(&I) LT 11).M2         00180000
&J       SETA  0  SET INDEX TO CHARACTER OF OPERAND.                    00200000
.L2      AIF   (&J+1 GT K'&SYSLIST(&I)).M2  ERROR END OF OPERAND.       00220000
&J       SETA  &J+1  BUMP CHARACTER INDEX.                              00240000
         AIF   ('&SYSLIST(&I)'(&J,1) NE '-').L2  LOOK FOR MINUS.        00260000
&N       SETA  &J+1  SET POINTER TO FIRST SVC CHARACTER.                00280000
.L3      AIF   (&J+1 GT K'&SYSLIST(&I)).M2  ERROR END OF OPERAND.       00300000
&J       SETA  &J+1  BUMP CHARACTER INDEX.                              00320000
         AIF   ('&SYSLIST(&I)'(&J,1) GE '0' AND '&SYSLIST(&I)'(&J,1) LE100340000
                '9').L3  GO BACK IF CHARACTER IN RANGE.                 00360000
         AIF   ('&SYSLIST(&I)'(&J,1) NE '-').M2  ERROR INVALID CHAR.    00380000
         AIF  (&J-&N GT 3).M2             SVC NUMBER GT 3 CHARS.        00400000
&C       SETC  '&SYSLIST(&I)'(&N,&J-&N)   SET SVC NO. IN C BOX.         00420000
&K       SETA  &C  PUT SVC NUMBER IN A BOX.                             00440000
         AIF  (&K LT 1 OR &K GT 255).M3   SVC OUT OF RANGE              00460000
.R4      ANOP                                                           00480000
&P       SETA  &J+2  SET POINTER TO FIRST TYPE CHARACTER.               00500000
&J       SETA  &J+1  BUMP CHARACTER INDEX.                              00520000
.L4      AIF   (&J+1 GT K'&SYSLIST(&I)).M2  ERROR END OF OPERAND.       00540000
&J       SETA  &J+1 BUMP CHARACTER INDEX.                               00560000
         AIF   ('&SYSLIST(&I)'(&J,1) GE '0' AND '&SYSLIST(&I)'(&J,1) LE100580000
                '5').L4  GO BACK IF CHARACTER IN RANGE.                 00600000
         AIF   (&J-&P EQ 0).L4A           ERROR INVALID CHAR.           00612000
         AIF   ('&SYSLIST(&I)'(&J,1) NE '-').M2  ERROR INVALID CHAR.    00620000
.L4A     ANOP                                                           00630000
         AIF  (&J-&P GT 1).M4                                           00640000
&D       SETC  '&SYSLIST(&I)'(&P,&J-&P)   SET TYPE NO. IN C BOX.        00660000
         AIF   (&J-&P EQ 0).M4            ERROR INVALID CHAR.           00670000
&L       SETA  &D  PUT TYPE NUMBER IN A BOX.                            00680000
.*                                      ONLY SGIEI2SV CAN SPECIFY TYPE5 00690020
         AIF   (&L EQ 5 AND NOT (&SGMENTB(88) OR &SGMENTB(69))).M4  SO  00700020
.*                                      TEST FOR GENERATE OR GENTSO ENY 00710020
         AIF  (&L LT 1 OR &L GT 5).M4   TYPE OUT OF RANGE               00720000
.R5      ANOP                                                           00740000
         AIF   (&J+2 GT K'&SYSLIST(&I)).M2  ERROR NOT ENOUGH CHAR.      00760000
&Q       SETA  &J+2  SET POINTER TO FIRST SAVE CHARACTER.               00780000
&J       SETA  &J+1  BUMP CHARACTER INDEX.                              00800000
.L5      ANOP       ENTRY FOR RETURN FROM SCAN.                         00820000
&J       SETA  &J+1  BUMP CHARACTER INDEX.                              00840000
         AIF   (&J GT K'&SYSLIST(&I)).L6  TRANSFER END OF SCAN.         00860000
         AIF   ('&SYSLIST(&I)'(&J,1) GE '0' AND '&SYSLIST(&I)'(&J,1) LE100880000
                '6').L5  GO BACK IF CHARACTER IN RANGE.                 00882000
.L6      AIF  (&J-&Q GT 1).M5                                           00920000
&F  SETC  '&SYSLIST(&I)'(&Q,1)     SET SAVE INTO C BOX                  00940000
         AIF  (&J-&Q EQ 0).M5      ERROR IF EQ TO ZERO                  00950000
&M       SETA  &F  PUT SAVE NUMBER IN A BOX.                            00960000
  AIF  (&M LT 0 OR &M GT 6).M5   SAVE OUT OF RANGE                      00980000
         AIF   (&N-4 NE 1).M2  ERROR NOT ENOUGH CHAR. BEFORE SVC NO.    01000000
  AIF  ('&SYSLIST(&I)'(&N-4,3) NE 'SVC').M6  NO SVC                     01020000
  AIF  ('&SYSLIST(&I)'(&P-1,1) NE 'T').M7    NO T                       01040000
  AIF  ('&SYSLIST(&I)'(&Q-1,1) NE 'S').M8   NO S                        01060000
  AIF (&L EQ 5 AND &M GE 1).M9                                          01080000
  AIF (&L EQ 1 AND &M GE 1).M9                                          01100000
         AIF   (&SGSVCAB(&K) OR &SGSVCBB(&K) OR &SGSVCCB(&K)).P2 ERR.   01120000
&SGSVCAB(&K) SETB (&L EQ 5 OR &L EQ 4)  SET TYPE FOUR BIT.              01140000
&SGSVCBB(&K) SETB (&L EQ 3 OR &L EQ 2)  SET TYPE TWO BIT.               01160000
&SGSVCCB(&K) SETB (&L EQ 5 OR &L EQ 3 OR &L EQ 1)  SET TYPE ONE BIT.    01180000
  AIF  (&L EQ 5 OR &L EQ 1).L8                                          01200000
&SGSVCDB(&K) SETB (&M EQ 6 OR &M EQ 5 OR &M EQ 4 OR &M EQ 3)  FOUR BIT. 01220000
&SGSVCEB(&K) SETB (&M EQ 6 OR &M EQ 5 OR &M EQ 2 OR &M EQ 1)  TWO BIT.  01240000
&SGSVCFB(&K) SETB (&M EQ 6 OR &M EQ 4 OR &M EQ 2 OR &M EQ 0)  ONE BIT.  01260000
.L8      AIF   (&I+1 LE N'&SYSLIST).L1  GO BACK FOR NEXT OPERAND.       01280000
         MEXIT                                                          01300000
.M1 MNOTE 5,'* * * IEISVC101 NO SVC SPECIFIED'                          01320000
&SGQUITB  SETB  1                                                       01340000
         MEXIT                                                          01360000
.M2 MNOTE 5,'* * * IEISVC102 OPERAND &I-VALUE &SYSLIST(&I) INVALID'     01380000
&SGQUITB  SETB  1                                                       01400000
         AGO  .L8                                                       01420000
.M3 MNOTE 5,'* * * IEISVC103 OPERAND &I-SVC VALUE &C INVALID'           01440000
&SGQUITB  SETB  1                                                       01460000
         AGO  .R4                                                       01480000
.M4 MNOTE 5,'* * * IEISVC104 OPERAND &I-TYPE VALUE  INVALID'            01500000
&SGQUITB  SETB  1                                                       01520000
&J       SETA  &J+1                                                     01530000
         AGO  .R5                                                       01540000
.M5 MNOTE 5,'* * * IEISVC105 OPERAND &I-SAVE VALUE &F INVALID'          01560000
&SGQUITB  SETB  1                                                       01580000
         AGO  .L8                                                       01600000
.M6 MNOTE 5,'* * * IEISVC106 OPERAND &I-SVC NOT SPECIFIED'              01620000
&SGQUITB  SETB  1                                                       01640000
         AGO  .L8                                                       01660000
.M7 MNOTE 5,'* * * IEISVC107 OPERAND &I-TYPE NOT SPECIFIED'             01680000
&SGQUITB  SETB  1                                                       01700000
         AGO  .L8                                                       01720000
.M8 MNOTE 5,'* * * IEISVC108 OPERAND &I-SAVE NOT SPECIFIED'             01740000
&SGQUITB  SETB  1                                                       01760000
         AGO  .L8                                                       01780000
.M9 MNOTE 5,'* * * IEISVC109 OPERAND &I-TYPE AND SAVE INCOMPATIBLE'     01800000
&SGQUITB  SETB  1                                                       01820000
         AGO  .L8                                                       01840000
.P2 MNOTE 5,'* * * IEISVC110 OPERAND &I-SVC &K REPEATED'                01860000
&SGQUITB  SETB  1                                                       01880000
         AGO  .L8                                                       01900000
         MEND                                                           01920000
