         MACRO                                                          00020000
         CONVERT &VALUE=,&DIGITS=,&TO=                                  00040000
         COPY  SGGBLPAK                                                 00060000
       LCLA  &A,&B,&C(8),&D                                             00080000
         LCLC  &CA                                                      00100000
&EBCDIC  SETC   ''                                                      00120000
&HEX     SETA  0                                                        00140000
         AIF   ('&TO' EQ 'HEX').TOHEX                                   00160000
         AIF   ('&TO' EQ 'EBCDIC').TOEBCDC                              00180000
.PARAERR MNOTE 0,'* * * SGERR031      ''TO'' VALUE NOT SPECIFIED'       00200000
&SGQUITB SETB  1                                                        00220000
         AGO   .MEND                                                    00240000
.TOEBCDC ANOP                                                           00260000
&A       SETA  &VALUE                                                   00280000
&B   SETA  &DIGITS+1                                                    00300019
&D       SETA  &A/65536            SHIFT RIGHT 16 POSITIONS.            00320000
&C(8)    SETA  &D/4096             SHIFT RIGHT 12 POSITIONS.            00340000
&C(7)    SETA  &D/256-16*&C(8)  SHIFT D RIGHT 8 & C LEFT 4-GET 4 ZEROS. 00360000
&C(6)    SETA  &D/16-&D/256*16  SHIFT D RIGHT 8 & 4MORE TO GET 4 0'S.   00380000
&C(5)    SETA  &D-&D/16*16                                              00400000
&C(4)    SETA  &A/4096-16*&D                                            00420000
&C(3)    SETA  &A/256-&A/4096*16                                        00440000
&C(2)    SETA  &A/16-&A/256*16                                          00460000
&C(1)    SETA  &A-&A/16*16                                              00480000
.BACK    AIF   (&B EQ 1).MEND                                           00500000
&B       SETA  &B-1                                                     00520000
&EBCDIC  SETC  '&EBCDIC'.'0123456789ABCDEF'(&C(&B)+1,1)                 00540000
         AGO   .BACK                                                    00560000
.TOHEX   AIF   (&B EQ K'&VALUE).MEND                                    00580019
&B       SETA  &B+1                                                     00600000
&CA      SETC  '&VALUE'(&B,1)                                           00620000
         AIF   ('&CA' GE '0').ZERTO9X                                   00640000
         AIF   ('&CA' EQ 'A').AX                                        00660000
         AIF   ('&CA' EQ 'B').BX                                        00680000
         AIF   ('&CA' EQ 'C').CX                                        00700000
         AIF   ('&CA' EQ 'D').DX                                        00720000
         AIF   ('&CA' EQ 'E').EX                                        00740000
         AIF   ('&CA' EQ 'F').FX                                        00760000
.INVALID MNOTE 0,'* * * SGERR030       VALUE=&VALUE NOT CONVERTABLE'    00780000
&SGQUITB SETB  1                                                        00800000
         AGO   .MEND                                                    00820000
.ZERTO9X ANOP                                                           00840000
&HEX     SETA  &HEX*16+&CA                                              00860000
         AGO   .TOHEX                                                   00880000
.AX      ANOP                                                           00900000
&HEX     SETA  &HEX*16+X'A'                                             00920000
         AGO   .TOHEX                                                   00940000
.BX      ANOP                                                           00960000
&HEX     SETA  &HEX*16+X'B'                                             00980000
         AGO   .TOHEX                                                   01000000
.CX      ANOP                                                           01020000
&HEX     SETA  &HEX*16+X'C'                                             01040000
         AGO   .TOHEX                                                   01060000
.DX      ANOP                                                           01080000
&HEX     SETA  &HEX*16+X'D'                                             01100000
         AGO   .TOHEX                                                   01120000
.EX      ANOP                                                           01140000
&HEX     SETA  &HEX*16+X'E'                                             01160000
         AGO   .TOHEX                                                   01180000
.FX      ANOP                                                           01200000
&HEX     SETA  &HEX*16+X'F'                                             01220000
         AGO   .TOHEX                                                   01240000
.MEND  MEND                                                             01260000
