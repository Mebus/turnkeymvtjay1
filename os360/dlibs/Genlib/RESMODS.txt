         MACRO                                                          00020000
        RESMODS  &PDS=,&MEMBERS=                                        00040000
         COPY  SGGBLPAK                                                 00060000
         LCLA  &I,&J                                                    00080000
         AIF   (&SGMENTB(37)).M1                                        00100000
&SGMENTB(37) SETB 1                                                     00120000
&I       SETA  1                                                        00140000
  AIF  (N'&PDS EQ 0).M2                                                 00160000
  AIF  (N'&PDS GT 1 OR K'&PDS LE 5 OR K'&PDS GE 14).M3                  00180000
  AIF  ('&PDS'(1,5) NE 'SYS1.').M3                                      00200000
&SGRESMC(&I)  SETC  '&PDS'(6,K'&PDS-5)                                  00220000
.L2      AIF   (N'&MEMBERS LT 1).N1                                     00240000
  AIF  (N'&MEMBERS GT 10).N2                                            00260000
&J       SETA  N'&MEMBERS                                               00280000
    MNOTE *,'                LISTED MEMBERS OF &PDS ARE RESIDENT'       00300000
.L4      AIF   (&I GT &J).L51                                           00320000
         AIF   (K'&MEMBERS(&I) LT 1 OR K'&MEMBERS(&I) GT 8).N3          00340000
&SGRESMC(&I+1) SETC '&MEMBERS(&I)'(1,K'&MEMBERS(&I))                    00360000
    MNOTE *,'         &MEMBERS(&I)'                                     00380000
.L5      ANOP                                                           00400000
&I       SETA  &I+1                                                     00420000
         AGO   .L4                                                      00440000
.L51     ANOP                                                           00460000
   MEXIT                                                                00480000
.M1 MNOTE 5,'* * * IEIRES101 RESMODS MACRO PREVIOUSLY USED'             00500000
&SGQUITB  SETB  1                                                       00520000
         MEXIT                                                          00540000
.M2 MNOTE 5,'* * * IEIRES102 PDS VALUE NOT SPECIFIED'                   00560000
&SGQUITB SETB 1                                                         00580000
   AGO .L2                                                              00600000
.M3 MNOTE 5,'* * * IEIRES103 PDS VALUE &PDS INVALID'                    00620000
&SGQUITB  SETB 1                                                        00640000
  AGO  .L2                                                              00660000
.N1 MNOTE 5,'* * * IEIRES104 MEMBERS VALUE NOT SPECIFIED'               00680000
&SGQUITB  SETB  1                                                       00700000
    MEXIT                                                               00720000
.N2 MNOTE 5,'* * * IEIRES105 MAXIMUM NUMBER OF MEMBERS EXCEEDED'        00740000
&SGQUITB  SETB  1                                                       00760000
&J  SETA  10                                                            00780000
   AGO  .L4                                                             00800000
.N3 MNOTE 5,'* * * IEIRES106 MEMBERS VALUE &MEMBERS(&I) INVALID'        00820000
&SGQUITB  SETB  1                                                       00840000
   AGO   .L5                                                            00860000
         MEND                                                           00880000
