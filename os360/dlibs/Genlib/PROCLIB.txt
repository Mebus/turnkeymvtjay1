         MACRO                                                          00020000
         PROCLIB &UNIT=,&VOLNO=                                         00040000
         COPY  SGGBLPAK                                                 00060000
         AIF   (NOT &SGMENTB(44)).PROC1                                 00080000
    MNOTE 5,'* * * IEIPRL101 PROCLIB MACRO PREVIOUSLY USED'             00100000
         AGO   .PRQUIT                                                  00120000
.PROC1   ANOP                                                           00140000
&SGMENTB(44)   SETB  1                                                  00160000
         AIF   (N'&UNIT EQ N'&VOLNO).PROC2                              00180000
    MNOTE 5,'* * * IEIPRL102 UNIT VALUE OR VOLNO VALUE MISSING'         00200000
         AGO   .PRQUIT                                                  00220000
.PROC2 AIF  ('&UNIT' EQ '').M1                                          00240000
         AIF   (K'&UNIT LE 8).PROC3                                     00260000
    MNOTE 5,'* * * IEIPRL103 UNIT VALUE &UNIT INVALID'                  00280000
&SGQUITB  SETB  1                                                       00300000
.PROC3   AIF   (K'&VOLNO LE 6).PROC4                                    00320000
    MNOTE 5,'* * * IEIPRL104 VOLNO VALUE &VOLNO INVALID'                00340000
         AGO   .PRQUIT                                                  00360000
.PROC4   ANOP                                                           00380000
&SGCTRLC(26)   SETC   '&VOLNO'                                          00400000
&SGCTRLC(25)   SETC   '&UNIT'                                           00420000
         MEXIT                                                          00440000
.M1 MNOTE *,'      PROCESS LIBRARY WILL RESIDE ON SYSRES'               00460000
         MEXIT                                                          00480000
.PRQUIT  ANOP                                                           00500000
&SGQUITB SETB  1                                                        00520000
         MEND                                                           00540000
