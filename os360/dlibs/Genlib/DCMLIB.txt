         MACRO                                                          00050021
         DCMLIB &UNIT=,&VOLNO=                                          00100021
         COPY  SGGBLPAK                                                 00150021
         AIF   (NOT &SGMENTB(89)).A                                     00200021
    MNOTE 5,'* * * IEIDCM100 DCMLIB MACRO PREVIOUSLY USED'              00250021
         AGO   .QUIT                                                    00300021
.A       ANOP                                                           00350021
&SGMENTB(89) SETB  1                                                    00400021
         AIF   (N'&UNIT EQ N'&VOLNO).B                                  00450021
    MNOTE 5,'* * * IEIDCM200 UNIT VALUE OR VOLNO VALUE MISSING'         00500021
         AGO   .QUIT                                                    00550021
.B       AIF   ('&UNIT' NE '').C                                        00600021
    MNOTE *,'     DCM LIBRARY WILL RESIDE ON LNKVOL'                    00650021
         AGO   .MEND                                                    00700021
.C       AIF   (K'&UNIT LE 8).D                                         00750021
    MNOTE 5,* * * IEIDCM300 UNIT VALUE &UNIT INVALID'                   00800021
         AGO   .QUIT                                                    00850021
.D       AIF   (K'&VOLNO LE 6).E                                        00900021
    MNOTE 5,'* * * IEIDCM400 VOLNO VALUE &VOLNO INVALID'                00950021
         AGO   .QUIT                                                    01000021
.E       ANOP                                                           01050021
&SGCTRLC(65) SETC '&UNIT'                                               01100021
&SGCTRLC(66) SETC '&VOLNO'                                              01150021
   MNOTE *,'      DCMLIB WILL RESIDE ON VOLUME &VOLNO'                  01200021
         AGO   .MEND                                                    01250021
.QUIT    ANOP                                                           01300021
&SGQUITB SETB  1                                                        01350021
.MEND    MEND                                                           01400021
