         MACRO                                                          00020000
          TELCMLIB  &UNIT=,&VOLNO=                                      00040000
         COPY  SGGBLPAK                                                 00060000
         AIF   (NOT &SGMENTB(48)).TEL1                                  00080000
    MNOTE 5,'* * * IEITEL101 TELCMLIB MACRO PREVIOUSLY USED'            00100000
         AGO   .TEQUIT                                                  00120000
.TEL1    ANOP                                                           00140000
&SGMENTB(48) SETB  1                                                    00160000
         AIF   (N'&UNIT EQ N'&VOLNO).TEL2                               00180000
    MNOTE 5,'* * * IEITEL102 MISSING OPERAND IN TELCMLIB'               00200000
         AGO   .TEQUIT                                                  00220000
.TEL2    AIF   ('&UNIT' EQ '').TELRES                                   00240000
         AIF   (K'&UNIT LE 8).TEL3                                      00260000
    MNOTE 5,'* * * IEITEL103 &UNIT IS INVALID TELCMLIB UNIT NAME'       00280000
         AGO   .TEQUIT                                                  00300000
.TEL3    AIF   (K'&VOLNO LE 6).TEL4                                     00320000
    MNOTE 5,'* * * IEITEL104 &VOLNO IS INVALID VOLUME '                 00340000
         AGO   .TEQUIT                                                  00360000
.TEL4    ANOP                                                           00380000
&SGCTRLC(33)  SETC  '&UNIT'                                             00400000
&SGCTRLC(34)  SETC  '&VOLNO'                                            00420000
         MEXIT                                                          00440000
.TELRES  MNOTE *,'   TELECOMMUNICATION LIBRARY WILL RESIDE ON SYSRES'   00460000
         MEXIT                                                          00480000
.TEQUIT  ANOP                                                           00500000
&SGQUITB SETB  1                                                        00520000
         MEND                                                           00540000
