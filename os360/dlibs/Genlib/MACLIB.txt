         MACRO                                                          00020000
         MACLIB  &UNIT=,&VOLNO=,&EXCLUDE=                               00040017
         COPY  SGGBLPAK                                                 00060000
         LCLA  &A                                                       00066017
         LCLC  &C                                                       00072017
         AIF   (NOT &SGMENTB(42)).MAC1                                  00080000
    MNOTE 5,'* * * IEIMAL101 MACLIB MACRO PREVIOUSLY USED'              00100000
         AGO  .MAQUIT                                                   00120000
.MAC1    ANOP                                                           00140000
&SGMENTB(42)   SETB  1                                                  00160000
         AIF   (N'&UNIT EQ N'&VOLNO).MAC2                               00180000
    MNOTE 5,'* * * IEIMAL102 UNIT VALUE OR VOLNO VALUE MISSING'         00200000
&SGQUITB  SETB  1                                                       00210017
         AGO   .MAC5                                                    00220017
.MAC2  AIF  ('&UNIT' EQ '').M1                                          00240000
         AIF   (K'&UNIT LE 8).MAC3                                      00260000
    MNOTE 5,'* * * IEIMAL103 UNIT VALUE &UNIT INVALID'                  00280000
&SGQUITB  SETB  1                                                       00300000
.MAC3    AIF   (K'&VOLNO LE 6).MAC4                                     00320000
    MNOTE 5,'* * * IEIMAL104 VOLNO VALUE &VOLNO INVALID'                00340000
&SGQUITB  SETB  1                                                       00350017
         AGO   .MAC5                                                    00360017
.MAC4    ANOP                                                           00380000
&SGCTRLC(15)   SETC   '&UNIT'                                           00400000
&SGCTRLC(16)   SETC   '&VOLNO'                                          00420000
         AGO   .MAC5                                                    00440017
.M1 MNOTE *,'      MACRO LIBRARY WILL RESIDE ON SYSRES'                 00460000
.MAC5    AIF   (N'&EXCLUDE LE 8).MAC6                                   00461000
    MNOTE 5,'* * * IEIMAL105 EXCLUDE VALUE &EXCLUDE INVALID'            00462017
         AGO   .MAQUIT                                                  00463017
.MAC6    AIF   (&A NE N'&EXCLUDE).MAC7                                  00464017
         MEXIT                                                          00465017
.MAC7    ANOP                                                           00466017
&A       SETA  &A+1                                                     00467017
&C       SETC  '&EXCLUDE(&A)'                                           00468017
         AIF   ('&C' NE 'BTAM').MAC8                                    00469017
&SGMALB(1)  SETB  1                                                     00470017
         AGO   .M2                                                      00471017
.MAC8    AIF   ('&C' NE 'QTAM').MACA                                    00472021
&SGMALB(2)  SETB  1                                                     00473017
         AGO   .M2                                                      00474017
.MACA    AIF   ('&C' NE 'GPS').MACB                                     00478019
&SGMALB(4)  SETB  1                                                     00479017
         AGO   .M2                                                      00479219
.MACB    AIF   ('&C' NE 'OCR').MACC                                     00479420
&SGMALB(5)     SETB 1                                                   00479619
         AGO   .M2                      GO INFORM USER                  00479720
.MACC    AIF   ('&C' NE 'TSO').MACD       TEST EXCLUDE=(TSO)            00479820
&SGMALB(7)     SETB 1                   YES - SET SWITCH                00479920
 AGO .M2                                                                00480120
.MACD AIF ('&C' NE 'TCAM').MACE                                         00480300
&SGMALB(6) SETB 1                                                       00480520
         AGO   .M2                                                      00480600
.MACE    AIF   ('&C' NE 'FDM').M3                                       00483300
&SGMALB(10) SETB 1                                                      00485300
.M2 MNOTE *,'      &C MACROS TO BE EXCLUDED FROM THE MACLIB'            00486500
         AGO   .MAC6                                                    00489200
.M3 MNOTE 5,'* * * IEIMAL106 EXCLUDE VALUE &C INVALID'                  00491900
&SGQUITB SETB  1                                                        00494600
         AGO   .MAC6                                                    00497300
.MAQUIT  ANOP                                                           00500000
&SGQUITB SETB  1                                                        00520000
         MEND                                                           00540000
