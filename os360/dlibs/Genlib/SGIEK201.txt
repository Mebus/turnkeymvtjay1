         MACRO                                                          02000014
         SGIEK201                                                       04000014
.*       140000-320000                                          LL45297 05000021
.*             SGIEK201 IS A FORTRAN H SYSGEN MACRO                     06000014
         COPY  SGGBLPAK            DEFINE GLOBALS FOR SYSGEN            08000014
         LCLA  &OPT,&LINECNT,&STORMAP,&TYPERUN,&PUNCH,&OBJLIST,&SORCODE*10000014
               ,&SORLST,&OBJID,&SOREDIT,&SORXREF                        12000014
&OPT     SETA  &SGFORTA(1)              OPTIMIZATION LEVEL FOR COMPILER 34000014
&LINECNT SETA  &SGFORTA(6)              LINE COUNT FOR COMPILER         36000014
.STORMAP AIF   (&SGFORTB(23)).TYPERUN   STORMAP OPTION TEST             38000014
&STORMAP SETA  1                  MAP OPTION                            40000014
.TYPERUN AIF   (&SGFORTB(25)).PUNCH     LOAD OPTION TEST                42000014
&TYPERUN SETA  1                  LOAD DEFAULT OPTION                   44000014
.PUNCH   AIF   (&SGFORTB(19)).OBJLIST   PUNCH OPTION TEST               46000014
&PUNCH   SETA  1                  DECK OPTION                           48000014
.OBJLIST AIF   (NOT &SGFORTB(28)).SORCODE OBJLIST TEST                  50000014
&OBJLIST SETA  1                  LIST OPTION                           52000014
.SORCODE AIF   (&SGFORTB(27)).SORLIST   SORCODE OPTION TEST             54000014
&SORCODE SETA  1                  BCD OPTION                            56000014
.SORLIST AIF   (&SGFORTB(21)).OBJCTID                                   58000014
&SORLST  SETA  1                                                        60000014
.OBJCTID AIF   (&SGFORTB(29)).SOREDIT                                   62000014
&OBJID   SETA  1                                                        64000014
.SOREDIT AIF   (&SGFORTB(44)).SORXREF                                   66000014
&SOREDIT SETA  1                                                        68000014
.SORXREF AIF   (NOT &SGFORTB(49)).JCL                                   70000014
&SORXREF SETA  1                                                        72000014
.JCL     ANOP                                                           74000014
&SGCTRLC(6) SETC 'IEK201'                                               76000014
         COPY  SGASMPAK            PROVIDE JCL                          78000014
         PUNCH '         SGIEK001   OPTN=&OPT,MAP=&STORMAP,TYPE=&TYPERU*80000014
               N,PNCH=&PUNCH,OBLST=&OBJLIST,CODE=&SORCODE,         *'   82000014
         PUNCH '               SRLST=&SORLST,LINCNT=&LINECNT,SZE=&SGFOR*84000014
               TA(5),ID=&OBJID,EDIT=&SOREDIT,XREF=&SORXREF'             86000014
         PUNCH '         END'                                           88000014
         PUNCH '/*'                                                     90000014
         MEND                                                           92000014
