         MACRO                                                          03000017
         SGIHJ500                                                       06000017
.* 120000-150000,580000-590000                                 IEBHDCPY 06500019
.* 210000-270000,340000-540000,690000                             M0401 07000018
.* 698000                                                         M0041 08000018
.*                                                                M0122 08500018
.*500000,694000-714000                                            19874 08700019
.*       RELEASE 21.8 CHANGES                                           08710000
.*0000102000                                                    SA73050 08720000
.*       RELEASE 21.7 CHANGES                                           08750021
.*0000                                                          SA57979 08800021
.*0000                                                          SA50338 08850021
.*0000                                                           S21940 08900000
         COPY  SGGBLPAK                                                 09000017
 PUNCH ' COPY OUTDD=SVCLIB,INDD=CQ548'                          SA73050 09050021
 PUNCH ' SELECT MEMBER=(IGC0J05B)'                              SA73050 09100021
 PUNCH ' COPY OUTDD=SVCLIB,INDD=DM508'                                  09600021
 PUNCH ' SELECT MEMBER=(IGC0506C,IGC0N06C,IGC0G95B,IGC0G05B)'   SA73050 10200000
 PUNCH ' SELECT MEMBER=(IGC0H05B,IGC0I05B,IGC0K05B,IGC0L05B,IGC0M05B)'  10800021
 PUNCH ' SELECT MEMBER=(IGC0M95B,IGC0K95B,IGC0L95B)'            SA50338 10850000
 PUNCH ' SELECT MEMBER=(IGC0N05B,IGC0P05B,IGC0S05B,IGC0W05B,IGC0Q05B)'  11400021
 PUNCH ' SELECT MEMBER=(IGC0N95B)'                              SA57979 11450021
 PUNCH ' SELECT MEMBER=(IGC0R05B,IGC0T05B,IGC0U05B)'                    12000021
         PUNCH ' COPY OUTDD=SVCLIB,INDD=CI505'                 IEBHDCPY 13000019
 PUNCH ' SELECT MEMBER=((IHJACP00,IGC0006C),(IHJACP01,IGC0106C))'       18000017
 PUNCH ' SELECT MEMBER=((IHJACP02,IGC0206C),(IHJACP20,IGC0A06C))' M0401 22000018
 PUNCH ' SELECT MEMBER=((IHJACP50,IGC0Q06C),(IHJARS00,IGC0105B))' M0401 26000018
 PUNCH ' SELECT MEMBER=((IHJACP25,IGC0D06C),(IHJACP70,IGC0S06C))'       30000017
 PUNCH ' SELECT MEMBER=((IEFVSMBR,IGC0005B))'                     M0122 32000018
 PUNCH ' SELECT MEMBER=((IHJARS01,IGC0205B),(IHJARS60,IGC0V05B))' M0401 35000018
 AIF (&SGSCHDB(3)).MVT                                            M0401 40000018
 PUNCH ' SELECT MEMBER=((IHJACP30,IGC0F06C),(IHJARS20,IGC0505B))' M0401 45000018
 PUNCH ' SELECT MEMBER=((IHJARS21,IGC0605B))'                     19874 49000019
 MEXIT                                                            19874 53000019
.MVT ANOP                                                               57000017
         PUNCH ' COPY OUTDD=SVCLIB,INDD=CI535'                 IEBHDCPY 58000019
 PUNCH ' SELECT MEMBER=((IHJQCP30,IGC0F06C),(IHJQCP31,IGC0G06C))'       60000017
 PUNCH ' SELECT MEMBER=((IHJQCP32,IGC0H06C),(IHJQRS20,IGC0505B))'       63000017
 PUNCH ' SELECT MEMBER=((IHJQRS21,IGC0605B),(IHJQRS22,IGC0705B))'       66000017
 PUNCH ' SELECT MEMBER=((IHJQRS23,IGC0805B),(IHJQRS24,IGC0905B))'       69000017
         MEND                                                           72000017
