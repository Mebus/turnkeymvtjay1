         MACRO                                                          00400018
         SGIEA2NP                                                       00800018
         COPY  SGGBLPAK                                                 01200018
         LCLA  &STORAGE                                           20066 01500020
         LCLA  &VOLTSZA                                            0019 01800018
         LCLB  &EMUL85B                                          A33578 01900020
         LCLB  &PWARN                                          @S21167P 01950021
         LCLC  &MODL,&SIZL,&SERL                                   ISER 02000018
         LCLC  &MODL2,&SIZL2,&SERL2                                ISER 02400018
         LCLC  &TEMPMOD,&TEMPSIZ,&TEMPSER                          ISER 02800018
         LCLC  &MOD(20),&SIZE(15),&SR(5),&MODL3(20)               20065 03100020
         LCLC  &HARDCPC                                            MCS0 03400018
         LCLC  &MODID,&BCT(20),&TIMBCTC                           20066 03500020
         LCLA  &X1,&X2                                             ISER 03600018
.*3131000200-006000                                                INSS 03900018
.*3131                                                             MCS0 04200018
.*3131                                                             MD85 04500018
.*3131                                                             LPA1 04800018
.*3131                                                             0019 05100018
&MOD(1)  SETC  '30'                    CPU MODEL LIST              ISER 05600018
&MOD(2)  SETC  '40'                                                ISER 06000018
&MOD(3)  SETC  '50'                                                ISER 06400018
&MOD(4)  SETC  '65'                                                ISER 06800018
&MOD(5)  SETC  '75'                                                ISER 07200018
&MOD(6)  SETC  '91'                                                ISER 07600018
&MOD(7)  SETC  '85'                                                MD85 07800018
&MOD(9)  SETC  'C3'                                                     07900019
&MOD(8)  SETC  '15'           MOD A48 OR 155                      20065 08000020
&MOD(10) SETC  '14'                MOD S68 6R-145                 20301 08100020
&MOD(11) SETC  '16'           MOD 553 OR 165                      20066 08200020
&MOD(12) SETC  '13'                     MODEL C86 (135)           21054 08250021
&MOD(13) SETC  'C3'                MODEL 370/195               @SA65200 08255021
&MOD(14)  SETC  '58'               MOD 158 OLYMPUS            BH S21122 08260021
&MOD(15)  SETC  '68'               MOD 168 PISCES             BH S21122 08280021
&MOD(20) SETC  '00'                                               20065 08300020
.* SET PRIMARY MODEL                                               ISER 08400018
.M1      ANOP                                                      ISER 08800018
&X1      SETA  &X1+1                                               ISER 09200018
         AIF   (&SGCPUB(&X1) NE 1).M1                             20065 10000020
&MODL    SETC  '&MOD(&X1)'                                         ISER 10800018
         AIF   (&X1 GT 7).ID195                                         10840019
&MODID   SETC  '&MOD(20)'(1,2).'&MOD(&X1)'(1,2)                   20301 10880020
         AGO   .NOID                                                    10920019
.ID195   ANOP                                                           10960019
.********                                                ******** 20065 10970020
.*          SET THREE CHARACTER MODEL ID FOR 'MODID' PARAMETER  * 20065 10980020
.********                                                ******** 20065 10990020
&MODL3(8) SETC '0155'                                             20065 11000020
&MODL3(9) SETC '0195'                                             20065 11010020
&MODL3(10) SETC '0145'                                            20301 11020020
&MODL3(11) SETC '0165'                                            20066 11030020
&MODL3(12) SETC '0135'                  MODEL C86 (135)           21054 11035021
&MODL3(13) SETC  '1195'                                       CK  21108 11037021
&MODL3(14) SETC  '0158'           OLYMPUS                     BH S21122 11039021
&MODL3(15) SETC  '0168'     PISCES                            BH S21122 11039421
&MODID    SETC '&MODL3(&X1)'                                      20065 11040020
.NOID    ANOP                                                           11120019
         AIF (&SGCPUB(40) NE 1).CALL                           BH M5821 11170021
.************************************************************* BH M5821 11180021
.*     SET THE BCT LOOP VALUE NEEDED FOR DEVICE DEPEND. TESTS  BH M5821 11190021
.************************************************************* BH M5821 11192021
&BCT(1)  SETC  '17000'       MOD 30 BCT VALUE                  BH M5821 11194021
&BCT(2)  SETC  '17000'       MOD 40 BCT VALUE                  BH M5821 11196021
&BCT(3)  SETC  '17000'       MOD 50 BCT VALUE                  BH M5821 11198021
&BCT(4)  SETC  '21000'       MOD 65 BCT VALUE                  BH M5821 11198421
&BCT(5)  SETC  '21000'       MOD 75 BCT VALUE                  BH M5821 11198821
&BCT(6)  SETC  '111200'      MOD 91 BCT VALUE                  BH M5821 11199221
&BCT(7)  SETC  '56340'       MOD 85 BCT VALUE                  BH M5821 11199621
&BCT(8)  SETC  '25000'       MOD 155 BCT VALUE                 BH M5821 11199721
&BCT(9)  SETC  '94600'       MOD 195 BCT VALUE                 BH M5821 11199821
&BCT(10) SETC  '17000'       MOD 145 BCT VALUE                 BH M5821 11199921
&BCT(11) SETC  '50000'       MOD 165 BCT VALUE                 BH M5821 11249921
&BCT(12) SETC  '17000'       MOD 135 BCT VALUE                 BH M5821 11259921
&BCT(13) SETC  '111200'            MODEL 370/195 BCT VALUE     @SA65200 11264921
&BCT(14) SETC  '25000'       MOD 158 BCT VALUE                 BH M5821 11269921
&BCT(15) SETC  '50000'       MOD 168 BCT VALUE                 BH M5821 11279921
&TIMBCTC SETC  '&BCT(&X1)'                                     BH M5821 11289921
.CALL    ANOP                                                  BH M5821 11291921
&SR(1)   SETC   '01'                   SER OPTION LIST - SEREP @SA77422 11292900
&SR(2)   SETC   '02'                                     SER 0 @SA77422 11293900
&SR(3)   SETC   '03'                                     SER 1 @SA77422 11294900
&SR(5)   SETC   '00'                                           @SA77422 11295900
         AIF   (&SGSUPRB(28) EQ 1).SKPSER                          ISER 11299921
         AIF   ((&SGCPUB(1) EQ 1 OR &SGSUPRB(31) EQ 1) AND (&SGMENTB(53X11600018
               ) EQ 0)).SETSERP                                    ISER 12000018
.* SET PRIMARY LEVEL OF SER SUPPORT                                ISER 23600018
&X2      SETA  0                                                   ISER 24000018
         AIF   (&SGSUPRB(10) EQ 1).SER1                            ISER 24400018
         AIF   (&SGSUPRB(9) EQ 1).SER0                             ISER 24800018
         AGO   .SEREP                                                   25800018
.SER1    ANOP                                                      ISER 26800018
&X2      SETA  &X2+1                                               ISER 27200018
.SER0    ANOP                                                      ISER 27600018
&X2      SETA  &X2+1                                               ISER 28000018
.SEREP   ANOP                                                      ISER 28400018
&X2      SETA  &X2+1                                               ISER 28800018
&SERL    SETC  '&SR(&X2)'                                          ISER 29200018
&SIZL    SETC  '00'                SET PRIMARY MACHINE SIZE    @SA65200 29400021
.* ENTRY TO LOOP FOR SETTING SER SWITCHES (SECONDARY LEVELS)       ISER 29600018
.* LOOP CONTROL: 5 PASSES                                          ISER 30000018
.* SET SWITCHES (&MODL,&MODL2) FOR SER-SUPPORTED MODELS            ISER 30400018
.CKSECMD ANOP                                                  @SA77422 30600000
&X1      SETA  1                                                   ISER 30800018
.MSSLOOP ANOP                                                      ISER 31200018
&X2      SETA  1                                                   ISER 31600018
         AIF   (&SGMOD1B(&X1) EQ 1).M4                             ISER 32000018
&X2      SETA  &X2+1                                               ISER 32400018
         AIF   (&SGMOD2B(&X1) EQ 1).M4                             ISER 32800018
&X2      SETA  &X2+1                                               ISER 33200018
         AIF   (&SGMOD3B(&X1) EQ 1).M4                             ISER 33600018
&X2      SETA  &X2+1                                               ISER 34000018
         AIF   (&SGMOD4B(&X1) EQ 1).M4                             ISER 34400018
&X2      SETA  &X2+1                                               ISER 34800018
         AIF   (&SGMOD5B(&X1) EQ 1).M4                             ISER 35200018
&X2      SETA  20                                                 20065 35600020
.M4      ANOP                                                      ISER 36000018
&TEMPMOD SETC  '&MOD(&X2)'                                         ISER 36400018
&TEMPSIZ SETC  '00'                SER SIZES NOT SUPPORTED     @SA65200 43600021
.* SET SWITCHES (&SERL,&SERL2) FOR LEVELS OF SER SUPPORT           ISER 44000018
&X2      SETA  1                                                   ISER 44400018
         AIF   (&SGSER1B(&X1) EQ 1).SR2                            ISER 44800018
&X2      SETA  &X2+1                                               ISER 45200018
         AIF   (&SGSER2B(&X1) EQ 1).SR2                            ISER 45600018
&X2      SETA  &X2+1                                               ISER 46000018
         AIF   (&SGSER3B(&X1) EQ 1).SR2                            ISER 46400018
&X2      SETA  5                                                   ISER 46800018
.SR2     ANOP                                                      ISER 47200018
&TEMPSER SETC  '&SR(&X2)'                                          ISER 47600018
&X1      SETA  &X1+1                                               ISER 55200018
         AIF   (&X1 GT 4).MSSL2                                    ISER 55600018
&MODL    SETC  '&MODL'.'&TEMPMOD'                                  ISER 56000018
&SIZL    SETC  '&SIZL'.'&TEMPSIZ'                                  ISER 56400018
&SERL    SETC  '&SERL'.'&TEMPSER'                                  ISER 56800018
         AGO   .MSSLOOP                                            ISER 57200018
.MSSL2   AIF   (&X1 GT 5).MSSL2A                                   ISER 57600018
&MODL2   SETC  '&TEMPMOD'                                          ISER 58000018
&SIZL2   SETC  '&TEMPSIZ'                                          ISER 58400018
&SERL2   SETC  '&TEMPSER'                                          ISER 58800018
         AGO   .MSSLOOP                                            ISER 59200018
.MSSL2A  ANOP                                                      ISER 59600018
&MODL2   SETC  '&MODL2'.'&TEMPMOD'                                 ISER 60000018
&SIZL2   SETC  '&SIZL2'.'&TEMPSIZ'                                 ISER 60400018
&SERL2   SETC  '&SERL2'.'&TEMPSER'                                 ISER 60800018
         AGO   .SERSET                                             ISER 61200018
.********      END MODEL INDEPENDENT SER      ********             ISER 61600018
.SETSERP ANOP                                                      ISER 62000018
.* SET SEREP ONLY SWITCH                                           ISER 62400018
&SERL    SETC  'FF'                                                ISER 62800018
         AGO   .SKPSER1                                            ISER 63200018
.SKPSER  ANOP                                                      ISER 63600018
.* SET SWITCHES FOR NO SER                                         ISER 64000018
&SERL    SETC  '00'                                                ISER 64400018
.SKPSER1 ANOP                                                      ISER 64800018
&SERL2   SETC  '00'                                                ISER 65200018
&MODL2   SETC  '00'                                                ISER 65600018
&SIZL    SETC  '00'                                                ISER 66000018
&SIZL2   SETC  '00'                                                ISER 66400018
   AGO  .CKSECMD                                                        66420021
.SERSET  AIF   (NOT &SGSCHDB(34)).NMCSSET                          MCS0 66450018
         AIF   (NOT &SGSCHDB(36)).NHCSYSL                          MCS0 66500018
&HARDCPC SETC  'SLOG'                                              MCS0 66550018
.*       SYSGEN HARDCOPY SPECIFICATION = SYSLOG                    MCS0 66600018
         AGO   .NMCSSET                                            MCS0 66650018
.NHCSYSL AIF   ('&SGSCHDC(14)' EQ '').NHCDADR                      MCS0 66700018
&HARDCPC SETC  '&SGSCHDC(14)'                                      MCS0 66750018
.*       SYSGEN HARDCOPY SPECIFICATION = (DEVICE ADDRESS)          MCS0 66800018
         AGO   .NMCSSET                                            MCS0 66850018
.NHCDADR ANOP                                                      MCS0 66900018
&HARDCPC SETC  '0000'                                              MCS0 66950018
.*       NO SYSGEN HARDCOPY SPECIFICATION                          MCS0 67000018
.NMCSSET ANOP                                                      MCS0 67050018
&STORAGE SETA  1                                                        67200018
         AIF   (&SGCPRGB(1) OR &SGCPRGB(2)).TIME1                  1595 67600018
         AIF   (&SGCPRGB(4)).SG2                                        68000018
&STORAGE SETA  4                                                        68400018
.*       SHARED STORAGE                                                 68800018
&PWARN   SETB  (&SGCPRGB(14))                                  @S21167P 69000021
         AGO   .TIME1                                              1595 69200018
.SG2     ANOP                                                           69600018
&STORAGE SETA  2                                                        70000018
.*       PARTITIONED STORAGE                                            70400018
.TIME1   ANOP                                                     20065 72400020
&VOLTSZA SETA  &SGDASDA                                           I267  75400019
         AIF   (NOT &SGMENTB(58)).NOEMU85                      @SA67998 75450021
         AIF   (NOT (&SGCPUB(7) OR &SGCPUB(8) OR &SGCPUB(10) OR        X75460021
               &SGCPUB(11) OR &SGCPUB(12) OR &SGCPUB(14) OR            X75470021
               &SGCPUB(15))).NOEMU85                           @SA67998 75480021
&EMUL85B SETB  1                                                 A33578 75500020
.NOEMU85 ANOP                                                    A33578 75550020
&SGCTRLC(6) SETC 'IEAANIP0'                                             75600018
         COPY  SGASMPAK                                                 76000018
         PUNCH  'IEAANIP0  START'                                       76400018
.*       IEAANIP TIMER,PROTECT,STORAGE,TTRINIT,HISVCIBM,LOWSVCUSER,     76800018
.*               LCS,COMMUNICATION,RESBLDL,RESACCSMETH,SMARTNIP,   LCS0 77200018
.*                 PRIMARYCONSOLE,ALTRNTCONSOLE,RESSVC,RJQVAL,     000C 77600018
.*               TRACE,SQS,MINPART,INITQBF,TIMERVALUE,2250CONSOLE,19029 78000019
.*               2260CONSOLE,ASR(MCH),ROLLOUT,CHANLCHKHANDLER,    19029 78400019
.*               TIME-SLICING,SERMODELS,SERSIZES,SERSUPPORTLEVELS, ISER 78800018
.*               MULTI-PROCESSING,PRIMARYCONSOLECPU2,ALTRNTCONSOLE MP65 79200018
.*               CPU2,MULTIPLECONSOLEOPTION,220CCONSOLE,2740/2741  MCS0 79400018
.*               CONSOLE,HARDCPY,LOG,MFT2LNKPAK,RESERPS,DDRSYS,@S21167P 79500021
.*               ASYNCATTACH,HEXMODELID,                           I250 79600019
.*               MOD7094EMULATORWITHWCS,BLOCKMULTPLXCHANNELS,    20065  79630020
.*               TSO,DASDDIVICES,CON3277,DDR,POWERWARNING      @S21167P 79660021
 PUNCH ' IEAANIP &SGCPUB(40),&SGSUPRB(23),&STORAGE,&SGSUPRB(3),&IEAHIA,X80000018
               &IEALOA,&SGCPRGB(11),&SGSUPRB(22),&SGSUPRB(20),&SGSUPRB(X80400018
               21),                                   XXXXXXX'          80800018
 PUNCH '               &SGMENTB(87),&SGSCHDC(1),&SGSCHDC(2),&SGSUPRB(25X81200018
               ),                             *****************'   000C 81600018
 PUNCH '               &SGSCHDA(7),                                    X82000018
                    *****************'                             000C 82400018
 PUNCH '               &SGSUPRA(1),                                    X82800018
                    *****************'                             000C 83200018
 PUNCH '               &SGCPRGA(6),                                    X83600018
                    *****************'                             000C 84000018
 PUNCH '               &SGSCHDA(9),                                    X84400018
                    *****************'                             000C 84800018
 PUNCH '               &SGSCHDA(8),                                    X85200018
                    *****************'                             000C 85600018
 PUNCH '               &TIMBCTC,                                       X86000020
                    *************'                                 000C 86400018
 PUNCH '               &SGSCHDA(10),                                   X86600019
                    *****************'                                  86800019
 PUNCH '               &SGSCHDA(11),                                   X87000019
                    *****************'                                  87200019
 PUNCH '               &SGSUPRB(28),                                   X87600018
                    *****************'                             000C 88000018
 PUNCH '               &SGCPRGB(6),                                    X88400018
                    *****************'                             000C 88800018
 PUNCH '               &SGSUPRB(29),                                   X89200018
                    *****************'                             000D 89600018
 PUNCH '               &SGCPRGB(12),                                   X90000018
                    *****************'                             ISER 90400018
 PUNCH '               &MODL.&MODL2,                                   X90800018
                       *********'                                  ISER 91200018
 PUNCH '               &SIZL.&SIZL2,                                   X91600018
                       *********'                                  ISER 92000018
 PUNCH '               &SERL.&SERL2,                                   X92400018
                       *********'                                  ISER 92800018
 PUNCH '               &SGCPRGB(13),                                   X93200018
                       **************'                             MP65 93600018
 PUNCH '               &SGSCHDC(12),                                   X94000018
                      **************'                              MP65 94400018
 PUNCH '               &SGSCHDC(13),                                   X94450018
                      **************'                              MP65 94500018
 PUNCH '               &SGSCHDB(34),                                   X94550018
                      **************'                              MCS0 94600018
 PUNCH '               &SG5450A,                                       X94650018
                    *************'                                 MCS0 94700018
 PUNCH '               &SGSCHDB(13),                                   X94750018
                      **************'                              MCS0 94800018
 PUNCH '               &HARDCPC,                                       X94830018
                      **************'                              MCS0 94870018
 PUNCH '               &SGSCHDB(38),                                   X94890018
                      **************'                              MCS0 94910018
 PUNCH '               &SGSUPRB(38),                                   X94950018
                      **************'                              LPA1 95000018
 PUNCH '               &SGSUPRB(40),                                   X95010019
                    *****************'                                  95020019
 PUNCH '               &SGSUPRB(34),                                   X95030019
                    *****************'                                  95040019
 PUNCH '               &SGSUPRB(37),                                   X95050019
                    *****************'                                  95060019
 PUNCH '               &MODID,                                         X95068019
                         * '                                            95076019
 PUNCH '               &EMUL85B,                                       X95079020
                              *       '                          A33578 95082020
 PUNCH '               &SGMENTB(68),                                   X95085020
                                  *       '                       20065 95088020
 PUNCH '               &SGSCHDB(44),                                   X95091020
                                  *       '                       20021 95094020
 PUNCH '               &VOLTSZA,                                       X95120021
                       ********    '                          CD  M1981 95140000
 PUNCH '               &SGSCHDA(18),                           BH 61205X95160021
                       ************'                           BH M5067 95180021
 PUNCH '               &SGSUPRB(33),                                   X95190021
                                  *'                           @S21167P 95193021
 PUNCH '               &PWARN'                                 @S21167P 95196021
         PUNCH '         END'                                           95200018
         PUNCH '/*'                                                     95600018
         MEND                                                           96000018
