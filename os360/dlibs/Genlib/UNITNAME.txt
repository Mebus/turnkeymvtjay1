         MACRO                                                          00020000
         UNITNAME   &UNIT=,&NAME=                                       00040000
         COPY  SGGBLPAK                                                 00060000
         GBLA &B,&LIMITA                                                00070000
         LCLA  &A,&N,&LENGTH                                            00080019
         LCLA  &LIMITB                                                  00082000
         LCLB  &RTNSW,&MULTAD                                           00090019
         LCLC  &C,&D,&E,&ADDR                                           00100019
         AIF   (T'&NAME EQ 'O' OR T'&UNIT EQ 'O').E8                    00110020
  AIF  (&SGMENTB(36)).MAX4                                              00120000
&SGMENTB(36)  SETB   1      UNITNAME ENTRY SWITCH.                      00140000
&B SETA 255   MAX. NO. OF NAME A-BOXES                                  00150018
  AIF  (&LIMIT(1)).MAX1                                                 00160000
 AIF (&LIMIT(3)).MAX3                                                   00190018
&SGMAXA(3)  SETA  50                                                    00200000
&LIMITA  SETA  510                                                      00210000
   AGO  .MX                                                             00220000
.MAX1  ANOP                                                             00240000
&SGMAXA(3)  SETA  100                                                   00260000
&LIMITA  SETA  510                                                      00270000
   AGO  .MX                                                             00280000
.MAX3 ANOP                                                              00330018
&SGMAXA(3) SETA 100                                                     00333018
&LIMITA  SETA  1028                                                     00335000
&B SETA 514   MAX. NO. OF NAME A-BOXES FOR 768 DEV. SUPPORT             00336018
.MX MNOTE *,'      MAXIMUM OF &SGMAXA(3) UNIT NAMES ALLOWED'            00340000
    MNOTE *,'      THE MAXIMUM NO. OF UNITS ALLOWED IS &B*2-THE NUMBER *00346018
               OF NAMES'                                                00352018
.MAX4  ANOP                                                             00360000
 AIF (K'&NAME LT 9 AND K'&NAME GT 0).TESTNM                             00367018
 AGO .BAD                                                               00374018
.TESTNM AIF (&A EQ K'&NAME).DONENM                                      00381018
&A SETA &A+1                                                            00388018
&C SETC '&NAME'(&A,1)                                                   00395018
 AIF (('&C' GE 'A' AND '&C' LE 'I') OR ('&C' GE 'J' AND '&C' LE 'R') OR*00402018
               ('&C' GE 'S' AND '&C' LE 'Z') OR ('&C' GE '0' AND '&C' L*00409018
               E '9')).TESTNM                                           00416018
 AIF ('&C' EQ '@' OR '&C' EQ '$' OR '&C' EQ '#' OR '&C' EQ '/' OR '&C' *00418019
               EQ '-').TESTNM                                           00420019
.BAD MNOTE 5,'* * * IEIUNI104 NAME VALUE &NAME INVALID'                 00423018
&SGQUITB SETB 1                                                         00430018
 AGO .MEND                                                              00437018
.DONENM  ANOP                                                           00444018
&A SETA 0                                                               00451018
.ON0     AIF   (&SGCNTRA(4) GE 1).ON1  HAS MACRO BEEN ENTERED BEFORE    00460000
         AGO   .INITIAL                                                 00480000
.ON1  ANOP                                                              00500000
         AIF   ('&SGUNNMC(&SGCNTRA(4))' EQ '&NAME').ON3                 00520000
     AIF  (&SGCNTRA(4) GE &SGMAXA(3)).E1                                00540000
&LIMITB  SETA  (&LIMITA-&SGCNTRA(4))          FIND MAX DEVICES AVAIBLE  00570000
         AIF   (&SGCNTRA(5) GT &LIMITB).E9    MAX EXCEDED               00572000
 AIF (&SGCNTRA(5) GE &B).E2            MAX. NO. OF UNITS EXCEEDED       00600018
&SGCNTRA(4)    SETA &SGCNTRA(4)+1  COUNT OF NAMES.                      00610000
&SGUNNMC(&SGCNTRA(4))  SETC  '&NAME'                                    00612000
    MNOTE *,'     &NAME IS THE SYMBOLIC NAME ASSOCIATED WITH THE'       00620000
    MNOTE *,'        DEVICES HAVING THE FOLLOWING ADDRESSES'            00640000
         AIF   (&SGCNTRA(6) LT 3).FILLIN                                00660000
&SGCNTRA(5)    SETA &SGCNTRA(5)+1  COUNT OF SETC'S IN USE FOR ADDRESSES 00680000
&SGCNTRA(6)    SETA 1              SET FLIP SWITCH TO ONE.              00700000
.FILLIN  ANOP              FFF MEANS END OF ONE NAME'S ADDRESSES.       00720000
&SGUNNMA(&SGCNTRA(5))  SETA 4096*&SGUNNMA(&SGCNTRA(5))+X'FFF'           00740000
&SGCNTRA(6)    SETA &SGCNTRA(6)+1                                       00760000
         AGO   .ON2                                                     00780000
.INITIAL ANOP            INITIALIATION ROUTINE.                         00800000
&SGCNTRA(4)    SETA 1         SET NAME COUNTER TO ONE.                  00820000
&SGCNTRA(5)    SETA 1         SET ADDRESS COUNTER TO ONE.               00840000
&SGCNTRA(6)    SETA 1         SET FLIP MECHANISM TO ONE.                00860000
&SGUNNMC(1)  SETC  '&NAME'                                              00880000
    MNOTE *,'     &NAME IS THE SYMBOLIC NAME ASSOCIATED WITH THE'       00900000
    MNOTE *,'        DEVICES HAVING THE FOLLOWING ADDRESSES'            00920000
.ON3     AIF   (&RTNSW).LOOP           IF ON, GENERATE MULTIPLE ADDRESS 00928019
         AIF   (N'&UNIT EQ 0).E7                                        00936019
         AIF   (&A EQ N'&UNIT).MEND                                     00944019
&A       SETA  &A+1                                                     00952019
         AIF   (K'&UNIT(&A) LT 3).E3                                    00960019
         AIF   (K'&UNIT(&A) GT 3).MULT  IF GT, CHECK FOR MULT ADDRESS   00968019
&C       SETC  '&UNIT(&A)'(1,1)                                         00980000
&D       SETC  '&UNIT(&A)'(2,1)                                         01000000
&E       SETC  '&UNIT(&A)'(3,1)                                         01020000
         AGO   .AIF                                                     01020819
.MULT    ANOP                                                           01021619
&MULTAD  SETB  1                                                        01022419
&LENGTH  SETA  K'&UNIT(&A)             LENGTH OF MULT ADDRESS OPERAND   01023219
         AIF   (&LENGTH LE 6).E4                                        01024019
         AIF   ('&UNIT(&A)'(5,1) NE ',').E4                             01024819
&LENGTH  SETA  &LENGTH-6                                                01025619
         AIF   (&LENGTH GT 3).E5                                        01026419
&N       SETA  &LENGTH                                                  01027219
.TEST    AIF   (&N EQ 0).VALID                                          01028019
&C       SETC  '&UNIT(&A)'(&N+5,1)     CHECK VALIDITY OF INCREMENTAL    01028819
         AIF   (NOT('&C' GE '0' AND '&C' LE '9')).E5                    01029619
&N       SETA  &N-1                                                     01030419
         AGO   .TEST                                                    01031219
.VALID   ANOP                                                           01032019
&RTNSW   SETB  1                                                        01032819
&C       SETC  '&UNIT(&A)'(6,&LENGTH)                                   01033619
&N       SETA  &C                                                       01034419
&ADDR    SETC  '&UNIT(&A)'(2,3)        ADDRESS TO BE INCREMENTED        01035219
.LOOP    ANOP                                                           01036019
&C       SETC  '&ADDR'(1,1)                                             01036819
&D       SETC  '&ADDR'(2,1)                                             01037619
&E       SETC  '&ADDR'(3,1)                                             01038419
.AIF     ANOP                                                           01039219
 AIF (NOT(('&C' GE '0' AND '&C' LE '9')OR('&C' GE 'A' AND '&C' LE 'F'))*01046220
               ).E3                                                     01053220
 AIF (NOT(('&D' GE '0' AND '&D' LE '9')OR('&D' GE 'A' AND '&D' LE 'F'))*01060220
               ).E3                                                     01067220
 AIF (NOT(('&E' GE '0' AND '&E' LE '9')OR('&E' GE 'A' AND '&E' LE 'F'))*01074220
               ).E3                                                     01081220
         AIF   (&MULTAD).CONV                                           01090019
         CONVERT TO=HEX,DIGITS=3,VALUE=&UNIT(&A)  GET HEX. VALUE AND    01100000
 AIF     (&HEX EQ X'FFF').E3           NO VALID FFF ADDRESS             01101020
         AGO   .MAX                                                     01103019
.CONV    CONVERT    TO=HEX,DIGITS=3,VALUE=&ADDR      GET HEX VALUE      01106019
         AIF   (&HEX EQ X'FFF').E4                                      01109020
.MAX     AIF   (&SGCNTRA(5) GE &B).E9        MAX. NO. OF UNITS EXCEEDED 01113021
&SGUNNMA(&SGCNTRA(5)) SETA 4096*&SGUNNMA(&SGCNTRA(5))+&HEX ADD IT IN.   01120000
         AIF   (&MULTAD).AD                                             01130019
    MNOTE *,'        &UNIT(&A)'                                         01140000
         AGO   .INC                                                     01141019
.AD MNOTE *,'        &ADDR'                                             01142019
&N       SETA  &N-1                                                     01143019
         AIF   (&N NE 0).SET                                            01144019
&RTNSW   SETB  0                                                        01145019
&MULTAD  SETB  0                                                        01146019
         AGO   .INC                                                     01147019
.SET     ANOP                                                           01148019
&HEX     SETA  &HEX+1                  INCREMENT ADDRESS                01149019
         CONVERT   TO=EBCDIC,VALUE=&HEX,DIGITS=3                        01150019
&ADDR    SETC  '&EBCDIC'                                                01151019
.INC     ANOP                                                           01152019
&SGCNTRA(6)    SETA &SGCNTRA(6)+1                                       01160000
.ON2     AIF   (&SGCNTRA(6) LT 3).ON3   IF THERE HAS BEEN TWO ADDRESSES 01180000
&SGCNTRA(5)    SETA &SGCNTRA(5)+1       STORED GO TO NEXT SETC SYMBOL.  01200000
&SGCNTRA(6)    SETA 1                                                   01220000
         AGO   .ON3                                                     01240000
.E1 MNOTE 5,'* * * IEIUNI101 &NAME-MAXIMUM NUMBER OF NAMES EXCEEDED'    01260000
&SGQUITB SETB  1                                                        01280000
         AGO   .MEND                                                    01300000
.E3      AIF   (&MULTAD).E6                                             01306019
    MNOTE 5,'* * * IEIUNI103 &NAME-UNIT VALUE &UNIT(&A) INVALID'        01312019
&SGQUITB SETB  1                                                        01318019
         AGO   .ON3                                                     01324019
.E6 MNOTE 5,'* * * IEIUNI106 &NAME-UNIT VALUE &ADDR INVALID'            01330019
&SGQUITB SETB  1                                                        01340000
         AGO   .ON3                                                     01360000
.E2 MNOTE 5,'* * * IEIUNI102 &NAME-EXCESSIVE ADDRESS VALUES'            01380000
&SGQUITB SETB  1                                                        01400000
         AGO   .MEND                                                    01402019
.E4 MNOTE 5,'* * * IEIUNI104 &NAME-MULTIPLE ADDRESS VALUE INVALID'      01404019
&SGQUITB SETB  1                                                        01406019
         AGO   .MEND                                                    01408019
.E5 MNOTE 5,'* * * IEIUNI105 &NAME-MULTIPLE ADDRESS INCREMENT INVALID'  01410019
&SGQUITB SETB  1                                                        01412019
         AGO   .MEND                                                    01414019
.E7 MNOTE 5,'* * * IEIUNI108 &NAME HAS NO ASSIGNED UNITS.'              01416020
&SGQUITB SETB  1                                                        01418019
         AGO   .MEND                                                    01418520
.E8 MNOTE 5,'* * *IEIUNI107 UNIT AND/OR NAME PARAMETERS ARE REQUIRED'   01419020
&SGQUITB SETB  1                                                        01419520
         AGO   .MEND                                                    01419900
.E9      MNOTE 5,'* * * IEIUNI109 MAXIMUM NUMBER OF UNITS SPECIFIED.  MC01469900
               AXIMUM IS &LIMITA-NUMBER OF UNITNAMES.'                  01479900
&SGQUITB SETB  1                                                        01489900
.MEND  MEND                                                             01519900
