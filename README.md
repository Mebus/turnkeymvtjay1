# The Turnkey MVT CD-ROM

This CD-ROM contains a complete, runnable OS/360 MVT release 21.8F system. The system has all of the compilers (except, at this writing, the FORTRAN runtime library), TSO, TCAM, and HASP-II 4.0 retrofitted from OS/VS2 SVS. There are also copies of many modification tapes. Everything's set up for use under Hercules, copies of which for Linux, Windows, and Mac OS X are also included.

The process used to generate the system is documented in Introduction to Generating and Running OS/360 on Hercules.
Contents

The following directories are on this CD:

* os360mvt - The prebuilt OS/360 MVT system:
  * dasd - Compressed CKD DASD images
  * dasdsetup - Control files and other data for creation of the DASD images
  * haspjcl - JCL to build and install HASP
  * html - The Introduction document
  * jcl - JCL to build the MVT system
  * sample - Sample JCL for several IBM utilities and compilers
  * tapes - Modifications and program source tapes 
  * os360 - Rick Fochtman's OS/360 distribution:
  * dlibs - Distribution libraries and sysgen macro source code
  * Loaders - Card image loaders
  * Reslibs - Loadable system residence volume libraries 

(The srclibs directory of Rick's distribution has been removed because of space limitations. It can be found on the CBT CD-ROM.)

In addition, a copy of The Wooden Paddle may be found in the root directory of this CD, as the file woodenpaddle.pdf. The installable copies of hercules are in the root, as well.

This turnkey CD is not as turnkey as the excellent version for MVS that Volker Bandke has made. You'll have to do the work of installing Hercules and the MVT system yourself. Fortunately, this isn't hard; a description of the process may be found here.

14 Feb 2003: NOTE: While HASP is included in the pregenerated system, it does not currently work. Any job that requests SYSOUT will ABEND with an S200. It's included, nevertheless, because I suspect the problem is easily fixed by someone more experienced with it than I am, and so that those who want to play with the system will have a real-life example of what systems programmers of the 1970s had to go through.
Jay Maynard, jmaynard@conmicro.cx 

Last updated 21 February 2003 